/*
 * @Date: 2022-10-25 10:56:36
 * @LastEditTime: 2022-10-25 14:38:13
 * @Description: 
 */

function getExploreCompatibility() {
    var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    var s;
    (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1] :
        (s = ua.match(/msie ([\d\.]+)/)) ? Sys.ie = s[1] :
            (s = ua.match(/edge\/([\d\.]+)/)) ? Sys.edge = s[1] :
                (s = ua.match(/firefox\/([\d\.]+)/)) ? Sys.firefox = s[1] :
                    (s = ua.match(/(?:opera|opr).([\d\.]+)/)) ? Sys.opera = s[1] :
                        (s = ua.match(/chrome\/([\d\.]+)/)) ? Sys.chrome = s[1] :
                            (s = ua.match(/version\/([\d\.]+).*safari/)) ? Sys.safari = s[1] : 0;
    // 根据关系进行判断
    if (Sys.ie) {
        console.log(('IE: ' + Sys.ie))
        return fasle
    }
    if (Sys.edge) {
        console.log(('EDGE: ' + Sys.edge))
        let vision = Number(Sys.edge.split('.')[0])
        if (vision >= 79) {
            return true
        } else {
            return false
        }
    }
    if (Sys.firefox) {
        console.log('Firefox: ' + Sys.firefox)
        let vision = Number(Sys.firefox.split('.')[0])
        if (vision >= 78) {
            return true
        } else {
            return false
        }
    }
    if (Sys.chrome) {
        console.log('Chrome: ' + Sys.chrome)
        let vision = Number(Sys.chrome.split('.')[0])
        if (vision >= 64) {
            return true
        } else {
            return false
        }
    }
    if (Sys.opera) {
        console.log('Opera: ' + Sys.opera)
        return true
    }
    if (Sys.safari) {
        console.log('Safari: ' + Sys.safari)
        let vision = Number(Sys.safari.split('.')[0])
        if (vision >= 12) {
            return true
        } else {
            return false
        }
    }
    console.log('获取失败')
    return true;
}
