import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import viteCompression from 'vite-plugin-compression'
// import Components from 'unplugin-vue-components/vite'
// import Pages from 'vite-plugin-pages';
const path = require('path')
function resolve(dir) {
  //__dirname 变量 是以文件所处的路径为值
  return path.join(__dirname, dir)
}
// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  const env = loadEnv(mode, process.cwd(), '')
  return {
    base: "./",
    plugins: [
      vue(),
      vueJsx(),
      viteCompression({
        threshold: 1024000
      })
      // Components({ dirs: ['src/views/components'], extensions: ['tsx'], include: [/\.tsx$/, /\.tsx\?tsx/], }),
      // Pages({
      //   dirs: 'src/views/components',
      //   extensions: ['tsx']
      // })
    ],
    define: {
      'process.env': env.APP_ENV
    },
    resolve: {
      alias: {
        '@': resolve('src'),
        'assets': resolve('src/assets'),
        'components': resolve('src/components'),
        'public': resolve('public')
      }
    },
    build: {
      // 指定输出路径(相对与根目录)
      outDir: "dist",
      // 指定生成静态资源的存放路径(相对outdir)
      assetsDir: "static",
      //是否开启调试
      sourcemap: false,
      minify: 'terser',
      chunkSizeWarningLimit: 500,
      target: 'es2015',
      cssTarget: 'chrome80',
      terserOptions: {
        compress: {
          //生产环境时移除console
          drop_console: true,
          drop_debugger: true
        }
      },
      rollupOptions: {
        output: {
          manualChunks(id) {
            if (id.includes('node_modules')) {
              return id
                .toString()
                .split('node_modules/')[1]
                .split('/')[0]
                .toString();
            }
          },
          chunkFileNames: (chunkInfo) => {
            const facadeModuleId = chunkInfo.facadeModuleId
              ? chunkInfo.facadeModuleId.split('/')
              : [];
            const fileName =
              facadeModuleId[facadeModuleId.length - 2] || '[name]';
            return `js/${fileName}/[name].[hash].js`;
          }
        }
      }
    },
    // build: {
    //   // 指定输出路径(相对与根目录)
    //   outDir: "sjsj2",
    //   // 指定生成静态资源的存放路径(相对outdir)
    //   assetsDir: "static",
    //   sourcemap: true,
    // },
    //部署的根路径
    // publicDir: './',
    server: {
      port: 9002,
      host: '0.0.0.0',
      open: true,
      watch: {
        ignored: ['node_modules'],
      },
      proxy: {
        [env.VITE_APP_API_BASE_ROOT]: {
          ws: true,
          target: env.VITE_APP_API_BASE_HOST,
          changeOrigin: true,
        },
        "/wjmrzt": {
          ws: true,
          target: "http://localhost:2010",
          changeOrigin: true,
        }
      }
    }
  }
})
