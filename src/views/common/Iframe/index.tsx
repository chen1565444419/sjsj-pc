/*
 * @Date: 2022-11-02 09:51:34
 * @LastEditTime: 2022-11-02 14:37:39
 * @Description: 
 */
import { defineComponent } from "vue";
import { useRoute } from "vue-router";
export default defineComponent({
    name: "Iframe",
    setup(props) {
        const route = useRoute() as any
        console.log(route.meta)
        return { route }

    },
    render() {
        return (
            <iframe src={this.route.meta.dz} frameborder="0" width="100%" height="95%"></iframe>

        )
    }
})
