/*
 * @Date: 2022-09-27 15:36:26
 * @LastEditTime: 2023-02-16 17:01:26
 * @Description: 平台首页
 */
import { defineComponent, ref } from "vue";
import { useUserStore } from '@/store/modules/user';

export default defineComponent({
    name: "Index",
    setup() {
        const renderDom = () => {
            return (
                <div style="background-color: #fff;height:100%">
                    {useUserStore().getUser.jgxx ? <div style="font-size:28px;padding:10% 30% 0">{useUserStore().getUser.jgxx.jgmc}-{useUserStore().getUser.yhxm},</div>
                        : null}
                    <div style="font-size:40px;padding:0 30%">欢迎您使用{useUserStore().getCurrentSystemName}</div>
                </div>
            )
        }
        return renderDom
    }
})
