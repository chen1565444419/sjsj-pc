/*
 * @Date: 2022-09-15 19:49:30
 * @LastEditTime: 2023-02-13 17:31:47
 * @Description: 登录页面
 */
import { defineComponent, nextTick, reactive, ref, Suspense } from "vue";
import classes from "./index.module.scss"
import { useUserStore } from '@/store/modules/user';
import MyForm from "@/components/MyForm";
import customSlots from "@/slots"
import axios from "@/axios";

export default defineComponent({
    setup() {
        const userStore = useUserStore();
        const xModal = ref()
        const xUpdate = ref();

        const myData = reactive({
            xtmc: userStore.getCurrentSystemName,
            loading: false,
            tcckShow: false,
            tcckProps: {
                /**
                 * 弹出窗口标题
                 */
                title: "",
                width: undefined as string | undefined,
                height: undefined,
            },
            formData: {
                yhdm: "",
                yhmm: "",
                //获取用户详细信息，不传默认只返回基础信息
                model: "details"
            },
            formItems: [
                {
                    field: "yhdm",
                    title: "用户名",
                    span: 24,
                    itemRender: {
                        name: "ElInput",
                        props: {
                            placeholder: "请输入用户名",
                            clearable: true,
                            "prefix-icon": "User",
                        },
                    },
                },
                {
                    field: "yhmm",
                    title: "密码",
                    span: 24,
                    itemRender: {
                        name: "ElInput",
                        props: {
                            placeholder: "请输入密码",
                            type: "password",
                            clearable: true,
                            "show-password": true,
                            "prefix-icon": "Lock",
                        },
                    },
                },
                {
                    align: "center",
                    span: 24,
                    itemRender: {
                        name: "$buttons",
                        children: [
                            { props: { type: "button", content: "忘记密码" } },
                            { props: { type: "submit", content: "登陆", status: "primary" } },
                        ],
                    },
                },
            ],
            formRule: {
                yhdm: [{ required: true, message: "请输入用户名" }],
                yhmm: [{ required: true, message: "请输入密码" }],
            },
        });

        const ggcx = () => {
            axios.post({
                sys: { authCode: "QTQX_WDGL", cllx: "select" },
                yobj: { yxx: "1", sfsxjg: "0", lb: "01", ckwd: "1" }
            }).then((response) => {
                if (response.status && response.data.list.length > 0 && response.data.list[0].id) {
                    axios.post({
                        sys: {
                            authCode: "QTQX_WDGL",
                            cllx: "dxjcxx"
                        },

                    }).then(async res => {
                        myData.tcckProps.title = "公告展示"
                        myData.tcckShow = true
                        console.log(xUpdate.value)
                        await nextTick()
                        await nextTick()
                        xUpdate.value.initPage(res.data, 'ggzs', null, { ggzs: response.data.list[0].id });
                    })
                }
            })

        }

        ggcx()

        /**
         * 弹窗页面回调
         */
        const fromClose = () => {
            myData.tcckShow = false
        }

        /**
         * @description: 登录表单提交
         * @return {*}
         */
        const submitEvent = () => {
            let query = {
                sjdx: {
                    dxdm: "SYS_QX_YHXX_MRDL",
                },
                sys: {
                    cllx: "yhdl",
                },
                yobj: myData.formData,
            }
            userStore.login(query)
        };

        return {
            myData,
            submitEvent,
            fromClose,
            xUpdate,
            xModal
        }
    },
    render() {
        return (<div class={classes.login}>
            <el-container>
                <el-main>
                    <div class={classes.content1}>
                        <div class={classes.xtbt}>{this.myData.xtmc}</div>
                        <vxe-form
                            data={this.myData.formData}
                            items={this.myData.formItems}
                            rules={this.myData.formRule}
                            loading={this.myData.loading}
                            onSubmit={this.submitEvent}
                        >
                        </vxe-form>
                    </div>
                </el-main>
                <el-footer> @2021 </el-footer>
            </el-container>
            <Suspense>
                <vxe-modal transfer ref="xModal" v-model={this.myData.tcckShow} {...this.myData.tcckProps}
                    show-zoom resize>
                    <MyForm ref="xUpdate" slots={customSlots} onClose={this.fromClose}></MyForm>
                </vxe-modal>
            </Suspense>
        </div >
        )
    }
})
