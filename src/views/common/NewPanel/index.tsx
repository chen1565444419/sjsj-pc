/*
 * @Date: 2022-09-24 18:18:02
 * @LastEditTime: 2022-10-10 15:57:31
 * @Description: 空白路由目录页
 */
import { defineComponent, KeepAlive, reactive, Suspense, watch } from "vue";
import { useRoute } from 'vue-router';

export default defineComponent({
    name: "NewPanel",
    setup() {
        const route = useRoute();
        const myData = reactive({
            key: 0,
        })
        watch(() => route.path, (newPath, oldPath) => {
            myData.key = new Date().getTime()
        }, { immediate: true });
        const renderDom = () => {
            return (

                <router-view style="height:100%" v-slots={{
                    default: (scope: any) => <Suspense><transition style="height:100%" name="move" mode="out-in"> <KeepAlive >{scope.Component}</KeepAlive> </transition></Suspense>,
                }}
                ></router-view>

            )
        }
        return renderDom
    }
})
