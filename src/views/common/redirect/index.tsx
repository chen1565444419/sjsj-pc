import { defineComponent, reactive } from "vue";
import { useRouter } from "vue-router";


export default defineComponent({
    name: 'Redirect',
    setup() {
        const route = useRouter()

        route.replace({ path: '/' + route.currentRoute.value.path })

    }
})
