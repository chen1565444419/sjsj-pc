/*
 *
 * @Date: 2022-09-15 19:49:30
 * @LastEditTime: 2023-02-13 11:35:57
 * @Description: 标签页
 */
import { defineComponent, onMounted, reactive, ref, watch } from "vue";
import { useTabsStore } from '@/store/modules/tabs';
import classes from "./index.module.scss"
import { useRoute } from "vue-router";
import router from "@/router";
import { ArrowDown } from "@element-plus/icons-vue";
export default defineComponent({
    name: "HomeTabs",
    setup() {
        const route = useRoute();
        const currentName = ref("");
        const tags = ref([] as any);
        onMounted(() => {
            //页面初始化设置
            tags.value = useTabsStore().tabsList
            watch(() => route.path, () => {
                currentName.value = route.meta.title as string;
                let result = tags.value.findIndex(
                    (item: any) => item.meta.title === currentName.value
                );
                changeMenu(tags.value[result]);
            }, { immediate: true, deep: true });
            watch(() => useTabsStore().tabsList, () => {
                tags.value = useTabsStore().tabsList
            })

            // watch(() => currentName.value, () => {
            //     let result = tags.value.findIndex(
            //         (item: any) => item.meta.title === currentName.value
            //     );
            //     changeMenu(tags.value[result]);
            //     router.push({ name: tags.value[result].name });

            // })
        })

        /**
         * @description: 关闭标签
         * @param {any} tag
         * @param {any} index
         * @return {*}
         */
        const handleClose = (tag: any, index: any) => {
            let length = tags.value.length - 1;
            useTabsStore().closeTab(tag);
            if (tag.name !== route.name) {
                return;
            }
            if (index === length) {
                router.push({ name: tags.value[index - 1].name });
            } else {
                router.push({ name: tags.value[index].name });
            }
        }

        /**
         * @description: 切换菜单
         * @param {any} item
         * @return {*}
         */
        const changeMenu = (item: any) => {
            let params = {
                path: route.path,
                name: route.name,
                meta: {
                    title: route.meta.title,
                },
            };
            useTabsStore().selectMenu(params);
        }

        /**
         * @description: 标签编辑事件
         * @param {any} targetName
         * @param {any} action
         * @return {*}
         */
        const handleTabsEdit = (targetName: any, action?: any) => {
            let result = tags.value.findIndex(
                (item: any) => item.meta.title === targetName
            );
            handleClose(tags.value[result], result);
        }

        /**
         * @description: 标签点击事件
         * @param {any} tab
         * @return {*}
         */
        const handleTabClick = (tab: any) => {
            console.log(tab)
            let result = tags.value.findIndex(
                (item: any) => item.meta.title === tab.props.name
            );
            changeMenu(tags.value[result]);
            router.push({ name: tags.value[result].name });
        }
        /**
        * 末尾操作
        * @param command
        */
        const handleTags = (command: any) => {
            const remove = (other?: boolean) => {
                if (other && route.path !== "/index") {
                    useTabsStore().setTabsList([{
                        path: route.path,
                        name: route.name,
                        meta: { title: route.meta.title }
                    }])
                }
                else {
                    useTabsStore().setTabsList([])
                    router.push('');
                }

            }

            command === "other" ? remove(true) : remove()
        };

        return {
            currentName,
            tags,
            handleClose,
            changeMenu,
            handleTabsEdit,
            handleTabClick,
            handleTags
        }

    },
    render() {
        return (<div class={classes.tab}>
            <el-tabs
                v-model={this.currentName} type="card" closable onEdit={this.handleTabsEdit} onTabClick={this.handleTabClick} >
                {this.tags.map((item: any, index: number) => {
                    return (<el-tab-pane key={item.meta.title} label={item.meta.title} name={item.meta.title}></el-tab-pane>)
                })}
            </el-tabs>
            <div class={classes.tagsCloseBox}>
                <el-dropdown onCommand={this.handleTags}
                    v-slots={{
                        dropdown: () => {
                            return (
                                <el-dropdown-menu size="small">
                                    <el-dropdown-item command="other">关闭其他</el-dropdown-item>
                                    <el-dropdown-item command="all">关闭所有</el-dropdown-item>
                                </el-dropdown-menu>
                            );
                        },
                    }}>
                    <el-button size="small" type="primary">
                        关闭选择
                        <el-icon><ArrowDown /></el-icon>
                    </el-button>
                </el-dropdown>
            </div >
        </div>)
    }
})
