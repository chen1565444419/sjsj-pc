/*
 * @Date: 2022-09-06 15:01:26
 * @LastEditTime: 2022-10-10 16:04:18
 * @Description: 单独展示页面
 */
import { defineComponent, reactive, Suspense } from "vue";
import HomeTabs from "../HomeTabs";
import classes from "./index.module.scss";
import MenuItem from '@/components/MenuItem'
export default defineComponent({
    components: { HomeTabs, MenuItem },
    setup() {
        const myData = reactive({
            // @ts-ignore
            xtmc: window.WEB_CONFIG.xtmc,
        });
        document.title = myData.xtmc;
        return {
            myData
        }
    },
    render() {
        return (
            <div class={classes.singleContentBox}>
                <div class={classes.singleContent}>
                    <el-scrollbar id="main-container" height="100%">
                        <Suspense>
                            <router-view v-slots={{
                                default: (scope: any) => <transition id="main-container" name="move" mode="out-in"> <keep-alive is={scope.Component} >{scope.Component}</keep-alive> </transition>,
                            }}
                            ></router-view>
                        </Suspense>
                    </el-scrollbar>
                </div>
            </div>
        )
    }
})
