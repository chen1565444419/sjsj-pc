/*
 * @Date: 2022-09-23 15:12:54
 * @LastEditTime: 2022-10-10 16:04:46
 * @Description: 错误处理页面
 */
import router from "@/router";
import { defineComponent } from "vue";
export default defineComponent({
    name: "Error",
    props: {
        errorCode: {
            type: String,
            default: "404",
        },
    },
    setup() {
        const goHome = () => {
            router.push('/')
        }
        const description = {
            404: "抱歉，您访问的页面不存在。",
            403: "抱歉，您无权访问此页面。",
            500: "抱歉，服务器报告错误。"
        }
        return { goHome, description }
    },
    render() {
        return (
            <el-empty v-slots={{
                description: () => {
                    return (<>
                        <div style="color:#303133;font-size:24px;line-height:44px">{this.errorCode}</div>
                        <div style="color:#909399">{this.description[this.errorCode]}</div>
                    </>)
                }
            }}>
                <el-button type="primary" onClick={this.goHome}>返回首页</el-button>
            </el-empty>
        )
    }
})
