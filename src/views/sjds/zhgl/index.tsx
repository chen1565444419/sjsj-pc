import { defineComponent, ref } from "vue";
import Sjdx from "@/components/Sjdx";
import { PictureFilled } from "@element-plus/icons-vue";

export default defineComponent({
    name: "SJDS_KETTLE_ZHGL",
    setup() {
        const show = ref(false);
        setTimeout(() => {
            show.value = true
        }, 0)
        return { show }
    },
    render() {
        return this.show ? <Sjdx params={{'sjdx.dxdm': "KETTLE_GLPT_ZHGL"}} slots={{
            zht: (data: any) => {
                return <div style="width:100%;text-align: center;">
                    <el-image
                        src={"data:image/png;base64," + data.data.zht}
                        preview-src-list={["data:image/png;base64," + data.data.zht]}
                        fit="cover"
                        v-slots={{
                            error: () => {
                                return (<el-icon style="font-size:22px;display:block;"><PictureFilled/></el-icon>)
                            }
                        }}
                    ></el-image>
                </div>
            }
}}/> : null
    }
})
