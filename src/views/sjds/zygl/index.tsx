import {defineComponent, nextTick, ref} from "vue";
import Sjdx from "@/components/Sjdx";
import axios from "@/axios";
import { PictureFilled } from "@element-plus/icons-vue";
import MySelect from "@/components/MySelect";
import MyMarkDown from "@/components/MyMarkDown";
import MyDownList from "@/components/MyDownList";
import {zdMcByDm} from "@/utils/common";

export default defineComponent({
    name: "SJDS_KETTLE_ZYGL",
    setup() {
        const show = ref(false);
        setTimeout(() => {
            show.value = true
        }, 0)
        return { show }
    },
    render() {
        const myData = {
            tthis:this,
            rule:{
                nullRule:[],
                gzlj:[{ required: true, message: '工作路径必填', trigger: 'change' }],
                shell:[{ required: true, message: 'shell命令必填', trigger: 'change' }],
                sjzt : [{ required: true, message: '数据载体必填', trigger: 'change' }],
                sql : [{ required: true, message: 'sql脚本必填', trigger: 'change' }],
                js : [{ required: true, message: 'JavaScript脚本必填', trigger: 'change' }],
                kmlm : [{ required: true, message: 'KM类名必填', trigger: 'change' }],
                kmpz : [{ required: true, message: 'KM配置必填', trigger: 'change' }]
            },
            zylxUpdate:(data: any) => {
                switch (data.property){
                    case "gzlj":
                    case "shell":
                        return data.data.zylx === 'shell';
                    case "sjzt":
                    case "sql":
                        return data.data.zylx === 'sql';
                    case "js":
                        return data.data.zylx === 'javascript';
                    case "kmlm":
                    case "kmpz":
                        return data.data.zylx === 'km';
                    default:
                        console.error("不支持的字段",data.property)
                        return true
                }
            }
        }
        return this.show ? <Sjdx params={{'sjdx.dxdm': "KETTLE_GLPT_ZYGL"}} slots={{
            zyt: (data: any) => {
                return <div style="width:100%;text-align: center;">
                    <el-image
                        src={"data:image/png;base64," + data.data.zyt}
                        preview-src-list={["data:image/png;base64," + data.data.zyt]}
                        fit="cover"
                        v-slots={{
                            error: () => {
                                return (<el-icon style="font-size:22px;display:block;"><PictureFilled/></el-icon>)
                            }
                        }}
                    ></el-image>
                </div>
            },
            zylx:  (data: any) => {
                if (data.$table) {
                    zdMcByDm("OTHER_KETTLE_ZYLX",data.row.zylx).then(res=>{
                        data.row.zylx = res
                    })
                    return <span>{data.row.zylx}</span>
                }else{
                    let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                    data.item.itemRender.props.disabled = cllx !== 'insert'&& cllx !== 'select';
                    const updateVal = (value: any) => {
                        data.data.zylx = value
                    }
                    //新增
                    return <MySelect {...data.item.itemRender.props} zdlb="OTHER_KETTLE_ZYLX"
                                   modelValue={data.data.zylx} onUpdate:modelValue={updateVal}></MySelect>
                }
            },
            gzlj: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.gzlj}</span>
                    )
                }
                let formRule = data.item.itemRender.props.myData.formRule;
                if(data.data.zylx === 'shell'){
                    formRule.gzlj = myData.rule.gzlj
                    formRule.shell = myData.rule.shell
                }else {
                    formRule.gzlj = myData.rule.nullRule
                    formRule.shell = myData.rule.nullRule
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.disabled = cllx !== 'insert'&&cllx !== 'update';
                const updateVal = (value: any) => {
                    data.data.gzlj = value
                }
                //新增
                return <vxe-input {...data.item.itemRender.props} modelValue={data.data.gzlj} onUpdate:modelValue={updateVal}></vxe-input>
            },
            shell: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.shell}</span>
                    )
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.mode = cllx !== 'insert' && cllx !== 'update'?"preview":"";
                const updateVal = (value: any) => {
                    data.data.shell = value
                }
                //新增
                return <MyMarkDown codeType="shell" {...data.item.itemRender.props} modelValue={data.data.shell} onUpdate:modelValue={updateVal}></MyMarkDown>
            },
            sjzt: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.sjzt}</span>
                    )
                }
                let formRule = data.item.itemRender.props.myData.formRule;
                if(data.data.zylx === 'sql'){
                    formRule.sjzt = myData.rule.sjzt
                    formRule.sql = myData.rule.sql
                }else {
                    formRule.sjzt = myData.rule.nullRule
                    formRule.sql = myData.rule.nullRule
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.disabled = cllx !== 'insert' && cllx !== 'update';
                const updateVal = (value: any) => {
                    data.data.sjzt = value
                }
                //新增
                return <MyDownList {...data.item.itemRender.props} zdlb="SYS_COMMON_SJZT"
                                   modelValue={data.data.sjzt} onUpdate:modelValue={updateVal}></MyDownList>
            },
            sql: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.sql}</span>
                    )
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.mode = cllx !== 'insert' && cllx !== 'update'?"preview":"";
                const updateVal = (value: any) => {
                    data.data.sql = value
                }
                //新增
                return <MyMarkDown codeType="sql" {...data.item.itemRender.props} modelValue={data.data.sql} onUpdate:modelValue={updateVal}></MyMarkDown>
            },
            js: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.js}</span>
                    )
                }
                let formRule = data.item.itemRender.props.myData.formRule;
                if(data.data.zylx === 'javascript'){
                    formRule.js = myData.rule.js
                }else {
                    formRule.js = myData.rule.nullRule
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.mode = cllx !== 'insert' && cllx !== 'update'?"preview":"";
                const updateVal = (value: any) => {
                    data.data.js = value
                }
                //新增
                return <MyMarkDown codeType="javascript" {...data.item.itemRender.props} modelValue={data.data.js} onUpdate:modelValue={updateVal}></MyMarkDown>
            },
            kmlm: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.kmlm}</span>
                    )
                }
                let formRule = data.item.itemRender.props.myData.formRule;
                if(data.data.zylx === 'km'){
                    formRule.kmlm = myData.rule.kmlm
                    formRule.kmpz = myData.rule.kmpz
                }else {
                    formRule.kmlm = myData.rule.nullRule
                    formRule.kmpz = myData.rule.nullRule
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.disabled = cllx !== 'insert' && cllx !== 'update';
                const updateVal = (value: any) => {
                    data.data.kmlm = value
                }
                //新增
                return <vxe-input {...data.item.itemRender.props} modelValue={data.data.kmlm} onUpdate:modelValue={updateVal}></vxe-input>
            },
            kmpz: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.kmpz}</span>
                    )
                }
                data.item.visibleMethod = myData.zylxUpdate
                let cllx = data.item.itemRender.props.myData.dxjcxx.sys.cllx;
                data.item.itemRender.props.disabled = cllx !== 'insert' && cllx !== 'update';
                const updateVal = (value: any) => {
                    data.data.kmpz = value
                }
                //新增
                return <MyMarkDown codeType="json" {...data.item.itemRender.props} modelValue={data.data.kmpz} onUpdate:modelValue={updateVal}></MyMarkDown>
            }
}}/> : null
    }
})
