/*
 * @Date: 2022-10-08 09:54:01
 * @LastEditTime: 2022-11-07 15:09:05
 * @Description: 自定义插槽实现模板
 */
import { defineComponent, ref } from "vue";
import Sjdx from "@/components/Sjdx";
import MyDownList from "@/components/MyDownList";

export default defineComponent({
    name: "KFZFW_QXGL_QXXX",
    setup() {
        const show = ref(false);
        setTimeout(() => {
            show.value = true
        }, 0)

        return { show }
    },
    render() {
        return this.show ? <Sjdx params={{ 'sys.authCode': "KFZFW_QXGL_QXXX" }} slots={{
            fqx: (data: any) => {
                if (data.$table) {
                    return (
                        <span>{data.row.dm}</span>
                    )
                }
                if (data.$form) {
                    const updateVal = (value: any) => {
                        data.data.fqx = value
                    }
                    const change = (value: any) => {
                        if (value && value.qxdm) {
                            data.data.dm = value.qxdm + "_"
                        }
                    }
                    return (
                        <MyDownList {...data.item.itemRender.props} zdlb="SYS_QX_QXXX" modelValue={data.data.fqx} onUpdate:modelValue={updateVal} onChange={change}></MyDownList>
                    )
                }
            }
        }}></Sjdx> : null
    }
})
