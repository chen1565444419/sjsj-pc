/*
 * @Date: 2022-10-28 15:47:09
 * @LastEditTime: 2023-02-13 10:12:47
 * @Description: 文件管理
 */
import { defineComponent, ref } from "vue";
import Sjdx from "@/components/Sjdx";
import axios from "@/axios";
import { PictureFilled } from "@element-plus/icons-vue";

export default defineComponent({
    name: "KFZFW_PTGL_WJGL",
    setup() {
        const show = ref(false);
        setTimeout(() => {
            show.value = true
        }, 0)

        return { show }
    },
    render() {
        return this.show ? <Sjdx params={{ 'sys.authCode': "KFZFW_PTGL_WJGL" }} slots={{
            wjnr: (data: any) => {
                const download = (id: string) => {
                    let params = {
                        sjdx: {
                            dxdm: 'SYS_QX_QTQX'
                        },
                        sys: {
                            cllx: 'download',
                        },
                        yobj: {
                            id: id
                        }
                    }
                    axios.download(params)
                }

                const upload = (uploadFile: any, uploadFiles: any) => {
                    getBase64(uploadFile.raw).then((res: any) => {
                        data.data.wjm = uploadFile.name
                        data.data.wjlx = uploadFile.name.split('.').pop()
                        data.data.wjnr = res.substring(res.indexOf(",") + 1)
                        console.log("转换base64成功")
                    })
                }
                const getBase64 = (file: any) => {
                    return new Promise((resolve, reject) => {
                        let reader = new FileReader()	//定义方法读取文件
                        reader.readAsDataURL(file)	//开始读文件  本身是图片的二进制数据 进行base64加密形成字符串
                        reader.onload = () => resolve(reader.result)//成功返回对应的值 reader.result可以直接放在img标签中使用
                        reader.onerror = () => reject(reader.error)//失败返回失败信息
                    })
                }
                if (data.$table) {
                    if (data.row.wjlx === 'png' || data.row.wjlx === 'jpg' || data.row.wjlx === 'jpeg' || data.row.wjlx === 'gif') {
                        return <el-image
                            style="height:100%;width:100%;"
                            src={axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + data.row.id}
                            preview-src-list={[axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + data.row.id]}
                            fit="cover"
                            v-slots={{
                                error: () => {
                                    return (<el-icon style="font-size:22px;display:block;"><PictureFilled /></el-icon>)
                                }
                            }}
                        ></el-image>
                    }
                    return <el-button size="small" link type='primary' onClick={() => download(data.row.id)}>下载</el-button>
                }
                if (data.$form) {
                    if (data.data.id) {
                        if (data.data.wjlx === 'png' || data.data.wjlx === 'jpg' || data.data.wjlx === 'gif') {
                            return <el-image
                                style="height:100%;width:100%;"
                                src={axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + data.data.id}
                                preview-src-list={[axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + data.data.id]}
                                fit="cover"
                                v-slots={{
                                    error: () => {
                                        return (<el-icon style="font-size:22px;display:block;"><PictureFilled /></el-icon>)
                                    }
                                }}
                            ></el-image>
                        }
                        return <el-button size="small" type="primary" onClick={() => download(data.data.id)}>下载</el-button>
                    }
                    return <el-upload ref="uploadRef" auto-upload={false} action="" limit={1} onChange={upload}>
                        <el-button size="small" type="primary">上传</el-button>
                    </el-upload>
                }
            }
        }}></Sjdx> : null
    }
})
