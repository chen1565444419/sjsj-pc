/*
 * @Description: 文件管理
 */
import { defineComponent, ref } from "vue";
import Editor from "@/components/MyEditor";

export default defineComponent({
    name: "KFZFW_PTGL_WJGL",
    components: { Editor },
    setup() {
        const show = ref(false);
        setTimeout(() => {
            show.value = true
        }, 0)

        return { show }
    },
    render() {
        return this.show ? <Editor></Editor> : null
    }
})
