/*
 * @Date: 2022-10-08 09:54:01
 * @LastEditTime: 2022-11-07 15:09:05
 * @Description: 自定义插槽实现模板
 */
import MyDocument from "@/components/MyDocument";
import { defineComponent, ref } from "vue";

export default defineComponent({
    name: "Test",
    components: { MyDocument },
    setup() {
    },
    render() {
        return <MyDocument modelValue=""></MyDocument>
    }
})
