import MyForm from '@/components/MyForm'
// import MySelectGrid from '@/components/MySelectGrid'
import { defineComponent, reactive, onMounted, ref, nextTick, computed, watch } from 'vue'
export default defineComponent({
    name: 'test1',
    components: { MyForm },
    setup() {
        const dxjcxx = {
            "cllxkz": {
                "select": {
                    "qxpz": {
                        "plclLeftZdans": 7,
                        "plclRightZdans": 2,
                        "plclLeft": {
                            "bjms": {
                                "type": "primary",
                                "content": "编辑模式",
                                "buttonOptions": {
                                    "clfs": "ymcsth",
                                    "htqqts": false,
                                    "sfxyxzjl": false,
                                    "isshow": true,
                                    "params": {
                                        "editCofnig": {"enabled": true},
                                        "dxjcxx": {
                                            "cllxkz": {
                                                "select": {
                                                    "qxpz": {
                                                        "plclLeft": {
                                                            "qxbj": {"buttonOptions": {"isshow": true}},
                                                            "bjms": {"buttonOptions": {"isshow": false}}
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            "qxbj": {
                                "type": "primary",
                                "content": "取消编辑",
                                "buttonOptions": {
                                    "clfs": "ymcsth",
                                    "htqqts": false,
                                    "sfxyxzjl": false,
                                    "isshow": false,
                                    "params": {
                                        "editCofnig": {"enabled": false},
                                        "dxjcxx": {
                                            "cllxkz": {
                                                "select": {
                                                    "qxpz": {
                                                        "plclLeft": {
                                                            "qxbj": {"buttonOptions": {"isshow": false}},
                                                            "bjms": {"buttonOptions": {"isshow": true}}
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            },
                            "insert": {
                                "type": "primary",
                                "content": "导入对象",
                                "buttonOptions": {"clfs": "tcck", "sfxyxzjl": false}
                            },
                            "plsc": {
                                "type": "primary",
                                "content": "批量删除",
                                "buttonOptions": {"clfs": "htqq", "htqqts": true}
                            },
                            "dcmb": {
                                "type": "primary",
                                "content": "导出模板",
                                "buttonOptions": {"clfs": "download", "htqqts": false, "sfxyxzjl": false}
                            },
                            "sjplsc": {
                                "type": "primary",
                                "content": "批量上传",
                                "buttonOptions": {"clfs": "upload", "htqqts": false, "sfxyxzjl": false}
                            },
                            "fzjl": {
                                "type": "primary",
                                "content": "复制",
                                "buttonOptions": {"clfs": "htqq", "htqqts": true, "params": {"sys": {"zxcz": "plcl"}}}
                            },
                            "pzdx": {
                                "type": "primary",
                                "content": "对象配置",
                                "buttonOptions": {
                                    "clfs": "tcck",
                                    "tcqp": true,
                                    "sfxyxzjl": false,
                                    "dxjcxx": {"sjdx": {"dxdm": "SYS_SJGL_SJDX"}, "sys": {"cllx": "update"}},
                                    "jcxxkz": {"obj": "dxjcxx.sjdx"}
                                }
                            },
                            "pzzd": {
                                "type": "primary",
                                "content": "字段配置",
                                "buttonOptions": {
                                    "clfs": "tcck",
                                    "sfxyxzjl": false,
                                    "tcqp": true,
                                    "dxjcxx": {"sjdx": {"dxdm": "SYS_SJGL_SJZD"}, "sys": {"cllx": "select"}},
                                    "jcxxkz": {"fields.sjdx.cxmrz": "dxjcxx.sjdx.id"}
                                }
                            },
                            "scdxst": {
                                "type": "primary",
                                "content": "生成实体",
                                "buttonOptions": {"clfs": "htqq", "htqqts": true}
                            }
                        },
                        "plclRight": {
                            "dcdqysj": {
                                "content": "导出当前页",
                                "buttonOptions": {"clfs": "download", "sfxyxzjl": false}
                            },
                            "dcsj": {
                                "content": "导出全部",
                                "buttonOptions": {
                                    "clfs": "download",
                                    "sfxyxzjl": false,
                                    "params": {"page": {"pageSize": 50000, "pageNumber": 1}}
                                }
                            }
                        }
                    }, "dcwjmc": "数据对象导出文件名称自定义", "my-help": "42F03A5FE3B9425E98E46B52298485A2"
                }, "update": {"my-help": "42F03A5FE3B9425E98E46B52298485A2"}
            },
            "fields": {
                "myList": {
                    "zddm": "myList",
                    "px": 60,
                    "zdywlb": "99",
                    "kjlx": "MySelectGrid",
                    "kzxx": {
                        "cllxkz": {
                            "select": {"show": true, "zdkd": 2, "readonly": true, "disabled": true},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 2},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 2}
                        },
                        "kjkz": {"title": ""},
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "查询表格"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "查询表格"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "CXBG",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211206223234",
                    "id": "11B812377E0741CBABA0E050994F96A5",
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "CHAXUNBIAOGE",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206231924",
                    "yxx": "1",
                    "zdmc": "查询表格",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "0"
                }, "myLbxz": {
                    "zddm": "myLbxz",
                    "px": 70,
                    "zdkd": 40,
                    "zdywlb": "99",
                    "kjlx": "checkbox",
                    "lbzs": "1",
                    "kzxx": {
                        "kjkz": {"title": ""},
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "列表选择"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "列表选择"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 40},
                            "dcsj": {"readonly": true, "show": false, "disabled": false, "zdkd": 40},
                            "insert": {"readonly": false, "show": false, "disabled": false, "zdkd": 40},
                            "update": {"readonly": false, "show": false, "disabled": false, "zdkd": 40},
                            "dcmb": {"readonly": true, "show": false, "disabled": false, "zdkd": 40},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": false, "zdkd": 40}
                        }
                    },
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "LBXZ",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211114004851",
                    "id": "100E48F672564E0A91D623CD2F5F22F5",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "LIEBIAOXUANZE",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211114004438",
                    "yxx": "1",
                    "zdmc": "列表选择",
                    "zydj": "06",
                    "mbzs": "0"
                }, "myLbxh": {
                    "zddm": "myLbxh",
                    "px": 80,
                    "zdkd": 50,
                    "zdmc": "序号",
                    "zdywlb": "99",
                    "kjlx": "seq",
                    "lbzs": "1",
                    "kzxx": {
                        "kjkz": {"treeNode": false},
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "序号"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "序号"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 50},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 50},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 50},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 50},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 50},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 50}
                        }
                    },
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "XH",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211114005941",
                    "id": "94779102E7C14110B3236DD056E0C6FB",
                    "gshff": "gshffLbxh",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "XUHAO",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211114004438",
                    "yxx": "1",
                    "zydj": "06",
                    "mbzs": "0"
                }, "dxdm": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 90,
                    "zdjp": "DXDM",
                    "xqzs": "1",
                    "zddm": "dxdm",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "1791DFCBA8D24121AE9F3C3B2D536756",
                    "kzxx": {
                        "yzgz": {
                            "insert1": {"zdpd": {"value": "SYS_SJGL_SJDX_DXDMCQ", "ts": "对象代码不能重复"}},
                            "update": {"notNull": {}, "length": {"min": 0, "max": 256}, "xxms": "对象代码"},
                            "insert": {"notNull": {}, "length": {"min": 0, "max": 256}, "xxms": "对象代码"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 1},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 1},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 1},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 1},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 1},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 1}
                        }
                    },
                    "lbzs": "1",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "hdyzgz": "zdpd:SYS_SJGL_SJDX_DXDMCQ:对象代码不能重复",
                    "zdms": "代表唯一数据对象，多处使用，唯一",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGDAIMA",
                    "zdcd": 256,
                    "bjbt": "1",
                    "cxms": "4",
                    "gxsj": "20211231175828",
                    "yxx": "1",
                    "zdmc": "对象代码",
                    "zdkd": 1,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxmc": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 100,
                    "zdjp": "DXMC",
                    "xqzs": "1",
                    "zddm": "dxmc",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "11F61230810B49B58632DFA903ACCC9C",
                    "kzxx": {
                        "yzgz": {
                            "update": {"notNull": {}, "length": {"min": 0, "max": 128}, "xxms": "对象名称"},
                            "insert": {"notNull": {}, "length": {"min": 0, "max": 128}, "xxms": "对象名称"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 1},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 1},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 1},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 1},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 1},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 1}
                        }
                    },
                    "lbzs": "1",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGMINGCHENG",
                    "zdcd": 128,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20181216152320",
                    "yxx": "1",
                    "zdmc": "对象名称",
                    "zdkd": 1,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxztlx": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 110,
                    "zdjp": "DXZTLX",
                    "xqzs": "0",
                    "zddm": "dxztlx",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$select",
                    "id": "BE07EAECA1B14074840607D13D91C5C8",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_DXZTLX"},
                                "xxms": "对象载体类型"
                            },
                            "insert": {
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_DXZTLX"},
                                "xxms": "对象载体类型"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "insert": {
                                "default": "oracle",
                                "readonly": false,
                                "show": false,
                                "disabled": true,
                                "zdkd": 80
                            },
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "zdzdlb": "SYS_SJGL_DXZTLX",
                    "xzmrz": "oracle",
                    "zdms": "ORACLE、POSTGRESS、MYSQL、GREEPLAM、本地文件、FTP文件、FASTDFS文件、SOLR、ES、HIVE、HBASE等",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "DUIXIANGZAITILEIXING",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190116164607",
                    "yxx": "1",
                    "zdmc": "对象载体类型",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "px": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 120,
                    "zdjp": "PX",
                    "xqzs": "1",
                    "zddm": "px",
                    "zdlx": "NUMBER",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "B13DA5C5E54642A0B0795DC64C4DAC36",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "number": {},
                                "notNull": {},
                                "length": {"min": 0, "max": 22},
                                "xxms": "排序"
                            }, "insert": {"number": {}, "notNull": {}, "length": {"min": 0, "max": 22}, "xxms": "排序"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 81},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 81},
                            "insert": {
                                "default": "99999",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 81
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 81},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 81},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 81}
                        }
                    },
                    "lbzs": "1",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "xzmrz": "99999",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "PAIXU",
                    "zdcd": 22,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20190328154018",
                    "yxx": "1",
                    "zdmc": "排序",
                    "zdkd": 81,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxms": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 130,
                    "zdjp": "DXMS",
                    "xqzs": "1",
                    "zddm": "dxms",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "C7C60D1E8D6D45D194F1694FC3373256",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 4000}, "xxms": "对象描述"},
                            "insert": {"length": {"min": 0, "max": 4000}, "xxms": "对象描述"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGMIAOSHU",
                    "zdcd": 4000,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181216154706",
                    "yxx": "1",
                    "zdmc": "对象描述",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxzy": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "1",
                    "zddx": "1",
                    "px": 140,
                    "zdjp": "DXZY",
                    "xqzs": "1",
                    "zddm": "dxzy",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$checkbox",
                    "id": "7F242AE208054131BB6095472B1E2B17",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 512}, "xxms": "对象作用"},
                            "insert": {"length": {"min": 0, "max": 512}, "xxms": "对象作用"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_SJGL_DXZY",
                    "zdms": "增量导出、数据插入、数据更新、页面查询、页面新增、页面修改等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGZUOYONG",
                    "zdcd": 512,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211218003202",
                    "yxx": "1",
                    "zdmc": "对象作用",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxlb": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 150,
                    "zdjp": "DXLB",
                    "xqzs": "1",
                    "zddm": "dxlb",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$select",
                    "id": "C1A47EAAC6DF444AADF6AE7CEF2261EB",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "notNull": {},
                                "length": {"min": 0, "max": 512},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_DXLB"},
                                "xxms": "对象类别"
                            },
                            "insert": {
                                "notNull": {},
                                "length": {"min": 0, "max": 512},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_DXLB"},
                                "xxms": "对象类别"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"default": "02", "readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_SJGL_DXLB",
                    "xzmrz": "02",
                    "zdms": "标准对象、临时对象、系统对象等人工分类",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGLEIBIE",
                    "zdcd": 512,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20181214144918",
                    "yxx": "1",
                    "zdmc": "对象类别",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxlx": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 160,
                    "zdjp": "DXLX",
                    "xqzs": "1",
                    "zddm": "dxlx",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$select",
                    "id": "1F0D95F5C7934A9A9672B43DAAC1CFAD",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "notNull": {},
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_DXLX"},
                                "xxms": "对象类型"
                            },
                            "insert": {
                                "notNull": {},
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_DXLX"},
                                "xxms": "对象类型"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {
                                "default": "table",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 80
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_SJGL_DXLX",
                    "xzmrz": "table",
                    "zdms": "表、视图、excel、txt等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGLEIXING",
                    "zdcd": 32,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20201022094128",
                    "yxx": "1",
                    "zdmc": "对象类型",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxzt": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "1",
                    "zddx": "0",
                    "px": 170,
                    "zdjp": "SJZT",
                    "xqzs": "1",
                    "zddm": "dxzt",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$select",
                    "id": "AFF62B502F2F4479A223C4D4FC604268",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "notNull": {},
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": false, "value": "SYS_COMMON_SJZT"},
                                "xxms": "数据载体"
                            },
                            "insert": {
                                "notNull": {},
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": false, "value": "SYS_COMMON_SJZT"},
                                "xxms": "数据载体"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {
                                "default": "default",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 80
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "1",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_COMMON_SJZT",
                    "xzmrz": "default",
                    "zdms": "根据对象载体类型不同，表示不同的具体载体，如具体数据库、ftp等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "SHUJUZAITI",
                    "zdcd": 32,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20210806162427",
                    "yxx": "1",
                    "zdmc": "数据载体",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxgs": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 180,
                    "zdjp": "DXGS",
                    "xqzs": "1",
                    "zddm": "dxgs",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "42DDB02C6BE54F3287E966536A28FF36",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 512}, "xxms": "对象归属"},
                            "insert": {"length": {"min": 0, "max": 512}, "xxms": "对象归属"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "数据库的某个用户、ftp的目录等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGGUISHU",
                    "zdcd": 512,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "对象归属",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "jtdx": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 190,
                    "zdjp": "JTDX",
                    "xqzs": "1",
                    "zddm": "jtdx",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "8C8F79468C5440CE958976F2F1D8B2F7",
                    "kzxx": {
                        "yzgz": {
                            "update": {"notNull": {}, "length": {"min": 0, "max": 64}, "xxms": "具体对象"},
                            "insert": {"notNull": {}, "length": {"min": 0, "max": 64}, "xxms": "具体对象"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 1},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 1},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 1},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 1},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 1},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 1}
                        }
                    },
                    "lbzs": "1",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "数据库的表视图、文件名、文件正则等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "JUTIDUIXIANG",
                    "zdcd": 64,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20181216152959",
                    "yxx": "1",
                    "zdmc": "具体对象",
                    "zdkd": 1,
                    "zydj": "06",
                    "mbzs": "1"
                }, "zjzd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 200,
                    "zdjp": "ZJZD",
                    "xqzs": "1",
                    "zddm": "zjzd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "225C33985D934B8C86EA09F0602E3A15",
                    "kzxx": {
                        "yzgz": {
                            "update": {"notNull": {}, "length": {"min": 0, "max": 32}, "xxms": "主键字段"},
                            "insert": {"notNull": {}, "length": {"min": 0, "max": 32}, "xxms": "主键字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"default": "id", "readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "xzmrz": "id",
                    "zdms": "用于查询详情等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "ZHUJIANZIDUAN",
                    "zdcd": 32,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20201022094128",
                    "yxx": "1",
                    "zdmc": "主键字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "qxzd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 210,
                    "zdjp": "QXZD",
                    "xqzs": "1",
                    "zddm": "qxzd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "85A238DFA38C4635BF0A122D531652EC",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "权限字段"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "权限字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "进行权限控制",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "QUANXIANZIDUAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "权限字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "zlzd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 220,
                    "zdjp": "ZLZD",
                    "xqzs": "1",
                    "zddm": "zlzd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "6252549EFB054AF092F56255A9B818BF",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "增量字段"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "增量字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {
                                "default": "gxsj",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 80
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "xzmrz": "gxsj",
                    "zdms": "进行增量抽取等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "ZENGLIANGZIDUAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190120154002",
                    "yxx": "1",
                    "zdmc": "增量字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "qczd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 230,
                    "zdjp": "QZZD",
                    "xqzs": "1",
                    "zddm": "qczd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "F645E558F568423FB6F3DFBF653133E5",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "去重字段"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "去重字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "用于插入更新",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "QUZHONGZIDUAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "去重字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "jyzd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 240,
                    "zdjp": "XYZD",
                    "xqzs": "1",
                    "zddm": "jyzd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "E31106B570C34EFA99DA7A5D0ECD3A68",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "校验字段"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "校验字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "用于存放校验信息",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "XIAOYANZIDUAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "校验字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "pczd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 250,
                    "zdjp": "PCZD",
                    "xqzs": "1",
                    "zddm": "pczd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "30C8FA8F466049C7B5B18D7EA6E4455C",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "批次字段"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "批次字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "用于存放批次号，知道具体数据来自哪次抽取等",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "PICIZIDUAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190120154002",
                    "yxx": "1",
                    "zdmc": "批次字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "yxxzd": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 270,
                    "zdjp": "WXZD",
                    "xqzs": "1",
                    "zddm": "yxxzd",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "6DA0E66189F24622A5D638D7471ABE75",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "有效性字段"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "有效性字段"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {
                                "default": "yxx",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 80
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "xzmrz": "yxx",
                    "zdms": "忘了",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "WUXIAOZIDUAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190120154002",
                    "yxx": "1",
                    "zdmc": "有效性字段",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "fytjxs": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 280,
                    "zdjp": "FYTJXS",
                    "xqzs": "1",
                    "zddm": "fytjxs",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$select",
                    "id": "D2C8B252E66749AD88DE11F7AEA41CAD",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "notNull": {},
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_FYTJXS"},
                                "xxms": "分页统计形式"
                            },
                            "insert": {
                                "notNull": {},
                                "length": {"min": 0, "max": 32},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_FYTJXS"},
                                "xxms": "分页统计形式"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"default": "1", "readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_SJGL_FYTJXS",
                    "xzmrz": "1",
                    "zdms": "控制分页查询数据量统计形式，如：默认形式、异步统计、默认不统计",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "FENYETONGJIXINGSHI",
                    "zdcd": 32,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "分页统计形式",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "mrpx": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 290,
                    "zdjp": "MRPX",
                    "xqzs": "1",
                    "zddm": "mrpx",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "0458F1D14CFA4F129A7C3D4080E53C3A",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 256}, "xxms": "默认排序"},
                            "insert": {"length": {"min": 0, "max": 256}, "xxms": "默认排序"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {
                                "default": "px,gxsj desc",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 80
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "xzmrz": "px,gxsj desc",
                    "zdms": "查询时的默认排序规则",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "MORENPAIXU",
                    "zdcd": 256,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20191024114557",
                    "yxx": "1",
                    "zdmc": "默认排序",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cscx": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 300,
                    "zdjp": "CSCX",
                    "xqzs": "1",
                    "zddm": "cscx",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$switch",
                    "id": "98022689F9964C5EB01277D7CDDEF414",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "length": {"min": 0, "max": 32},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "初始查询"
                            },
                            "insert": {
                                "length": {"min": 0, "max": 32},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "初始查询"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_COMMON_LJPD",
                    "zdms": "控制查询页面是否初始化查询",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "CHUSHICHAXUN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "初始查询",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cxtj": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 320,
                    "zdjp": "CXTJ",
                    "xqzs": "1",
                    "zddm": "cxtj",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$switch",
                    "id": "9B67E5497FEF41B59F6532E58F406380",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "查询条件"
                            },
                            "insert": {
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "查询条件"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_COMMON_LJPD",
                    "zdms": "控制是否展示",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "CHAXUNTIAOJIAN",
                    "zdcd": 8,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "查询条件",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "sxxz": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 330,
                    "zdjp": "SXXZ",
                    "xqzs": "1",
                    "zddm": "sxxz",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$switch",
                    "id": "79D6A31B437D42BC924402FB75F00E98",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "属性选择"
                            },
                            "insert": {
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "属性选择"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_COMMON_LJPD",
                    "zdms": "控制是否展示",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "SHUXINGXUANZE",
                    "zdcd": 8,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "属性选择",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "bjxh": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 340,
                    "zdjp": "BJXH",
                    "xqzs": "1",
                    "zddm": "bjxh",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$switch",
                    "id": "00B7DB2CE2AB4000B8D7CA38B38C4101",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "编辑序号"
                            },
                            "insert": {
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "编辑序号"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_COMMON_LJPD",
                    "zdms": "控制是否显示编辑页面的字段序号",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "BIANJIXUHAO",
                    "zdcd": 8,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181211224727",
                    "yxx": "1",
                    "zdmc": "编辑序号",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "ljq": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 350,
                    "zdjp": "LJQ",
                    "xqzs": "1",
                    "zddm": "ljq",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "91356492ACB7420096C3B148DA2DEFEB",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 512}, "xxms": "拦截器"},
                            "insert": {"length": {"min": 0, "max": 512}, "xxms": "拦截器"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "后台扩展系统",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "LANJIEQI",
                    "zdcd": 512,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20220405151725",
                    "yxx": "1",
                    "zdmc": "拦截器",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "zddrsql": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 360,
                    "zdjp": "ZDDRSQL",
                    "xqzs": "1",
                    "zddm": "zddrsql",
                    "zdlx": "CLOB",
                    "cjsj": "20181211224727",
                    "zdts": "支持如下方式导入字段信息：\n1、获取默认sql或自定义sql导入字段信息。\n2、直接按如下格式配置字段信息,一行一个字段，依次为：字段代码,字段描述,字段类型,字段长度：\nxm|姓名|VARCHAR2|100\nzjhm|证件号码|VARCHAR2|100\nzjlx|证件类型@SYS_COMMON_ZJLX|VARCHAR2|100",
                    "kjlx": "MyMarkDown",
                    "id": "F5077DF906A845A0881E1AE96123AEBF",
                    "kzxx": {
                        "kjkz": {
                            "span": 20,
                            "itemRender": {"props": {"codeType": "sql", "leftToolbar": "0", "rightToolbar": "0"}}
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 4000}, "xxms": "字段导入SQL"},
                            "insert": {"length": {"min": 0, "max": 4000}, "xxms": "字段导入SQL"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "用于刷新对象字段",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "ZIDUANDAORUSQL",
                    "zdcd": 4000,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211208163957",
                    "yxx": "1",
                    "zdmc": "字段导入SQL",
                    "style": "height:200px;width: calc(100% - 100px);  display: inline;",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "myHqmrsql": {
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 362,
                    "zdjp": "HQMRSQL",
                    "xqzs": "0",
                    "zddm": "myHqmrsql",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211111002420",
                    "zdywlb": "99",
                    "kjlx": "$buttons",
                    "id": "22E59A583A5141B6A86072027772F5DE",
                    "kzxx": {
                        "kjkz": {
                            "title": "",
                            "align": "left",
                            "span": 4,
                            "btns": {
                                "dis": {
                                    "type": "button",
                                    "content": "获取默认SQL",
                                    "status": "primary",
                                    "buttonOptions": {"htqqts": false, "sfjybd": true, "jghbdbd": true}
                                }
                            }
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "获取默认sql"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "获取默认sql"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "insert": {"readonly": false, "show": true, "disabled": true, "zdkd": 130},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 130}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "HUOQUMORENSQL",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206232651",
                    "yxx": "1",
                    "zdmc": "获取默认sql",
                    "zdkd": 130,
                    "zydj": "06",
                    "mbzs": "0"
                }, "sqlmb": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 370,
                    "zdjp": "SQLMB",
                    "xqzs": "1",
                    "zddm": "sqlmb",
                    "zdlx": "CLOB",
                    "cjsj": "20181211224727",
                    "zdts": "具体使用方法请参考帮助文档",
                    "kjlx": "MyMarkDown",
                    "id": "49E9745E2F0D4B46AF2C6FB4C15D9050",
                    "kzxx": {
                        "kjkz": {
                            "itemRender": {
                                "props": {
                                    "codeType": "sql",
                                    "leftToolbar": "0",
                                    "rightToolbar": "0"
                                }
                            }
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 4000}, "xxms": "SQL模板"},
                            "insert": {"length": {"min": 0, "max": 4000}, "xxms": "SQL模板"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "insert": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdms": "用于扩展后端，基于beetl实现",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "SQLMOBAN",
                    "zdcd": 4000,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20220610094935",
                    "yxx": "1",
                    "zdmc": "SQL模板",
                    "style": "height:200px",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cjsj": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 410,
                    "zdjp": "CJSJ",
                    "xqzs": "1",
                    "zddm": "cjsj",
                    "cxmrz": "{\"start\":{\"method\":\"subtract\",\"dw\":\"day\",\"value\":\"30\"},\"end\":{\"method\":\"add\",\"dw\":\"day\",\"value\":\"1\"}}",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "ElDatePicker",
                    "id": "DEF6643C4553418A9A5413B3E88D5351",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "date": {"value": "yyyyMMddHHmmss"},
                                "length": {"min": 0, "max": 14},
                                "xxms": "创建时间"
                            },
                            "insert": {
                                "date": {"value": "yyyyMMddHHmmss"},
                                "length": {"min": 0, "max": 14},
                                "xxms": "创建时间"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 130},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 130},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 130},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 130}
                        }
                    },
                    "lbzs": "0",
                    "gshff": "vueTimeGsh",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "hdyzgz": "date:yyyyMMddHHmmss",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "CHUANGJIANSHIJIAN",
                    "zdcd": 14,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181217175132",
                    "yxx": "1",
                    "zdmc": "创建时间",
                    "fgshff": "vueTimeFgsh",
                    "zdkd": 130,
                    "zydj": "06",
                    "mbzs": "1"
                }, "gxsj": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 420,
                    "zdjp": "GXSJ",
                    "xqzs": "1",
                    "zddm": "gxsj",
                    "cxmrz": "{\"start\":{\"method\":\"subtract\",\"dw\":\"day\",\"value\":\"30\"},\"end\":{\"method\":\"add\",\"dw\":\"day\",\"value\":\"1\"}}",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "ElDatePicker",
                    "id": "7D0E4866234C4E90A30D44A12A8A9256",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "date": {"value": "yyyyMMddHHmmss"},
                                "length": {"min": 0, "max": 14},
                                "xxms": "更新时间"
                            },
                            "insert": {
                                "date": {"value": "yyyyMMddHHmmss"},
                                "length": {"min": 0, "max": 14},
                                "xxms": "更新时间"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 130},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 130},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 130},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 130}
                        }
                    },
                    "lbzs": "1",
                    "gshff": "vueTimeGsh",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "1",
                    "yxbj": "0",
                    "hdyzgz": "date:yyyyMMddHHmmss",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "GENGXINSHIJIAN",
                    "zdcd": 14,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190510173953",
                    "yxx": "1",
                    "zdmc": "更新时间",
                    "fgshff": "vueTimeFgsh",
                    "zdkd": 130,
                    "zydj": "06",
                    "mbzs": "1"
                }, "zydj": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 430,
                    "zdjp": "ZYDJ",
                    "xqzs": "1",
                    "zddm": "zydj",
                    "zdlx": "NUMBER",
                    "cjsj": "20200424184149",
                    "kjlx": "$select",
                    "id": "E27A3E01678F4A00A9EF23F17690DE75",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "number": {},
                                "length": {"min": 0, "max": 8},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_ZYGJ"},
                                "xxms": "资源等级"
                            },
                            "insert": {
                                "number": {},
                                "length": {"min": 0, "max": 8},
                                "zd": {"zddx": false, "zszdx": true, "value": "SYS_SJGL_ZYGJ"},
                                "xxms": "资源等级"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"default": "06", "readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_SJGL_ZYGJ",
                    "xzmrz": "06",
                    "zdms": "从高到低为1~10，按每三级分为高中低，一些较少使用的资源就设置为10级，10级默认不使用，需要使用时，需要特殊配置，系统默认6级",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "ZIYUANDENGJI",
                    "zdcd": 8,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20201022094136",
                    "yxx": "1",
                    "zdmc": "资源等级",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "yxx": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 440,
                    "zdjp": "YXX",
                    "xqzs": "1",
                    "zddm": "yxx",
                    "cxmrz": "1",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$switch",
                    "id": "862C5DD21DFD4E03ABE18D5431A34369",
                    "kzxx": {
                        "yzgz": {
                            "update": {
                                "notNull": {},
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "有效性"
                            },
                            "insert": {
                                "notNull": {},
                                "length": {"min": 0, "max": 8},
                                "zd": {"value": "SYS_COMMON_LJPD"},
                                "xxms": "有效性"
                            }
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "insert": {"default": "1", "readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "zdzdlb": "SYS_COMMON_LJPD",
                    "xzmrz": "1",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "YOUXIAOXING",
                    "zdcd": 8,
                    "bjbt": "1",
                    "cxms": "1",
                    "gxsj": "20211214223151",
                    "yxx": "1",
                    "zdmc": "有效性",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "kzxx": {
                    "cxzs": "1",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 450,
                    "zdjp": "KZXX",
                    "xqzs": "1",
                    "zddm": "kzxx",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "jsoneditor",
                    "id": "19B3B82F6D144DF1AB270F3CFA0145F4",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 4000}, "xxms": "扩展信息"},
                            "insert": {"length": {"min": 0, "max": 4000}, "xxms": "扩展信息"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "insert": {
                                "default": "{\n  \"sys\":{\n    \"fields\": {\n      \"cjrdm\": {\n        \"zdms\": \"创建人代码\",\n        \"zddm\": \"cjrdm\"\n      },\n      \"cjrxm\": {\n        \"zdms\": \"创建人名称\",\n        \"zddm\": \"cjrxm\"\n      },\n      \"cjrdwdm\": {\n        \"zdms\": \"创建人单位代码\",\n        \"zddm\": \"cjrdwdm\"\n      },\n      \"cjrdwmc\": {\n        \"zdms\": \"创建人单位名称\",\n        \"zddm\": \"cjrdwmc\"\n      }\n    }\n  }\n}",
                                "readonly": false,
                                "show": true,
                                "disabled": false,
                                "zdkd": 2
                            },
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "xzmrz": "{\n  \"sys\":{\n    \"fields\": {\n      \"cjrdm\": {\n        \"zdms\": \"创建人代码\",\n        \"zddm\": \"cjrdm\"\n      },\n      \"cjrxm\": {\n        \"zdms\": \"创建人名称\",\n        \"zddm\": \"cjrxm\"\n      },\n      \"cjrdwdm\": {\n        \"zdms\": \"创建人单位代码\",\n        \"zddm\": \"cjrdwdm\"\n      },\n      \"cjrdwmc\": {\n        \"zdms\": \"创建人单位名称\",\n        \"zddm\": \"cjrdwmc\"\n      }\n    }\n  }\n}",
                    "zdms": "JSON格式",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "KUOZHANXINXI",
                    "zdcd": 4000,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211201000138",
                    "yxx": "1",
                    "zdmc": "扩展信息",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cjrxm": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 460,
                    "zdjp": "CJRXM",
                    "xqzs": "1",
                    "zddm": "cjrxm",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "6D4C2E8FDF144C33A052F73A5023FC70",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 64}, "xxms": "创建人姓名"},
                            "insert": {"length": {"min": 0, "max": 64}, "xxms": "创建人姓名"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "CHUANGJIANRENXINGMING",
                    "zdcd": 64,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20201022094136",
                    "yxx": "1",
                    "zdmc": "创建人姓名",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cjrdm": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 470,
                    "zdjp": "CJRDM",
                    "xqzs": "0",
                    "zddm": "cjrdm",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "74C136CED3234539BE0C9BBCCA0B681E",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "创建人代码"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "创建人代码"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "zdzdlb": "SYS_COMMON_USER",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "CHUANGJIANRENDAIMA",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190420114426",
                    "yxx": "1",
                    "zdmc": "创建人代码",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cjrdwmc": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 480,
                    "zdjp": "CJRDWMC",
                    "xqzs": "1",
                    "zddm": "cjrdwmc",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "46EC9E47E5B7495BAB3CA82CE1303467",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 256}, "xxms": "创建人单位名称"},
                            "insert": {"length": {"min": 0, "max": 256}, "xxms": "创建人单位名称"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "CHUANGJIANRENDANWEIMINGCHENG",
                    "zdcd": 256,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181217175132",
                    "yxx": "1",
                    "zdmc": "创建人单位名称",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "cjrdwdm": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 490,
                    "zdjp": "CJRDWDM",
                    "xqzs": "0",
                    "zddm": "cjrdwdm",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "BABA0BA35E82425DAB5EBC6ED313FE1C",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "创建人单位代码"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "创建人单位代码"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "zdzdlb": "SYS_COMMON_ORG",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "CHUANGJIANRENDANWEIDAIMA",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190420114457",
                    "yxx": "1",
                    "zdmc": "创建人单位代码",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxjp": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 500,
                    "zdjp": "DXJP",
                    "xqzs": "1",
                    "zddm": "dxjp",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "3A4BA1700D9A4BEE94A0C39EBD07C719",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 512}, "xxms": "对象简拼"},
                            "insert": {"length": {"min": 0, "max": 512}, "xxms": "对象简拼"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 80},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 80},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 80},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 80},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 80}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGJIANPIN",
                    "zdcd": 512,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20181219142757",
                    "yxx": "1",
                    "zdmc": "对象简拼",
                    "zdkd": 80,
                    "zydj": "06",
                    "mbzs": "1"
                }, "dxqp": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 510,
                    "zdjp": "DXQP",
                    "xqzs": "1",
                    "zddm": "dxqp",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "24008D5E8ED94CF5A5514372D666B17C",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 512}, "xxms": "对象全拼"},
                            "insert": {"length": {"min": 0, "max": 512}, "xxms": "对象全拼"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 2},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": true, "disabled": true, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "DUIXIANGQUANPIN",
                    "zdcd": 512,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20190618191343",
                    "yxx": "1",
                    "zdmc": "对象全拼",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "1"
                }, "id": {
                    "cxzs": "1",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 520,
                    "zdjp": "ZJ",
                    "xqzs": "1",
                    "zddm": "id",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181211224727",
                    "kjlx": "$input",
                    "id": "07997AE0327B4C39826FEAD22836EB9D",
                    "kzxx": {
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 32}, "xxms": "主键"},
                            "insert": {"length": {"min": 0, "max": 32}, "xxms": "主键"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}},
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 2},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": true, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": true, "zdkd": 2}
                        }
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "ZHUJIAN",
                    "zdcd": 32,
                    "bjbt": "0",
                    "cxms": "4",
                    "gxsj": "20210312101222",
                    "yxx": "1",
                    "zdmc": "主键",
                    "zdkd": 2,
                    "zydj": "10",
                    "mbzs": "0"
                }, "myLbcz": {
                    "zddm": "myLbcz",
                    "px": 530,
                    "zdkd": 165,
                    "zdmc": "操作",
                    "zdywlb": "99",
                    "kjlx": "$buttons",
                    "lbzs": "1",
                    "kzxx": {
                        "kjkz": {
                            "fixed": "right", "zdans": 3, "btns": {
                                "update": {
                                    "type": "text",
                                    "content": "编辑",
                                    "status": "primary",
                                    "buttonOptions": {"clfs": "tcck", "tcqp": true}
                                },
                                "delete": {
                                    "type": "text",
                                    "content": "删除",
                                    "status": "primary",
                                    "buttonOptions": {"clfs": "htqq", "htqqts": true}
                                },
                                "dxjcxx": {
                                    "type": "text",
                                    "content": "详情",
                                    "status": "primary",
                                    "buttonOptions": {"clfs": "tcck", "tcqp": true}
                                },
                                "fzjl": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "复制",
                                    "buttonOptions": {
                                        "clfs": "htqq",
                                        "htqqts": true,
                                        "params": {"sys": {"zxcz": "plcl"}}
                                    }
                                },
                                "sxdx": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "刷新对象",
                                    "buttonOptions": {"htqqts": true}
                                },
                                "bzpx": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "标准排序",
                                    "buttonOptions": {"htqqts": true}
                                },
                                "fzdx": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "复制对象",
                                    "buttonOptions": {"htqqts": true}
                                },
                                "scdxst": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "生成对象实体",
                                    "buttonOptions": {"htqqts": true}
                                },
                                "ckdx": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "查看对象",
                                    "buttonOptions": {
                                        "clfs": "tcck",
                                        "tcqp": true,
                                        "dxjcxx": {"sys": {"cllx": "select"}},
                                        "jcxxqqcskz": {"sjdx.dxdm": "dxjcxx.obj.dxdm"}
                                    }
                                },
                                "zdpz": {
                                    "type": "text",
                                    "status": "primary",
                                    "content": "字段配置",
                                    "buttonOptions": {
                                        "clfs": "tcck",
                                        "tcqp": true,
                                        "dxjcxx": {"sjdx": {"dxdm": "SYS_SJGL_SJZD"}, "sys": {"cllx": "select"}},
                                        "jcxxkz": {"fields.sjdx.cxmrz": "dxjcxx.obj.id"}
                                    }
                                }
                            }
                        },
                        "cllxkz": {
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 165},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 165},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 165},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 165},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 165},
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 165}
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "操作"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "操作"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "CZ",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211127183656",
                    "id": "895E4389D69A4BD0820171024FC16C98",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "CAOZUO",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206235413",
                    "yxx": "1",
                    "zydj": "06",
                    "mbzs": "0"
                }, "myCxan": {
                    "zddm": "myCxan",
                    "px": 530,
                    "zdywlb": "99",
                    "cxzs": "1",
                    "kjlx": "$buttons",
                    "kzxx": {
                        "cllxkz": {
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 165},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 165},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 165},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 165},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 165},
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 165}
                        },
                        "kjkz": {
                            "title": "",
                            "align": "right",
                            "span": 24,
                            "btns": {
                                "cxan": {
                                    "type": "submit",
                                    "content": "Query",
                                    "status": "primary",
                                    "buttonOptions": {},
                                    "name": "cxan"
                                }
                            }
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "查询按钮"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "查询按钮"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "CXAN",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181214210403",
                    "id": "3113272B5A7E41E9B357C2C0A2970C8E",
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "CHAXUNANNIU",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206231544",
                    "yxx": "1",
                    "zdmc": "查询按钮",
                    "zdkd": 165,
                    "zydj": "06",
                    "mbzs": "0"
                }, "myZdpz": {
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "px": 535,
                    "zdjp": "ZDPZ",
                    "xqzs": "1",
                    "zddm": "myZdpz",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20181221220600",
                    "zdywlb": "99",
                    "kjlx": "MySelectGrid",
                    "id": "3F120C100FDF423AB7B694146C7DD65C",
                    "kzxx": {
                        "kjkz": {
                            "title1": "",
                            "itemRender": {
                                "props": {
                                    "glzd": "sjdx",
                                    "dxjcxx": {"sjdx": {"dxdm": "SYS_SJGL_SJZD"}, "sys": {"cllx": "select"}}
                                }
                            }
                        },
                        "cllxkz": {
                            "select": {"readonly": true, "show": false, "disabled": false, "zdkd": 2},
                            "insert": {"readonly": false, "show": false, "disabled": false, "zdkd": 2},
                            "update": {"readonly": false, "show": true, "disabled": false, "zdkd": 2},
                            "dxjcxx": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dcsj": {"readonly": true, "show": true, "disabled": false, "zdkd": 2},
                            "dcmb": {"readonly": true, "show": false, "disabled": false, "zdkd": 2}
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 400000}, "xxms": "字段配置"},
                            "insert": {"length": {"min": 0, "max": 400000}, "xxms": "字段配置"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "1",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "ZIDUANPEIZHI",
                    "zdcd": 400000,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211208172638",
                    "yxx": "1",
                    "zdmc": "字段配置",
                    "zdkd": 2,
                    "zydj": "06",
                    "mbzs": "0"
                }, "myXjan": {
                    "zddm": "myXjan",
                    "px": 540,
                    "zdywlb": "99",
                    "kjlx": "$buttons",
                    "kzxx": {
                        "cllxkz": {
                            "insert": {"show": true, "readonly": false, "disabled": true, "zdkd": 130},
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 130}
                        },
                        "kjkz": {
                            "title": "",
                            "align": "center",
                            "span": 24,
                            "btns": {
                                "insert": {
                                    "type": "button",
                                    "content": "提交",
                                    "status": "primary",
                                    "buttonOptions": {"htqqts": true, "sfgbtc": true}
                                },
                                "gb": {
                                    "type": "button",
                                    "content": "关闭",
                                    "buttonOptions": {"clfs": "gbtc", "sfjybd": false, "sfsxym": false}
                                }
                            }
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "新建按钮"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "新建按钮"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "cxzs": "0",
                    "xzzs": "1",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "XJAN",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211125165619",
                    "id": "E7BEE583004C4220B2FBA2728EA1B188",
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "XINJIANANNIU",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206231527",
                    "yxx": "1",
                    "zdmc": "新建按钮",
                    "zdkd": 130,
                    "zydj": "06",
                    "mbzs": "0"
                }, "myBjan": {
                    "zddm": "myBjan",
                    "px": 550,
                    "zdywlb": "99",
                    "kjlx": "$buttons",
                    "kzxx": {
                        "cllxkz": {
                            "update": {"show": true, "readonly": false, "disabled": true, "zdkd": 130},
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dcsj": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "dxjcxx": {"readonly": true, "show": false, "disabled": true, "zdkd": 130}
                        },
                        "kjkz": {
                            "title": "",
                            "align": "center",
                            "span": 24,
                            "btns": {
                                "update": {
                                    "type": "button",
                                    "content": "提交",
                                    "status": "primary",
                                    "buttonOptions": {"htqqts": true, "sfgbtc": true}
                                },
                                "gb": {
                                    "type": "button",
                                    "content": "关闭",
                                    "buttonOptions": {"clfs": "gbtc", "sfjybd": false, "sfsxym": false}
                                }
                            }
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "编辑按钮"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "编辑按钮"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "BJAN",
                    "xqzs": "0",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211114154930",
                    "id": "15E2AEF823ED46FEB7A505CFEAA4A195",
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "1",
                    "zdqp": "BIANJIANNIU",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206231518",
                    "yxx": "1",
                    "zdmc": "编辑按钮",
                    "zdkd": 130,
                    "zydj": "06",
                    "mbzs": "0"
                }, "myXqan": {
                    "zddm": "myXqan",
                    "px": 560,
                    "zdywlb": "99",
                    "kjlx": "$buttons",
                    "kzxx": {
                        "cllxkz": {
                            "dxjcxx": {"show": true, "readonly": true, "disabled": true, "zdkd": 130},
                            "insert": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "update": {"readonly": false, "show": false, "disabled": true, "zdkd": 130},
                            "dcsj": {"readonly": true, "show": true, "disabled": true, "zdkd": 130},
                            "dcmb": {"readonly": true, "show": false, "disabled": true, "zdkd": 130},
                            "select": {"readonly": true, "show": false, "disabled": true, "zdkd": 130}
                        },
                        "kjkz": {
                            "title": "",
                            "align": "center",
                            "span": 24,
                            "btns": {
                                "gb": {
                                    "type": "button",
                                    "content": "关闭",
                                    "buttonOptions": {"clfs": "gbtc", "sfjybd": false, "sfsxym": false}
                                }
                            }
                        },
                        "yzgz": {
                            "update": {"length": {"min": 0, "max": 0}, "xxms": "详情按钮"},
                            "insert": {"length": {"min": 0, "max": 0}, "xxms": "详情按钮"}
                        },
                        "zhgz": {"select": {"qdqhkg": {}}, "update": {"qdqhkg": {}}, "insert": {"qdqhkg": {}}}
                    },
                    "cxzs": "0",
                    "xzzs": "0",
                    "zdfy": "0",
                    "zddx": "0",
                    "zdjp": "XQAN",
                    "xqzs": "1",
                    "zdlx": "VARCHAR2",
                    "cjsj": "20211110193905",
                    "id": "1EC624619AE548ACADF56FF0FA7213F7",
                    "lbzs": "0",
                    "sjdx": "SYS_SJGL_SJDX",
                    "yxpx": "0",
                    "yxbj": "0",
                    "cxbt": "0",
                    "bjzs": "0",
                    "zdqp": "XIANGQINGANNIU",
                    "zdcd": 0,
                    "bjbt": "0",
                    "cxms": "1",
                    "gxsj": "20211206231455",
                    "yxx": "1",
                    "zdmc": "详情按钮",
                    "zdkd": 130,
                    "zydj": "06",
                    "mbzs": "0"
                }
            },
            "sys": {
                "authCode": "KFZFW_PTGL_SJDX",
                "fields": {
                    "sszzd": {"zddm": "dxmc", "zdms": "搜索主字段"},
                    "pyjp": {"zdms": "拼音简拼", "zddm": "dxjp"},
                    "pyqp": {"zdms": "拼音全拼", "zddm": "dxqp"},
                    "sszdlb": {"zdms": "搜索字段列表", "zddm": "dxms||dxdm"},
                    "cjrdm": {"zdms": "创建人代码", "zddm": "cjrdm"},
                    "cjrxm": {"zdms": "创建人名称", "zddm": "cjrxm"},
                    "cjrdwdm": {"zdms": "创建人单位代码", "zddm": "cjrdwdm"},
                    "cjrdwmc": {"zdms": "创建人单位名称", "zddm": "cjrdwmc"}
                },
                "cllx": "select"
            },
            "sjdx": {
                "bjxh": "1",
                "bjymkz": "<my-edit id=\"dataform1\" :fromdata=\"fromdata\" :sjdxid=\"sjdxid\" :pagemodel=\"pagemodel\">\n      <button type=\"button\" v-if=\"pagemodel=='add'\" class=\"btn btn-primary btn-sm\" \n    \t     title=\"获取默认字段导入SQL\" @click=\"plcl('dis')\">默认SQL</button>\n</my-edit>\n<script>\nfunction sjdxZdy(vp){\n\tvp.el=\"#dataform1\";\n    vp.methods.plcl=function(cllx){\n          this.$children[0].plcl(cllx);\n    }\n\tvp.methods.getDefaultImpSql=function(){\n        var _this=this;\n   \tif(!$(_this.$options.el).isValid()){\n    \t\treturn;\n    \t}else{\n                var _params = {myparams:JSON.stringify(_this.$children[0]._data.updatedata)};\n                _params['map[\"sqlType\"]']=\"dis\";\n                _params['id']=_this.sjdxid;\n    \t\t$.post(\"sjdx/getDefaultSql.do\",_params,function(data){\n    \t\t\tif(data.status){\n    \t\t\t    Vue.set(_this.fromdata,\"zddrsql\",data.msg);\n    \t\t\t}else{\n    \t        \tlayer.alert(data.msg ? data.msg : \"操作失败！\", { \n                           icon:2                      \n                    });\n                }\n            });\n        }\n    }\n}\nfunction sjdxZdyjy(data,_this){\n    if(!((_this.pagemodel=='edit'&&data.zddrsql==undefined)||!isEmpty(data.zddrsql))){\n        layer.alert(\"字段导入SQL不能为空\", { \n               icon:2                      \n        });\n        return false;\n    }\n    return true;\n}\n</script>                                               ",
                "cjrdm": "BC5D77315CA84C6C807988E3CD17E70D",
                "cjrdwdm": "141B1AFC7E634176BDA7DB7F491A9004",
                "cjrdwmc": "临时机构",
                "cjrxm": "系统管理员",
                "cjsj": "20181210203129",
                "cscx": "1",
                "cxlbkz": "<!-- 数据对象列表扩展 -->\n<my-list id=\"listPage\" :fromdata=\"fromdata\" :sjdxid=\"sjdxid\">\n\t<span :id=\"'lbplcz-'+sjdxid\"></span>\n</my-list>\n<!--  v-if=\"qxByQxm('FZDX')\"  v-if=\"qxByQxm('SCST')\" -->\n<!--         <button class=\"btn btn-primary btn-sm\" @click=\"plcl('fzdx')\"><i class=\"icon ion-ios-copy-outline\"></i> 复制对象</button> -->\n<!--         <button class=\"btn btn-primary btn-sm\" @click=\"plcl('scdxst')\"><i class=\"icon ion-log-in\"></i> 生成实体</button> -->\n<script type=\"text/x-tp\" id=\"my-sjdx-lbplcz-tp\">\n<span>\n{{if qxByQxm(user,sjdx,'FZDX')}}\n<button class=\"btn btn-primary btn-sm\" onclick=\"plcl('fzdx')\"><i class=\"icon ion-ios-copy-outline\"></i> {{= qxByQxm(user,sjdx,'FZDX').mc}}</button>\n{{/if}}\n{{if qxByQxm(user,sjdx,'SCDXST')}}\n<button class=\"btn btn-primary btn-sm\" onclick=\"plcl('scdxst')\"><i class=\"icon ion-log-in\"></i> {{= qxByQxm(user,sjdx,'SCDXST').mc}}</button>\n{{/if}}\n</span>\n</script>\n<script type=\"text/x-tp\" id=\"my-sjdx-lbcz-tp\">\n<span>\n{{if qxByQxm(user,sjdx,'CKDX')}}\n<span class=\"divider\"></span>\n<a href=\"sjdx/list.do?dxdm={{= row.dxdm}}\" target=\"_blank\" title=\"查看对象，查看列表页面\"><i class=\"icon ion-eye\"></i></a>\n{{/if}}\n{{if qxByQxm(user,sjdx,'SXDX')}}\n<span class=\"divider\"></span>\n<a href=\"sjdx/plcl.do?id={{= sjdx.id}}&e_cllx=sxdx&e_ids={{= row[sjdx.zjzd]}}\" \na-oper=\"dxcl\" title=\"刷新对象,从原始表信息更新字段\"><i class=\"icon ion-ios-refresh\"></i></a>\n{{/if}}\n</span>\n</script>\n<script>\nfunction sjdxZdy(vp){\n    vp.el=\"#listPage\";\n    vp.methods.plcl=function(cllx){\n          this.$children[0].plcl(cllx);\n    }\n    vp.mounted=function(){\n        lbplcz(this);\n    }\n}\nfunction plcl(cllx){\n    listFrom.plcl(cllx);\n}\nfunction lbplcz(_this){\n    var qtcz = $(\"#my-sjdx-lbplcz-tp\").tmpl({sjdx:_this.sjdx,\n        user:_this.$children[0]._data.user}).html();\n    $('#lbplcz-'+_this.sjdxid).html(qtcz);\n}\nfunction sjdxlbcz(value,_this) {\n    var cz = defaultLbcz(value,_this);\n    if(\"非列表模式\"==cz){\n        return cz;\n    }\n    var qtcz = $(\"#my-sjdx-lbcz-tp\").tmpl({sjdx:_this.$root.sjdx,\n        user:_this.$parent.$parent.$parent._data.user,\n\t    row:_this.$root.listPage.rows[_this.$parent.$parent.ri]}).html();\n    return cz+qtcz;\n}\n</script>",
                "cxtj": "1",
                "dxdm": "SYS_SJGL_SJDX",
                "dxjp": "XT-SJGL-SJDX",
                "dxlb": "02",
                "dxlx": "table",
                "dxmc": "系统-数据管理-数据对象",
                "dxms": "1",
                "dxqp": "XITONG-SHUJUGUANLI-SHUJUDUIXIANG",
                "dxzt": "default",
                "dxztlx": "mysql",
                "dxzy": "10,11",
                "fytjxs": "1",
                "gxsj": "20220517160536",
                "id": "SYS_SJGL_SJDX",
                "jtdx": "SYS_SJGL_SJDX",
                "kzxx": "{\n\t//系统参数\n  \"sys\": {\n\t\t\"wlsc\": true,\n    \"fields\": {\n      \"sszzd\": {\n        \"zddm\": \"dxmc\",\n        \"zdms\": \"搜索主字段\"\n      },\n      \"pyjp\": {\n        \"zdms\": \"拼音简拼\",\n        \"zddm\": \"dxjp\"\n      },\n      \"pyqp\": {\n        \"zdms\": \"拼音全拼\",\n        \"zddm\": \"dxqp\"\n      },\n      \"sszdlb\": {\n        \"zdms\": \"搜索字段列表\",\n        \"zddm\": \"dxms||dxdm\"\n      },\n      \"cjrdm\": {\n        \"zdms\": \"创建人代码\",\n        \"zddm\": \"cjrdm\"\n      },\n      \"cjrxm\": {\n        \"zdms\": \"创建人名称\",\n        \"zddm\": \"cjrxm\"\n      },\n      \"cjrdwdm\": {\n        \"zdms\": \"创建人单位代码\",\n        \"zddm\": \"cjrdwdm\"\n      },\n      \"cjrdwmc\": {\n        \"zdms\": \"创建人单位名称\",\n        \"zddm\": \"cjrdwmc\"\n      }\n    }\n  },\n\t\"cllxkz\": {\n\t\t\"select\": {\n\t\t\t\"qxpz\": {\n\t\t\t\t\"plclLeft\": {\n\t\t\t\t\t\"insert\": {\n\t\t\t\t\t\t\"content\": \"导入对象\"\n\t\t\t\t\t},\n\t\t\t\t\t\"scdxst\": {\n\t\t\t\t\t\t\"type\": \"primary\",\n\t\t\t\t\t\t\"content\": \"生成实体\",\n\t\t\t\t\t\t\"buttonOptions\": {\n\t\t\t\t\t\t\t\"clfs\": \"htqq\",\n\t\t\t\t\t\t\t\"htqqts\": true\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t},\n\t\t\t\t\"plclRight\": {}\n\t\t\t},\n\t\t\t\"dcwjmc\": \"数据对象导出文件名称自定义\",\n\t\t\t\"my-help\": \"42F03A5FE3B9425E98E46B52298485A2\"\n\t\t},\n\t\t\"update\": {\n\t\t\t\"my-help\": \"42F03A5FE3B9425E98E46B52298485A2\"\n\t\t}\n\t}\n}",
                "lbfxk": "0",
                "ljq": "cn.benma666.sjsj.ljq.sjgl.SjdxLjq",
                "mrpx": "px asc,gxsj desc",
                "px": 9999,
                "sqlmb": "select\n-- @pageTag(){\n    ${sql.selectField}\n-- @}\nfrom ${sql.from} t\n-- @ where(){\n   ${sql.whereStr} \n\t -- 需要权限则添加如下语句，不需要则删除\n\t ${sql.authWhere} \n-- @}",
                "sxxz": "1",
                "tails": {
                    "authCode": "KFZFW_PTGL_SJDX",
                    "kzxxObj": {
                        "sys": {
                            "wlsc": true,
                            "fields": {
                                "sszzd": {"zddm": "dxmc", "zdms": "搜索主字段"},
                                "pyjp": {"zdms": "拼音简拼", "zddm": "dxjp"},
                                "pyqp": {"zdms": "拼音全拼", "zddm": "dxqp"},
                                "sszdlb": {"zdms": "搜索字段列表", "zddm": "dxms||dxdm"},
                                "cjrdm": {"zdms": "创建人代码", "zddm": "cjrdm"},
                                "cjrxm": {"zdms": "创建人名称", "zddm": "cjrxm"},
                                "cjrdwdm": {"zdms": "创建人单位代码", "zddm": "cjrdwdm"},
                                "cjrdwmc": {"zdms": "创建人单位名称", "zddm": "cjrdwmc"}
                            }
                        },
                        "cllxkz": {
                            "select": {
                                "qxpz": {
                                    "plclLeft": {
                                        "insert": {"content": "导入对象"},
                                        "scdxst": {
                                            "type": "primary",
                                            "content": "生成实体",
                                            "buttonOptions": {"clfs": "htqq", "htqqts": true}
                                        }
                                    }, "plclRight": {}
                                }, "dcwjmc": "数据对象导出文件名称自定义", "my-help": "42F03A5FE3B9425E98E46B52298485A2"
                            }, "update": {"my-help": "42F03A5FE3B9425E98E46B52298485A2"}
                        }
                    },
                    "sjztObj": {
                        "fwq": "gxpt2_web",
                        "wlkj": "3",
                        "cjrxm": "系统管理员",
                        "px": 10,
                        "dm": "default",
                        "lx": "mysql",
                        "sjkl": "1",
                        "cjsj": "20181222194513",
                        "cjrdm": "BC5D77315CA84C6C807988E3CD17E70D",
                        "dzjdlx": "1",
                        "mc": "默认数据库",
                        "sflxtj": "0",
                        "id": "394A0D538356436E83D3F9AC390D1514",
                        "kzxx": "{}",
                        "mm": "q9oelsOfqG4=",
                        "ljc": "jdbc:mysql://111.229.217.7:3306/sjsj2?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&allowMultiQueries=true&serverTimezone=Asia/Shanghai",
                        "cjrdwdm": "141B1AFC7E634176BDA7DB7F491A9004",
                        "fwfs": "Native",
                        "cjrdwmc": "临时机构",
                        "gxsj": "20191211113629",
                        "yxx": "1",
                        "zt": "1",
                        "yhm": "sjsj2"
                    }
                },
                "xqan": "1",
                "yxx": "1",
                "yxxzd": "yxx",
                "zjzd": "id",
                "zlzd": "gxsj"
            },
            "yobj": {}
        }

        return { dxjcxx }
    },
    render() {
        return (<div><MyForm dxjcxx={this.dxjcxx} MyFormVNode={<>测试</>}></MyForm></div>)
    }
})
