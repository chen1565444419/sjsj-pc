import { computed, defineComponent, reactive, watch } from 'vue'

export default defineComponent({
    name: 'MyIconPicker',
    emits: ["update:modelValue"],
    setup(props, context) {
        const iconChange = (ev: any) => {
            console.log(ev)
            context.emit("update:modelValue", ev);
        }

        return { iconChange }
    },
    render() {
        return (
            <e-icon-picker onChange={this.iconChange} />

        )
    }
})
