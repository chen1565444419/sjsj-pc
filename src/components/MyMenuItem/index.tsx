import { ElMessage } from 'element-plus'
import { defineComponent } from 'vue'
import classes from './index.module.scss'



const MyMenuItem = defineComponent({
    name: 'MyMenuItem',
    props: {
        item: {
            type: Object,
            required: true
        },
    },
    emits: ["handleItemClick"],
    setup(props, context) {
        /**
         * 点击某个菜单时
         * @param data 菜单项
         */
        const selectMenu = (data: any) => {
            if (data.parent === 0) {
                switch (data.dzlx) {
                    case "01":
                    case "02":
                    case "03":
                    case "04":
                        // context.emit('handleItemClick', '12321312312321')
                        // context.emit("itemClick", `#/singleHome/sjdx?sys.authCode=$(data.dm)&pathName=$(data.name)`)
                        break
                    default:
                        ElMessage.error("暂不支持的地址类型：" + data.dzlx);
                }
            }
        }
        return {
            props,
            selectMenu
        }
    },
    render() {
        const handleRoute = () => {
            const { item } = this.props
            // 最后一层的情况，渲染菜单项
            if (!item.children || item.children.length === 0) {
                return <el-menu-item class={classes.menuItem} style="margin-top:3px" onClick={() => this.selectMenu(item)} index={item.dm}>
                    <e-icon icon-name={item.icon ? item.icon : "component Menu"} style="vertical-align: middle;margin-right: 5px;width: 24px;text-align: center;font-size: 18px;" />
                    <span>{item.mc}</span>
                </el-menu-item>
            }
            const slots = {
                title: () => {
                    return <>
                        <e-icon icon-name={item.icon ? item.icon : "component Menu"} style="vertical-align: middle;margin-right: 5px;width: 24px;text-align: center;font-size: 18px;" />
                        <span>{item.mc ? item.mc : '未定义菜单名称'}</span>
                    </>
                }
            }
            // 有children属性，递归
            return <el-sub-menu class={classes.menuItem} index={item.dm} v-slots={slots}>
                {item.children.map((child: any) => {
                    return <MyMenuItem item={child} key={child.dm}></MyMenuItem>
                })}
            </el-sub-menu>
        }
        return <>{handleRoute()}</>
    }
})

export default MyMenuItem