import { defineComponent, reactive, ref, watch } from 'vue'
import { ElMessage } from 'element-plus'
import axios from "@/axios";
import classes from './index.module.scss';
export default defineComponent({
    name: "MyFile",
    props: {
        //有的时候可能需要返回动态绑定到表单,如果没有，则可以不用(常用于编辑详情)
        modelValue: {
            type: String
        },
        /**
         * 这个不用了
         */
        showFile: {
            type: Boolean,
            default: true
        },
        //是否支持上传多个文件,不传默认为1个文件
        limitFlie: {
            type: Number,
            default: 1
        },
        //对象代码,文件提交的对象代码
        zdlb: {
            type: String,
        },
        disabled: {
            type: String
        },
        //处理类型,如果不传值，则默认为批量上传文件
        cllxValue: {
            type: String,
            default: "sjplsc"
        },
        readonly: {
            type: String
        },
        mbxzcs: {
            type: Object,
            default: {}
        },
        /**
         * 请求参数
         */
        qqcs:{
            type: Object
        }
    },
    //与父组件进项数据双向绑定
    emits: ['update:modelValue'],
    setup(props, context) {
        const myData: any = reactive({
            //保存上传文件的个数
            fileList: [],
            value: [],
            isdisabled: props.readonly || props.disabled,
            qqcs:{ "sys.authCode": "QTQX", "sys.cllx": "upload", "yobj.sjzt": 'default' }
        })
        // @ts-ignore
        myData.ctyd = window.WEB_CONFIG.ctdy;
        myData.urlRoot = axios.getBaseURL();
        /**
         * 当上传文件改变是触发，
         */
        const success = (response: any, file: any, fileList: any) => {
            let responseList = [] as any
            fileList.forEach((item: any) => {
                if (item.status === 'success') {
                    responseList.push(item.response.data[0])
                }
            })
            context.emit("update:modelValue", responseList)
        }

        const handleRemove = (file: any, fileList: any) => {
            let responseList = [] as any
            fileList.forEach((item: any) => {
                if (item.status === 'success') {
                    responseList.push(item.response.data[0])
                }
            })
            context.emit("update:modelValue", responseList)
        }
        /**
         * 文件超出限制的钩子函数
         */
        const handleExceed = (files: any, fileList: any) => {
            ElMessage.warning(`当前限制选择 ${props.limitFlie} 个文件，本次选择了 ${files.length} 个文件，共选择了 ${files.length} 个文件`);
        }
        /**
         * 自定义文件上传
         * @param params 文件参数
         */
        // const uploadSectionFile = (params: any) => {
        //     const file = params.file,
        //         fileType = file.type,
        //         isFile = fileType.indexOf("sheet") !== -1;
        //     // if(!isFile){
        //     //   ElMessage.error("只能上传xlsx格式文件！")
        //     //   return ;
        //     // }
        //     const form = new FormData();
        //     form.append("files", file);
        //     form.append("sys.cllx", "upload");
        //     form.append("sys.authCode", 'QTQX');
        //     form.append("yobj.sjzt", 'default');
        //     axios.post(form).then((res: any) => {
        //         if (res.status) {
        //             ElMessage.success('上传成功！');
        //         } else {
        //             ElMessage.error(res.msg)
        //         }
        //     })
        // }
        /**
         * 对文件进行清除
         */
        const uploadEle = ref(null) as any
        /**
         * 文件下载查看
         */
        const wjxzBtn = () => {
            if (props.modelValue === undefined || props.modelValue === '') {
                ElMessage.error("没有数据，请先上传数据")
            } else {
                if(props.modelValue.length==32){
                    let params = {
                        sjdx: {
                            dxdm: 'SYS_QX_QTQX'
                        },
                        sys: {
                            cllx: 'download',
                        },
                        yobj: {
                            id: props.modelValue
                        }
                    } as any;
                    axios.download(params)
                }else {
                    axios.download({},{
                        method: 'get',
                        url:props.modelValue
                    })
                }
            }
        }
        watch(() => props.disabled, (newValue) => {
            myData.isdisabled = props.readonly || newValue
        })
        watch(() => props.readonly, (newValue) => {
            myData.isdisabled = props.disabled || newValue
        })

        const download = () => {
            axios.download(props.mbxzcs)
        }

        if(props.qqcs){
            for(let key in props.qqcs){
                myData.qqcs[key]=props.qqcs[key];
            }
        }

        return {
            myData,
            handleExceed,
            // uploadSectionFile,
            download,
            wjxzBtn,
            success,
            handleRemove,
            uploadEle
        }
    },
    render() {
        return (
            <div style="min-height: 80px;box-shadow: 0 2px 12px 0 rgb(0 0 0 / 10%);padding:10px;display:flex;align-items:center">
                {!this.myData.isdisabled ? (<el-upload
                    ref="uploadEle"
                    disabled={this.myData.isdisabled}
                    v-model={this.myData.value}
                    action={this.myData.urlRoot}
                    multiple
                    name={"files"}
                    show-file-list={this.showFile}
                    limit={this.limitFlie}
                    data={this.myData.qqcs}
                    file-list={this.myData.fileList}
                    on-exceed={this.handleExceed}
                    // on-change={this.handleChange}
                    on-success={this.success}
                    on-remove={this.handleRemove} v-slots={{
                        trigger: () => { return <el-button size="small" type="primary" class={classes.uploadPlsc}>文件上传</el-button> }
                    }}>
                    {this.mbxzcs.sjdx ? <el-link type="info" underline={false} onClick={this.download} style="margin-left:10px">模板下载</el-link> : null}
                </el-upload>) : <el-button link type='primary' onClick={this.wjxzBtn}>查看</el-button>}

            </div>
        )
    }
})
