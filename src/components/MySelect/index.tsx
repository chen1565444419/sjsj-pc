import { defineComponent, reactive, ref, watch } from 'vue'
import { assignDeep, clone, getByPath, zdList } from "@/utils/common"
export default defineComponent({
    name: 'MySelect',
    inheritAttrs: true,
    props: {
        /**
         * 输入值
         */
        modelValue: {
            type: String
        },
        /**
         * 字典类别
         */
        zdlb: {
            type: String,
            required: true
        },
        disabled: {
            type: String
        },
        readonly: {
            type: String
        },
        /**
         * 搜索参数，与默认参数合并
         */
        searchParams: {
            type: Object,
            default: {}
        }
    },
    emits: ["update:modelValue"],
    setup: async (props, context) => {
        const myData: any = reactive({
            options: [],
            valve: props.modelValue?String(props.modelValue):"",
            isdisabled: props.readonly || props.disabled,
            searchParams: {
            }
        } as any);
        myData.searchParams = assignDeep({}, props.searchParams);
        const xInput = ref();
        //加载字典
        myData.options = clone(await zdList(props.zdlb))
        if (getByPath(myData.searchParams, "sys.pageCllx") === "select") {
            myData.options.push({
                dm: "my-null",
                mc: "为空"
            });
            myData.options.push({
                dm: "my-not-null",
                mc: "非空"
            });
        }
        watch(() => props.zdlb, async (newZdlb) => {
            myData.options = clone(await zdList(newZdlb))
            if (getByPath(myData.searchParams, "sys.pageCllx") === "select") {
                myData.options.push({
                    dm: "my-null",
                    mc: "为空"
                });
                myData.options.push({
                    dm: "my-not-null",
                    mc: "非空"
                });
            }
        })
        watch(() => props.disabled, (newValue) => {
            myData.isdisabled = props.readonly || newValue
        })
        watch(() => props.readonly, (newValue) => {
            myData.isdisabled = props.disabled || newValue
        })
        watch(() => props.modelValue, (newValue) => {
            myData.valve = newValue
        })
        const updateVal = (newValue: any) => {
            if (newValue == null) {
                newValue = ""
            }
            context.emit("update:modelValue", newValue)
            myData.valve = newValue
        }

        /**
         * 返回值
         */
        return {
            myData,
            xInput,
            updateVal
        }
    },
    render() {
        return (
            <>
                <vxe-select ref="xInput" transfer options={this.myData.options} disabled={this.myData.isdisabled}
                    model-value={this.myData.valve} onUpdate:modelValue={this.updateVal} option-props={{ value: "dm", label: "mc" }}>
                </vxe-select>
            </>
        )
    }
})
