/*
 * @Date: 2022-10-08 09:54:01
 * @LastEditTime: 2022-11-02 15:32:55
 * @Description: 菜单组件
 */
import router from '@/router'
import { ElMessage } from 'element-plus'
import { defineComponent } from 'vue'
import { useTabsStore } from '@/store/modules/tabs';

const SidebarItem = defineComponent({
    name: 'SidebarItem',
    props: {
        item: {
            type: Object,
            required: true
        },
    },
    setup(props) {
        /**
         * 点击某个菜单时
         * @param data 菜单项
         */
        const selectMenu = (data: any) => {
            switch (data.meta.dzlx) {
                case "01":
                case "02":
                case "03":
                case "04":
                case "05":
                    if (data.meta.dkfs === "01") {
                        let params = {
                            path: data.path,
                            name: data.name,
                            meta: {
                                title: data.meta.title,
                            },
                        };
                        useTabsStore().selectMenu(params);
                        useTabsStore().updateKeepList(params);

                        router.push({ name: data.name })
                    }
                    else if (data.meta.dkfs === "02") {
                        if (data.meta.dzlx === "01" || data.meta.dzlx === "03") {
                            window.open(window.location.href.split("#")[0] + "#/singleHome/" + data.path, "_blank");
                        } else {
                            let Baseurl = "";
                            if (data.meta.dz.slice(0,4) !== "http") {
                                Baseurl = window.location.href.split("#")[0]
                            }
                            window.open(Baseurl + data.meta.dz, "_blank");
                        }
                    }
                    // router.replace({ path: '/redirect/' + data.name })
                    else {
                        ElMessage.error("暂不支持的打开方式：" + data.dzlx);
                    }
                    break
                default:
                    ElMessage.error("暂不支持的地址类型：" + data.dzlx);
            }
        }
        return () => {
            const handleRoute = () => {
                const { item } = props
                // 最后一层的情况，渲染菜单项
                if (!item.children || item.children.length === 0||item.meta.lx=='01') {
                    return <el-menu-item onClick={() => selectMenu(item)} index={item.name}>
                        <e-icon icon-name={item.meta.icon ? item.meta.icon : "component Menu"} style="vertical-align: middle;margin-right: 5px;width: 24px;text-align: center;font-size: 18px;" />
                        <span>{item.meta.title}</span>
                    </el-menu-item>
                }
                const slots = {
                    title: () => {
                        return <>
                            <e-icon icon-name={item.meta.icon ? item.meta.icon : "component Menu"} style="vertical-align: middle;margin-right: 5px;width: 24px;text-align: center;font-size: 18px;" />
                            <span>{item.meta.title ? item.meta.title : '未定义菜单名称'}</span>
                        </>
                    }
                }

                // 有children属性，递归
                return <el-sub-menu index={item.name} v-slots={slots}>
                    {item.children.map((child: any) => {
                        return <SidebarItem item={child} key={child.dm}></SidebarItem>
                    })}
                </el-sub-menu>
            }
            return <>{handleRoute()}</>
        }
    }
})

export default SidebarItem
