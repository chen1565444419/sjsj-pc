import { defineComponent, nextTick, onBeforeUnmount, reactive, ref, watch } from "vue";
import { useSocketStore } from '@/store/modules/socket'
import classes from './index.module.scss'
import { HomeFilled } from "@element-plus/icons-vue";
export default defineComponent({
    name: "MyRealTimeMsg",
    inheritAttrs: true,
    props: {
        mdddl: {
            type: String,
            default: null
        },
        mddxl: {
            type: String,
            default: null
        }
    },
    setup: (props, context) => {
        const socketStore = useSocketStore()

        const scrollbarRef = ref()

        let autoScroll = ref(true)

        let dataList: any[] = reactive([])

        socketStore.sendMsg({ "xxlb": "1", mdddl: props.mdddl, mddxl: props.mddxl })

        socketStore.$onAction(({ name, args }: any) => {
            //监听receiveMsg方法
            if (name === 'receiveMsg') {
                if (args[0].mdddl && args[0].mdddl === props.mdddl && args[0].mddxl == props.mddxl) {
                    dataList.push(args[0].xxnr)
                    if (args[0].xxlb === '00') {
                        dataList = []
                    }
                    scrollbarRef.value.update()
                    if (autoScroll.value) {
                        nextTick(() => {
                            scrollbarRef.value.setScrollTop(99999999999999999999999999)
                        })
                    }
                }
            }
        })
        const changeAutoScroll = () => {
            autoScroll.value = !(autoScroll.value);
        }

        onBeforeUnmount(() => {
            socketStore.sendMsg({ "xxlb": "2", mdddl: props.mdddl, mddxl: props.mddxl })
        })
        return {
            changeAutoScroll,
            scrollbarRef,
            autoScroll,
            dataList,
        };

    },
    render() {
        return <div class={classes.container}>
            <div class={classes.title}>
                <div class={classes.autoScroll} onClick={this.changeAutoScroll}>
                    自动滚动：{this.autoScroll ? "开" : "关"}
                    <el-icon><HomeFilled /></el-icon>
                </div>
            </div>
            <div style="height: 500px;padding:10px">
                <el-scrollbar ref="scrollbarRef" key={this.dataList.length}>
                    {this.dataList.map((item: any) => {
                        return <div>{item}</div>
                    })}
                </el-scrollbar></div></div>
    }
});
