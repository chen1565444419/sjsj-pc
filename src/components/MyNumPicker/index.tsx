/*
 * @Date: 2023-02-16 10:07:17
 * @LastEditTime: 2023-02-16 10:57:54
 * @Description: 
 */
import { defineComponent, ref } from 'vue'

export default defineComponent({
    name: 'MyNumPicker',
    props: {
        /**
         * 输入值
         */
        modelValue: {
            type: Array
        },
    },
    emits: ["update:modelValue"],
    setup(props, context) {
        const numList = ref([]) as any
        if (props.modelValue) {
            numList.value = props.modelValue
        }

        const blur = () => {
            context.emit("update:modelValue", numList.value);
            console.log('blur')
        }

        return {
            numList,
            blur
        }
    },
    render() {
        return (<div style="display:flex;align-items:center;">
            <el-input-number style="flex: 1" v-model={this.numList[0]} {...this.$attrs} onBlur={this.blur} /><div style="padding:0 2px"> - </div><el-input-number style="flex: 1" v-model={this.numList[1]} {...this.$attrs} onBlur={this.blur} />
        </div>)
    }
})
