import axios from "@/axios";
import { defineComponent, ref, watch } from "vue";

export default defineComponent({
    name: "MyExportModal",
    props: {
        visible: {
            type: Boolean,
            default: false
        },
        size: {
            type: Number,
            default: 50000
        }
    },
    emits: ["update:modelValue"],
    setup(props, ctx) {
        let visible = ref(props.visible)
        watch(() => props.visible, (newValue) => {
            visible.value = newValue
        })
        watch(() => visible.value, (newValue) => {
            ctx.emit('update:modelValue', newValue)
        })
        let total = ref(0)
        let params = ref()
        let value = ref(1)
        let optionsData = ref([] as any[])
        let optionProps = {
            value: 'value',
            label: 'label'
        }
        const closeModal = () => {
            visible.value = false
        }
        const init = (paramsValue: any, totalValue: number) => {
            console.log("init")
            params.value = paramsValue
            total.value = totalValue
            console.log(params.value)
            console.log(total.value)
            optionsData.value = []
            for (let i = 1; i < Math.ceil(totalValue / paramsValue.page.pageSize); i++) {
                optionsData.value.push({
                    value: i,
                    label: '第' + ((i - 1) * paramsValue.page.pageSize + 1) + '~' + i * paramsValue.page.pageSize + '条'
                },)
            }
            optionsData.value.push({
                value: Math.ceil(totalValue / paramsValue.page.pageSize),
                label: '第' + ((Math.ceil(totalValue / paramsValue.page.pageSize) - 1) * paramsValue.page.pageSize + 1) + '~' + totalValue + '条'
            })
        }
        const confirm = () => {
            params.value.page.pageNumber = value
            axios.download(params.value);
        }

        return {
            visible,
            closeModal,
            value,
            optionsData,
            optionProps,
            init,
            confirm
        }
    },
    render() {
        return (<vxe-modal width="400px" height="200px" title="导出" showFooter={true} v-model={this.visible} v-slots={{
            default: () => {
                return <div style="text-align:center;line-height:24px">
                    <div>导出数据过多,</div>
                    <div>请选择需要的数据，分批导出</div>
                    <vxe-select transfer style="margin-top:12px;width:200px" v-model={this.value} options={this.optionsData} optionProps={this.optionProps}>
                    </vxe-select>
                </div>
            },
            footer: () => {
                return <div style="text-align:center"><vxe-button status="primary" onClick={this.confirm}>确定</vxe-button>  <vxe-button onClick={this.closeModal}>取消</vxe-button></div>
            }
        }}> </vxe-modal>)
    }
})
