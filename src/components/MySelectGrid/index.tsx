import { defineComponent, reactive, ref, nextTick, computed, watch, onMounted } from 'vue'
import { ElMessage, ElMessageBox } from "element-plus";
import { VXETable } from '@majinju/vxe-table';
import axios from "@/axios";
import {dayjsMethod, hasAuth, getByPath, assignDeep, copyByPathMap, isEmpty, setByPath, clone} from "@/utils/common";
import { options } from "@/plugins/vxe-table";
import MyForm from "@/components/MyForm";
import MyExportModal from "@/components/MyExportModal";
import classes from './index.module.scss';
import * as echarts from 'echarts'
import { ArrowDown, Bell, HomeFilled, MuteNotification, PictureFilled, QuestionFilled } from '@element-plus/icons-vue';
import {types} from "sass";
import Boolean = types.Boolean;

export default defineComponent({
    name: "MySelectGrid",
    components: { MyForm, MyExportModal },
    props: {
        /**
         * 输入值
         */
        modelValue: {
            type: Array
        },
        /**
         * 查询条件
         */
        searchParams: {
            type: Object
        },
        mode: {
            type: String,
            //默认查询页面（ybms），支持：ybms(一般模式)、zjms(组件模式)
            default: "ybms"
        },
        /**
         * 父页面传入对象基础信息
         */
        dxjcxx: {
            type: Object,
            required: true
        },
        /**
         * 是否禁用
         */
        disabled: {
            type: Boolean
        },
        /**
         * 是否只读
         */
        readonly: {
            type: Boolean
        },
        /**
         * 初始编辑状态
         */
        csbjzt:{
            type: Boolean
        },
        /**
         * key组件标志
         */
        key: {
            type: String
        },
        /**
         * 插槽
         */
        slots: {
            type: Object
        },
        getList: {
            type: Function
        },
        /**
         * 是否显示分页器
         */
        pagerShow: {
            type: Boolean,
            default: true
        }
    },
    //返回列表数据（可以在批量保存时调用），关闭弹窗
    emits: ["update:modelValue", "close"],
    setup: async (props, context) => {
        const echartsRef = ref()
        const updateEcharts = (option: any) => {
            myData.echarts = echarts.init(echartsRef.value)
            myData.echarts.clear()
            myData.echarts.setOption(option)
        }
        const lbmsChange = ({ value }: any) => {
            myData.lbmsValue = value
            if (value !== 'list') {
                myData.tableHideClass = "hidden"
                myData.lbmsList.map((item: any) => {
                    if (item.dm === value) {
                        let option = JSON.parse(JSON.stringify(item.option))
                        option.dataset.source = myData.tableData
                        nextTick(() => {
                            updateEcharts(option)
                        })
                        return
                    }
                })
            } else {
                myData.tableHideClass = ""

            }
        }
        let myData: any = reactive({
            tableHideClass: "",
            lbmsValue: "",
            lbmsList: [],
            fytjxs: "1",
            myHelp: undefined as string | undefined,
            exportModalVisible: false,
            echarts: null,
            slots: props.slots,
            selectReqData: {
                sjdx: {
                    dxdm: '',
                    id: ''
                },
                sys: {
                    authCode: '',
                    cllx: ''
                },
                page: {
                    pageNumber: '',
                    totalRequired: true,
                    pageSize: 0,
                    orderBy: ''
                },
                yobj: {}
            },
            /**
             * 对象基础信息
             */
            dxjcxx: {
                sjdx: {
                    dxmc: '',
                    yxxzd: '',
                    dxdm: '',
                    id: '',
                    zjzd: '',
                    cscx: '',
                    cxtj: '',
                },
                sys: {
                    authCode: '',
                    cllx: ''
                },
                obj: {},
                fields: {}
            } as any,
            /**
             * 操作全部数据
             */
            czqbsj: false,
            /**
             * 全局统一配置
             */
            options: options,
            /**
             * 表单数据，查询默认值可以设置其中
             */
            formData: {
            },
            /**
             * 表单字段信息
             */
            formItems: [] as any[],
            /**
             * 表单字段验证规则
             */
            formRule: {
            },
            /**
             * 表格基础属性
             */
            gridOptions: {
                // showOverflow:false
            },
            /**
             * 列表批量操作栏
             */
            tableToolbar: {
                slots: {
                    buttons: 'toolbar_left',
                    tools: 'toolbar_right'
                }
            },
            /**
             * 导出配置
             */
            exportConfig: {
                remote: true,
                // 默认选中类型
                // 局部自定义类型
                types: ['csv', 'html', 'xml', 'txt'],
                // 自定义数据量列表
                modes: ['current', 'all']
            },
            /**
             * 树形结构配置
             */
            treeConfig: {
                lazy: true,
                loadMethod: {},
                parentField: '',
                rowField: '',
                treeQqsjkz: ''
            },
            /**
             * 列表字段信息
             */
            tableColumn: [] as any[],
            /**
             * 列表数据
             */
            tableData: [],
            /**
             * 列表序号配置
             */
            seqConfig: {
                seqMethod({ rowIndex }: any) {
                    return myData.pagerConfig.pageSize * (myData.pagerConfig.currentPage - 1) + rowIndex + 1;
                }
            } as any,
            /**
             * 分页配置
             */
            pagerConfig: {
                pageSize: options.pager.pageSize,
                pageSizes: options.pager.pageSizes,
                currentPage: 1,
                total: null
            },
            /**
             * 表格编辑配置
             */
            editCofnig: {
                enabled: props.csbjzt==undefined?!(props.disabled || props.readonly): props.csbjzt,
                trigger: 'click',
                mode: 'cell',
                showStatus: true,
                // beforeEditMethod: isEdit
            },
            //允许编辑
            yxbj:!(props.disabled || props.readonly),
            menuConfig: {
                body: {
                    options: [
                        [
                            { code: 'cover', name: '覆盖当前列' }]
                    ]
                },
                trigger: 'cell',
                visibleMethod({ column }: any) {
                    console.log(column.editRender && myData.editCofnig.enabled)
                    if (column.editRender && myData.editCofnig.enabled) {
                        return true
                    }
                }
            },
            /**,
             * 列表复选框配置
             */
            checkboxConfig: {
                checkField: "",
                checkFieldOld: ""
            },
            /**
             * 表格字段验证规则
             */
            tableRule: {},
            /**
             * 弹出窗口是否展示
             */
            tcckShow: false,
            tcckProps: {
                /**
                 * 弹出窗口标题
                 */
                title: "",
                width: undefined,
                height: undefined
            }
        })

        /**
         * 查询表单引用
         */
        const xFrom = ref();
        /**
         * 表格引用
         */
        const xGrid = ref();
        const exportModalRef = ref();
        const objKeySort = (arys: any) => {
            let newkey = Object.keys(arys).sort((a: string, b: string) => {
                return arys[a].px - arys[b].px
            });
            let newObj = {};
            newkey.forEach(item => {
                newObj[item] = arys[item]
            })
            return newObj; //返回排好序的新对象
        }

        /**
         * 初始化基础信息
         * @param dxjcxx 外部提供的基础信息
         * @returns {Promise<{fields}|*>}
         */
        async function initJcxx(dxjcxx: any) {
            if (!dxjcxx.fields) {
                //加载对象基础信息
                await axios.post({
                    sjdx: dxjcxx.sjdx,
                    sys: {
                        authCode: dxjcxx.sys.authCode,
                        cllx: "dxjcxx"
                    }
                }, { ctdy: false }).then(async (rep) => {
                    dxjcxx = rep.data;
                });
            }
            myData.dxjcxx = dxjcxx;
            myData.fytjxs = myData.dxjcxx.sjdx.fytjxs
            return dxjcxx;
        }

        /**
         * 初始化字段的表格验证规则
         * @param f 对应的字段
         */
        function initTableRule(f: any) {
            myData.tableRule[f.zddm] = [
                {
                    validator({ itemValue }: any) {
                        //自定义校验规则，跟后台规则一致的前端实现
                        console.log(itemValue);
                    }
                }
            ]
            if (getByPath(f.kzxxObj, "yzgz.update.notNull")) {
                //单独处理非空校验，便于在页面给用户红色星号提示必填字段
                myData.tableRule[f.zddm].push({ required: true, message: f.zdmc + '必填', trigger: 'blur' });
            }
        }

        /**
         * 初始化表复选框相关配置
         * @param dxjcxx 对象基础信息
         */
        function initTableCheckbox(dxjcxx: any) {
            if (getByPath(dxjcxx, "cllxkz.select.checkboxConfig")) {
                myData.checkboxConfig = getByPath(dxjcxx, "cllxkz.select.checkboxConfig")
                //设置判断复选框选中的字段
                myData.checkboxConfig.checkField = myData.checkboxConfig.checkFieldOld + "_boolean"
            }
        }
        /**
         * 初始化表格树相关配置
         * @param dxjcxx 对象基础信息
         */
        function initTableTree(dxjcxx: any) {
            if (!getByPath(dxjcxx, "cllxkz.select.tree.parentField")) {
                //没有配置树
                return
            }
            //树形结构采用默认序号方法
            delete myData.seqConfig.seqMethod;
            //树形结构
            myData.treeConfig = getByPath(dxjcxx, "cllxkz.select.tree")
            if (myData.treeConfig.lazy === false) {
                //不是懒加载
                return;
            }
            myData.treeConfig.lazy = true
            myData.treeConfig.loadMethod = function ({ row }: any) {
                return new Promise((resolve, reject) => {
                    // let yobj = assignDeep({},myData.selectReqData.yobj);
                    let yobj = {};
                    if (myData.dxjcxx.sjdx.yxxzd) {
                        //加载树还是要考虑有效性
                        yobj[myData.dxjcxx.sjdx.yxxzd] = myData.formData[myData.dxjcxx.sjdx.yxxzd];
                    }
                    yobj[myData.treeConfig.parentField] = row[myData.treeConfig.rowField]
                    //树形结构请求数据
                    const treeQqsj = {
                        sjdx: {
                            "dxdm": myData.dxjcxx.sjdx.dxdm,
                            "id": myData.dxjcxx.sjdx.id
                        },
                        sys: {
                            authCode: dxjcxx.sys.authCode,
                            cllx: "select"
                        },
                        yobj: yobj,
                        page: {
                            pageSize: 500
                        }
                    }
                    copyByPathMap(treeQqsj, myData, myData.treeConfig.treeQqsjkz)
                    axios.post(treeQqsj).then(req => {
                        resolve(req.data.list)
                    }).catch((e) => {
                        reject(null);
                        console.log("查询失败：" + e);
                    })
                })
            }
        }

        /**
         * 初始查询项
         * @param f 字段配置
         * @returns {{field: *, title: *, span: number}}
         */
        function initCxx(f: any) {
            if (f.cxzs === '1') {
                let lxkz = f.kzxxObj.cllxkz['select'] || {};
                //查询默认值
                if (f.cxmrz) {
                    //不管查询是否展示，查询默认值都要生效，这样可以生成一些隐藏查询条件
                    myData.formData[f.zddm] = f.cxmrz;
                }
                //配置查询项
                const fi:any = {
                    field: f.zddm,
                    title: f.zdmc,
                    span: 8,
                    folding: lxkz.folding,
                    itemRender: {
                        name: f.kjlx,
                        props: {
                            key: "cx_" + f.id,
                            placeholder: f.zdts,
                            // maxlength: f.zdcd,
                            size: '',
                            clearable: true,
                            zdlb: '',
                            searchParams: {},
                            multiple: false,
                        },
                        children: {}
                    }
                }
                fi.itemRender.props.slots = myData.slots
                fi.itemRender.props.myData = myData
                if (f.zdkd > 10000) {
                    fi.span = f.zdkd % 100;
                }
                //添加校验规则
                myData.formRule[f.zddm] = [
                    {
                        validator({ itemValue, rule, rules, data, property }: any) {
                            //自定义校验规则，跟后台规则一致的前端实现
                            console.log(itemValue);
                        }
                    }
                ]
                if (getByPath(f.kzxxObj, "yzgz.select.notNull")) {
                    //单独处理非空校验，便于在页面给用户红色星号提示必填字段
                    myData.formRule[f.zddm].push({ required: true, message: f.zdmc + '必填', trigger: 'change' });
                }
                switch (f.kjlx) {
                    case 'ElCascader':
                        fi.itemRender.name = 'MyCascader';
                        fi.itemRender.props.size = options.input.size;
                        fi.itemRender.props.multiple = f.zddx === '1';
                        if (fi.itemRender.props.clearable) {
                            fi.itemRender.props.clearable = options.input.clearable
                        }
                        fi.itemRender.props.zdlb = f.zdzdlb;
                        break
                    case '$switch':
                    case '$select':
                        //还要考虑字典树
                        if (f.zddx === '1'||f.zdfy === '1') {
                            //大字典，采用下拉分页搜索框、
                            fi.itemRender.name = 'MyDownListMultiple';
                        } else {
                            //普通下拉框
                            fi.itemRender.name = 'MySelect';
                        }
                        fi.itemRender.props.zdlb = f.zdzdlb;
                        fi.itemRender.props.searchParams = {
                            sys: {
                                pageCllx: "select"
                            }
                        };
                        break
                    case '$radio':
                    case '$checkbox':
                        //还要考虑字典树
                        fi.itemRender.name = 'MyCheckbox';
                        fi.itemRender.props.zdlb = f.zdzdlb;
                        break
                    case 'ElDatePicker':
                        //时间选择器
                        fi.itemRender.props = Object.assign(fi.itemRender.props, {
                            type: 'datetimerange',
                            clearable: options.input.clearable,
                            size: options.input.size,
                            defaultTime: [
                                new Date(2000, 1, 1, 0, 0, 0),
                                new Date(2000, 2, 1, 23, 59, 59)
                            ],
                            valueFormat: "YYYYMMDDHHmmss"
                        });
                        if (f.cxmrz) {
                            const times = JSON.parse(f.cxmrz);
                            let val1 = "";
                            let val2 = "";
                            if (times.start) {
                                val1 = dayjsMethod(times.start)
                            }
                            if (times.end) {
                                val2 = dayjsMethod(times.end)
                            }
                            myData.formData[f.zddm] = [val1, val2];
                        }
                        break
                    case 'ElInputNumber':
                        fi.itemRender.name = 'MyNumPicker';
                        break
                    case '$buttons':
                        //按钮组
                        let children = [];
                        //获取配置的按钮组
                        let $buttons = getByPath(f.kzxxObj, "kjkz.btns");
                        for (let i in $buttons) {
                            //处理类型设置到名称，便于后续按钮事件中使用
                            $buttons[i].name = i;
                            //TODO 按钮点击回调方法,以后再按表单页面扩展
                            // $buttons[i].click = function(option){
                            //   plcl(option.props.name,option.props);
                            // }
                            children.push({ props: $buttons[i] })
                        }
                        fi.itemRender.children = children;
                        break
                    case 'slot':
                        fi.slots = { "default": fi.field }
                        break
                    default:
                        //默认普通输入框
                        fi.itemRender.name = '$input';
                }
                //通用
                assignDeep(fi, f.kzxxObj.kjkz);
                //按处理类型设置
                myData.formItems.push(assignDeep(fi, lxkz.kjkz));
            }
        }

        /**
         * 初始化表格列信息
         * @param f 对应字段配置
         */
        function initTableColumn(f: any) {
            if (f.lbzs === '1') {
                let lxkz = getByPath(f.kzxxObj, "cllxkz.insert") || {};
                //配置列表字段
                const fi = {
                    field: f.zddm,
                    title: f.zdmc,
                    align: 'center',
                    width: '',
                    minWidth: '',
                    maxWidth: '200px',
                    sortable: false,
                    type: '',
                    formatter: '',
                    params: {
                        yszddm: f.yszddm
                    },
                    slots: {},
                    editRender: {
                        name: '',
                        props: {
                            key: "lb_" + f.id,
                            placeholder: f.zdts,
                            //是否禁用
                            disabled: getByPath(f.kzxxObj, "cllxkz.update.disabled"),
                            //是否只读
                            readonly: getByPath(f.kzxxObj, "cllxkz.update.readonly"),
                            enabled: true,
                            zdlb: '',
                            // maxlength: '',
                        },
                    }
                } as any
                fi.editRender.props.enabled = !(fi.editRender.props.disabled || fi.editRender.props.readonly)

                if (f.zdkd > 20000) {
                    fi.width = (Math.round(f.zdkd / 10000)*20+40)+'px';
                } else if (f.zdkd > 10000) {
                    fi.minWidth = '150px';
                } else {
                    fi.width = f.zdkd + "px";
                }
                if (f.yxpx === '1') {
                    fi.sortable = true;
                }
                switch (f.kjlx) {
                    case 'checkbox':
                        //列表选择
                        fi.type = 'checkbox';
                        break
                    case 'seq':
                        //列表序号
                        fi.type = 'seq';
                        break
                    case '$switch':
                        //开关控件
                        fi.formatter = 'formatterZd';
                        fi.editRender.name = 'MySwitch';
                        fi.editRender.props = Object.assign(fi.editRender.props, {
                            zdlb: f.zdzdlb,
                            openValue: "1",
                            closeValue: "0"
                        });
                        break
                    case '$radio':
                    case '$checkbox':
                    case 'ElCascader':
                    case '$select':
                        //下拉字典
                        fi.formatter = 'formatterZd';
                        //还要考虑字典树
                        if (f.zddx === '1') {
                            //大字典，采用下拉分页搜索框、
                            fi.editRender.name = 'MyDownListMultiple';
                        } else if (f.zdfy === '1') {
                            //大字典，采用下拉分页搜索框、
                            fi.editRender.name = 'MyDownList';
                        } else {
                            //普通下拉框
                            fi.editRender.name = 'MySelect';
                        }
                        fi.editRender.props.zdlb = f.zdzdlb;
                        break
                    case 'ElDatePicker':
                        //时间控件
                        fi.editRender.name = 'ElDatePicker';
                        fi.formatter = 'formatDate';
                        fi.editRender.props = Object.assign(fi.editRender.props, {
                            type: 'datetime',
                            clearable: options.input.clearable,
                            size: options.input.size,
                            valueFormat: "YYYYMMDDHHmmss"
                        });
                        break
                    case '$textarea':
                        //时间选择器
                        fi.editRender.name = 'textarea';
                        fi.editRender.props.maxlength = f.zdcd;
                        break
                    case '$buttons':
                        //按钮组
                        fi.params = getByPath(f.kzxxObj, "kjkz")
                        if (fi.params.btns) {
                            //遍历按钮组，给px赋默认值
                            for (let i in fi.params.btns) {
                                if (isEmpty(fi.params.btns[i].px)) {
                                    fi.params.btns[i].px = 99
                                }
                            }
                            //通过px来对其进行排序
                            fi.params.btns = objKeySort(fi.params.btns)
                        }
                        fi.slots = { default: 'lbcz' }
                        break
                    case 'MyImage':
                        fi.slots = { default: 'MyImage' }
                        break
                    case 'slot':
                        fi.slots = { "default": fi.field }
                        break
                    case 'ElInputNumber':
                        fi.editRender.name = 'ElInputNumber';
                        break
                    default:
                        fi.editRender.name = '$input';
                }
                //合并字段的控件扩展，覆盖默认值，通用
                assignDeep(fi, f.kzxxObj.kjkz);
                //按处理类型设置
                assignDeep(fi, lxkz.kjkz)
                assignDeep(fi.editRender, fi.itemRender)
                assignDeep(fi.params, fi.editRender.props)
                if (!fi.editRender.props.enabled) {
                    delete fi.editRender
                    // fi.editRender = new (Object as any)
                }
                myData.tableColumn.push(fi);
            }
        }
        /**
         * 获取查询权限的子权限配置
         */
        const qxpz: any = computed(() => {
            let qxpz = getByPath(myData, "dxjcxx.cllxkz.select.qxpz");
            if (!qxpz) {
                qxpz = {}
            }
            return qxpz
        })
        /**
         * 当前对象是否有权限
         * @param cllx 处理类型
         * @returns {*} true：有权限，false：无权限
         */
        const tHasAuth = (cllx: any) => {
            return hasAuth(myData.dxjcxx.sys.authCode + "_" + cllx)
        }
        /**
         * 获取权限列表
         * @param qxz 权限组
         * @param gdczSize 区分更多操作的按钮数
         * @param gdcz 本次是否获取更多操作
         * @param row 操作行的数据，为空则是批量操作按钮
         */
        const hqqxlb = (qxz: any, gdczSize: any, gdcz = false, row?: any) => {
            let qxlb = {}
            let qxlb1 = {}
            for (const cllx in qxz) {
                //有权限、配置要展示、该行没有要求不展示
                if (tHasAuth(cllx) && !(qxz[cllx].buttonOptions.isshow === false)&&!(row&&row[cllx+"Show"]===0)
                    &&(myData.yxbj===true||qxz[cllx].buttonOptions.bjqx!==true)
                ) {
                    qxlb[cllx] = qxz[cllx];
                }
            }
            let count = 0;
            if (gdcz) {
                //获取更多操作
                if (Object.keys(qxlb).length > gdczSize) {
                    for (const cllx in qxlb) {
                        if (count >= gdczSize - 1) {
                            qxlb1[cllx] = qxlb[cllx];
                        }
                        count++
                    }
                } else {
                    return {}
                }
            } else {
                //获取默认展示的操作
                const size = Object.keys(qxlb).length > gdczSize ? gdczSize - 1 : gdczSize;
                for (const cllx in qxlb) {
                    if (count < size) {
                        qxlb1[cllx] = qxlb[cllx];
                    }
                    count++
                }
            }
            return qxlb1;
        }
        /**
         * 设置页大小
         * @param pageSize 页大小
         */
        const setPageSize = (pageSize: any) => {
            myData.selectReqData.page.pageSize = pageSize;
            myData.pagerConfig.pageSize = pageSize
        }
        /**
         * 设置当前页
         * @param currentPage 页大小
         */
        const setPageNumber = (currentPage: any) => {
            myData.selectReqData.page.pageNumber = currentPage;
            myData.pagerConfig.currentPage = currentPage
        }
        /**
         * 分页切换事件
         * @param currentPage 当前页
         * @param pageSize 页大小
         */
        const pageChange = ({ currentPage, pageSize }: any) => {
            setPageSize(pageSize);
            setPageNumber(currentPage);
            myData.selectReqData.page.totalRequired = false
            getList();
        }
        /**
         * 自定义字段排序
         * @param column
         */
        const sortChange = ({ column }: any) => {
            myData.selectReqData.page.orderBy = column.order ? (column.params.yszddm + " " + column.order) : "";
            myData.selectReqData.page.totalRequired = false
            getList();
        }
        /**
         * 搜索按钮
         */
        const search = () => {
            //点击搜索按钮，恢复为第一页
            setPageNumber(1);
            myData.fytjxs = myData.dxjcxx.sjdx.fytjxs
            myData.selectReqData.page.totalRequired = true
            getList()
        }
        /**
         * 获取总数
         */
        const getTotal = () => {
            let query = { ...myData.selectReqData }
            query.page.totalRequired = true
            query.page.listRequired = false
            axios.post(query).then(req => {
                if (req.data.totalRow >= 0) {
                    myData.pagerConfig.total = req.data.totalRow;
                    myData.fytjxs = "0"
                }
            })
        }
        /**
         * 执行分页查询
         */
        const getList = (isLoading = true) => {
            myData.selectReqData.yobj = myData.formData;
            myData.selectReqData.page.listRequired = true;
            if (myData.fytjxs !== "1") {
                myData.selectReqData.page.totalRequired = false;
            }
            if (props.getList) {
                props.getList(getListMr, myData, xGrid)
            } else {
                getListMr(isLoading)
            }
        }
        const getListMr = (isLoading = true) => {
            axios.post(myData.selectReqData, { tloading: isLoading }).then(req => {
                if ((myData.selectReqData.page.totalRequired || myData.pagerConfig.total === null)
                    && req.data.totalRow >= 0) {
                    myData.pagerConfig.total = req.data.totalRow;
                }
                if (myData.fytjxs === "2" || myData.fytjxs === "3") {
                    myData.pagerConfig.total = 9999999999999999999999;
                }
                myData.tableData = req.data.list
                if (!isEmpty(myData.lbmsValue)) {
                    lbmsChange({ value: myData.lbmsValue })
                }
                // updateEcharts(req.data.list)
                xGrid.value.reloadData(myData.tableData)
                if (myData.bjsy && !isEmpty(myData.bjsy) && audioPlay.value && myData.tableData.length > 0) {
                    audio.value.play();
                }
            }).catch((e) => {
                console.log("查询失败：" + e);
            })
            if (myData.fytjxs === "2") {
                getTotal()
            }
        }
        /**
         * 通用后台请求
         * @param cllx 处理类型
         * @param buttonOptions 按钮参数
         * @param ids 操作id数组
         * @param row 操作行
         * @param ur 表格编辑的数据列表
         * @param changeCheckData 变化的选择数据
         */
        const htqq = (cllx: any, buttonOptions: any, ids: any, row: any, ur: any, changeCheckData?: any, treeExpandData?: any) => {
            //后台请求参数
            const htqqcs = {
                sys: {
                    authCode: myData.dxjcxx.sys.authCode,
                    cllx: cllx,
                    ids: ids,
                    editTableData: ur,
                    changeCheckData: changeCheckData,
                    treeExpandData: treeExpandData
                },
                yobj: clone(myData.selectReqData.yobj)
            }
            copyByPathMap(htqqcs, myData, buttonOptions.jcxxkz)
            axios.post(assignDeep(htqqcs, buttonOptions.params)).then((req: any) => {
                ElMessage.success(req.msg);
                if (buttonOptions.sfsxym !== false) {
                    //判断是否是树形结构并且拥有父节点并且cllx为delete
                    if (cllx === 'delete' && !isEmpty(getByPath(myData.dxjcxx, "cllxkz.select.tree.parentField")) && row.fqx) {
                        let data = findRow(row[myData.treeConfig.parentField], myData.tableData);
                        data && xGrid.value.reloadTreeExpand(data)
                    } else {
                        //修改数据的场景要重新统计总量
                        myData.selectReqData.page.totalRequired = true
                        getList()
                    }
                }
                if (buttonOptions.sfgbtc === true) {
                    context.emit('close', buttonOptions.sfsxym)
                }
            }).catch((req) => {
                console.log("处理异常", req)
            });
        }

        /**
         * 递归根据id查找行
         * @param id 待查找的id
         * @param list 列表
         */
        function findRow(id: any, list: Array<any>): any {
            let r: any;
            for (let i in list) {
                r = list[i];
                if (r[myData.treeConfig.rowField] === id) {
                    return r;
                }
                if (r.children) {
                    //有子节点
                    let c = findRow(id, r.children);
                    if (c) {
                        return c;
                    }
                }
            }
            return;
        }

        /**
         * 获取列表复选框变化的数据
         * @param changeData 存储变化的数据
         * @param tableData 表格数据
         */
        function getChangeCheck(changeData: any, tableData: any) {
            const checkFieldOld = myData.checkboxConfig.checkFieldOld
            if (!checkFieldOld) {
                //没有配置选中状态字段，无法区分状态变化
                return
            }
            const checkFieldNew = checkFieldOld + "_boolean"
            const grid = xGrid.value;
            for (const i in tableData) {
                const row = tableData[i];
                if ((row[checkFieldOld] == 1) !== (row[checkFieldNew] || grid.isIndeterminateByCheckboxRow(row))) {
                    //原始值不等于新值则表示变化了
                    changeData[row[myData.dxjcxx.sjdx.zjzd]] = {
                        "checked": (row[checkFieldNew] || grid.isIndeterminateByCheckboxRow(row)),
                        "expand": grid.isTreeExpandLoaded(row),
                        "obj": row
                    }
                }
                if (row.children) {
                    //子元素迭代获取
                    getChangeCheck(changeData, row.children)
                }
            }
        }
        // @ts-ignore
        const ctyd = window.WEB_CONFIG.ctdy;

        /**
         * 批量处理<br/>
         * 考虑场景：直接后台调用、弹窗
         */
        const plcl = async (cllx: any, btnProps: any, row?: any) => {
            //按钮参数
            const buttonOptions = btnProps.buttonOptions;
            //操作的行数据设置为obj，便于后续使用
            myData.dxjcxx.obj = clone(row)
            //处理方式
            const clfs = buttonOptions.clfs || 'htqq';
            let content = btnProps.content;
            let ids: any[] = []
            const cr = xGrid.value.getCheckboxRecords(true);
            if (cr.length === 0 && (buttonOptions.sfxyxzjl !== false)
                && row === undefined && !myData.czqbsj) {
                //没有选中行，且没有要求不选择记录，且不是操作具体某行记录,且没有操作全部数据
                ElMessage.warning("请选择要操作的数据");
                return;
            }
            const isTree = getByPath(myData.dxjcxx, "cllxkz.select.tree.parentField");
            const grid = xGrid.value;
            //存储树形结构展开情况
            const treeExpandData = {}
            cr.forEach((item: any) => {
                ids.push(item[myData.dxjcxx.sjdx.zjzd]);
                if (isTree) {
                    treeExpandData[item[myData.dxjcxx.sjdx.zjzd]] = grid.isTreeExpandLoaded(item);
                }
            });
            if (row) {
                ids = [row[myData.dxjcxx.sjdx.zjzd]];
            }
            setByPath(myData, "dxjcxx.sys.ids", ids);
            //计算树形复选场景的选中取消情况
            //获取当前列表数据
            const tableData = xGrid.value.getTableData().tableData
            //存储复选框变化的数据
            const changeCheckData = {}
            getChangeCheck(changeCheckData, tableData);
            switch (clfs) {
                //后台请求
                case "htqq":
                    if (buttonOptions.htqqts) {
                        ElMessageBox.confirm("你确定" + content + "吗?", "提示", {
                            confirmButtonText: "确定",
                            cancelButtonText: "取消",
                            type: "warning"
                        }).then(() => {
                            htqq(cllx, buttonOptions, ids, myData.dxjcxx.obj, null, changeCheckData,treeExpandData)
                        }).catch(function () {
                            console.info("用户取消操作：" + content)
                        })
                    } else {
                        htqq(cllx, buttonOptions, ids, myData.dxjcxx.obj, null, changeCheckData,treeExpandData)
                    }
                    break
                //批量保存
                case "plbc":
                    const ur = xGrid.value.getUpdateRecords()
                    if (ur.length === 0) {
                        ElMessage.error("没有编辑待保存的数据");
                        return
                    }
                    const errMap = await xGrid.value.validate()
                    if (errMap) {
                        ElMessage.error("数据校验不通过，请检查编辑的数据！");
                        return
                    }
                    //修改数据项列表，只保留id和修改项
                    let ui: {}[] = []
                    //遍历修改的row
                    ur.map((i: any) => {
                        let obj = {}
                        obj[myData.dxjcxx.sjdx.zjzd] = i[myData.dxjcxx.sjdx.zjzd]
                        //遍历column判断列是否被修改
                        myData.tableColumn.map((item: any) => {
                            if (xGrid.value.isUpdateByRow(i, item.field)) {
                                obj[item.field] = i[item.field]
                            }
                        })
                        ui.push(obj)
                    })
                    if (buttonOptions.htqqts) {
                        ElMessageBox.confirm("你确定" + content + "吗?", "提示", {
                            confirmButtonText: "确定",
                            cancelButtonText: "取消",
                            type: "warning"
                        }).then(() => {
                            htqq(cllx, buttonOptions, ids, myData.dxjcxx.obj, ui)
                        }).catch(function () {
                            console.info("用户取消操作：" + content)
                        })
                    } else {
                        htqq(cllx, buttonOptions, ids, myData.dxjcxx.obj, ui)
                    }
                    break
                //弹出窗口
                case "tcck":
                    //窗口标题
                    myData.tcckProps.title = content + "【" + myData.dxjcxx.sjdx.dxmc + "】"
                    //窗口高宽初始化
                    myData.tcckProps.width = "80%"
                    myData.tcckProps.height = "80%"
                    //窗口显示
                    myData.tcckShow = true
                    assignDeep(myData.tcckProps, buttonOptions.tckz)
                    if (buttonOptions.tcqp) {
                        //最大化
                        xModal.value.maximize()
                    } else {
                        //还原
                        xModal.value.revert()
                    }
                    let tdxjcxx = myData.dxjcxx;
                    if (buttonOptions.dxjcxx) {
                        //加载对象基础信息，该按钮设置了处理其他数据对象
                        let jcxxqqcs = {
                            sjdx: buttonOptions.dxjcxx.sjdx,
                            yobj: buttonOptions.dxjcxx.yobj,
                            sys: {
                                authCode: buttonOptions.dxjcxx.sys.authCode,
                                cllx: "dxjcxx"
                            }
                        }
                        copyByPathMap(jcxxqqcs, myData, buttonOptions.jcxxqqcskz)
                        await axios.post(jcxxqqcs, { ctdy: false }).then((rep) => {
                            tdxjcxx = rep.data;
                        });
                        if (buttonOptions.dxjcxx.obj) {
                            //该按钮设置了数据
                            tdxjcxx.obj = buttonOptions.dxjcxx.obj;
                        }
                        tdxjcxx.sys.cllx = getByPath(buttonOptions, "dxjcxx.sys.cllx");
                        if (!tdxjcxx.sys.cllx) {
                            //按钮没有设置处理类型时，采用按钮的处理类型
                            tdxjcxx.sys.cllx = cllx
                        }
                    } else {
                        //默认采用按钮的处理类型
                        tdxjcxx.sys.cllx = cllx
                    }
                    if (!tdxjcxx.obj) {
                        tdxjcxx.obj = myData.dxjcxx.obj || {};
                    }
                    tdxjcxx.sys.ylParams = clone(myData.selectReqData);
                    tdxjcxx.sys.ylParams.sys.ids = ids;
                    copyByPathMap(tdxjcxx, myData, buttonOptions.jcxxkz)
                    //根据处理类型扩展判断弹框关闭是否需要二次确认
                    xModalBeforeClose.value = tdxjcxx.cllxkz[tdxjcxx.sys.cllx] && tdxjcxx.cllxkz[tdxjcxx.sys.cllx].tcgbts;

                    await nextTick()
                    await nextTick()
                    xUpdate.value.tcck(tdxjcxx, tdxjcxx.sys.cllx, buttonOptions, tdxjcxx.obj, ids);
                    break
                //文件下载
                case "download":
                    let params = clone(myData.selectReqData);
                    params.sys.cllx = cllx;
                    params.sys.ids = ids;
                    params.sys.xzms = 2;
                    params.page.listRequired = true;
                    params.page.totalRequired = false;
                    if (buttonOptions.dcwjm) {
                        //后台定制了文件名称
                        params.sys.dcwjm = buttonOptions.dcwjm
                    }
                    if (params.sys.cllx === 'dcsj' && buttonOptions.yxfydc && myData.pagerConfig.total > buttonOptions.params.page.pageSize) {
                        exportModalRef.value.init(assignDeep(params, buttonOptions.params), myData.pagerConfig.total)
                        myData.exportModalVisible = true;
                        return
                    }
                    copyByPathMap(params, myData, buttonOptions.jcxxkz)

                    axios.download(assignDeep(params, buttonOptions.params));
                    break
                //文件上传
                case "upload":
                    //调用文件上传接口后，再拿着返回的文件对象信息请求设置的处理类型。
                    const { files } = await VXETable.readFile({
                        multiple: true
                    })
                    //后台请求参数,默认
                    let htqqcs = {
                        "sys.authCode": "QTQX",
                        "sys.cllx": "upload",
                        // "yobj.sjzt": "default"
                    }
                    //复制参数
                    if (buttonOptions.jcxxkz) {
                        for (const key in buttonOptions.jcxxkz) {
                            htqqcs[key] = getByPath(myData, buttonOptions.jcxxkz[key])
                        }
                    }
                    //合并固定参数
                    htqqcs = assignDeep(htqqcs, buttonOptions.params);
                    const formBody = new FormData();
                    for (const key in htqqcs) {
                        formBody.append(key, htqqcs[key]);
                    }
                    for (let i = 0; i < files.length; i++) {
                        formBody.append("files", files[i]);
                    }
                    axios.upload(formBody).then((res: any) => {
                        if (res.status) {
                            if (!buttonOptions.schqq) {
                                ElMessage.info(res.msg ? res.msg : "上传成功")
                                if (buttonOptions.sfsxym !== false) {
                                    //修改数据的场景要重新统计总量
                                    myData.selectReqData.page.totalRequired = true
                                    getList()
                                }
                            } else {
                                axios.post(assignDeep({
                                    sys: {
                                        authCode: myData.dxjcxx.sys.authCode,
                                        cllx: cllx,
                                        files: res.data,
                                    },
                                }, buttonOptions.params2)).then((response: any) => {
                                    ElMessage.info({
                                        message: response.msg ? response.msg : "操作成功",
                                        showClose: true,
                                        duration: 10000
                                    })
                                    if (buttonOptions.sfsxym !== false) {
                                        //修改数据的场景要重新统计总量
                                        myData.selectReqData.page.totalRequired = true
                                        getList()
                                    }
                                }).catch(error => {
                                    if (buttonOptions.sccwxq === false) {
                                        console.info("处理异常不展示错误详情");
                                    } else if (error.code === 1001) {
                                        axios.post({
                                            sjdx: {
                                                "dxdm": "SYS_LOG_SJSCCW"
                                            },
                                            sys: {
                                                "cllx": "dxjcxx"
                                            }
                                        }).then(async res0 => {
                                            myData.tcckProps.title = "数据上传错误列表"
                                            myData.tcckShow = true
                                            await nextTick()
                                            await nextTick()
                                            res0.data.sys.cllx = 'select'
                                            res0.data.fields.qqid.cxmrz = error.qqid
                                            xUpdate.value.initPage(res0.data);
                                        })
                                    }
                                });
                            }
                        }
                    });
                    break
                //页面参数替换
                case "ymcsth":
                    myData = assignDeep(myData, buttonOptions.params)
                    ElMessage.info("[" + content + "]设置成功")
                    break
                case "insertAt":
                    // myData.tableData.push({})
                    xGrid.value.insertAt({}, -1)
                    xGrid.value.refreshColumn()
                    break
                case "newTab":
                    if (isEmpty(buttonOptions.url)) {
                        ElMessage.error("打开新tab的方式必须配置url参数");
                        return
                    }
                    let Baseurl = "";
                    let urlParams = "";
                    if (buttonOptions.url.includes("?")) {
                        urlParams = "&"
                    } else {
                        urlParams = "?"
                    }
                    if (buttonOptions.url.slice(4) !== "http") {
                        Baseurl = window.location.href.split("#")[0]
                    }
                    if (buttonOptions.jcxxkz) {
                        for (const key in buttonOptions.jcxxkz) {
                            urlParams += key + "=" + getByPath(myData, buttonOptions.jcxxkz[key]) + "&"
                        }
                    }
                    window.open(Baseurl + buttonOptions.url + urlParams, "_blank")
                    break
                default:
                    ElMessage.error("暂不支持该处理方式：" + clfs);
            }
        }

        /**
         * 页面初始化
         * @param dxjcxx 外部提供的基础信息
         * @returns {Promise<void>}
         */
        const initPage = async (dxjcxx: any) => {
            myData.formItems = []
            myData.tableColumn = []
            myData.tableData = []
            myData.formData = {}
            dxjcxx = await initJcxx(dxjcxx);
            initTableTree(dxjcxx);
            initTableCheckbox(dxjcxx)
            //此处再进行一次myData与对象中的该处理类型扩展合并,便于对页面其他参数的设置
            myData = assignDeep(myData, getByPath(dxjcxx, "cllxkz.select.pagekz"))
            if (myData.dxjcxx.cllxkz.select.qxpz.plclLeft) {
                for (let i in myData.dxjcxx.cllxkz.select.qxpz.plclLeft) {
                    if (isEmpty(myData.dxjcxx.cllxkz.select.qxpz.plclLeft[i].px)) {
                        myData.dxjcxx.cllxkz.select.qxpz.plclLeft[i].px = 99
                    }
                }
                myData.dxjcxx.cllxkz.select.qxpz.plclLeft = objKeySort(myData.dxjcxx.cllxkz.select.qxpz.plclLeft)
            }
            if (myData.dxjcxx.cllxkz.select.qxpz.plclRight) {
                for (let i in myData.dxjcxx.cllxkz.select.qxpz.plclRight) {
                    if (isEmpty(myData.dxjcxx.cllxkz.select.qxpz.plclRight[i].px)) {
                        myData.dxjcxx.cllxkz.select.qxpz.plclRight[i].px = 99
                    }
                }
                myData.dxjcxx.cllxkz.select.qxpz.plclRight = objKeySort(myData.dxjcxx.cllxkz.select.qxpz.plclRight)
            }
            if (myData.dxjcxx.cllxkz.select.lbmskz) {
                myData.lbmsList = myData.dxjcxx.cllxkz.select.lbmskz.lbmsList
                myData.lbmsValue = myData.dxjcxx.cllxkz.select.lbmskz.myLbms
            }

            /**
             * 查询请求数据
             */
            myData.selectReqData = {
                sjdx: {
                    dxdm: myData.dxjcxx.sjdx.dxdm,
                    id: myData.dxjcxx.sjdx.id
                },
                sys: {
                    authCode: myData.dxjcxx.sys.authCode,
                    cllx: "select"
                },
                page: {
                    pageNumber: '',
                    orderBy: '',
                    totalRequired: true,
                    pageSize: myData.pagerConfig.pageSize
                },
                yobj: {}
            }
            const fields = myData.dxjcxx.fields;
            for (const key in fields) {
                const f = fields[key];
                initTableRule(f);
                initCxx(f);
                initTableColumn(f);
            }
            //读取是否配置了查询框最多显示数量
            if (myData.dxjcxx.cllxkz.select && myData.dxjcxx.cllxkz.select.zscxxsl && myData.dxjcxx.cllxkz.select.zscxxsl > 0) {
                myData.formItems.map((item: any, index: number) => {
                    if (index > myData.dxjcxx.cllxkz.select.zscxxsl - 1 && item.field !== "myCxan")
                        item.folding = true;
                })
            }
            if (myData.dxjcxx.cllxkz.select && myData.dxjcxx.cllxkz.select.myHelp) {
                myData.myHelp = myData.dxjcxx.cllxkz.select.myHelp
            }

            if (props.searchParams) {
                myData.formData = props.searchParams
            }
            //初始化查询
            if (myData.dxjcxx.sjdx.cscx === '1') {
                await getList();
            }
            if (myData.dssx && myData.setIntervalFun === undefined) {
                myData.setIntervalFun = setInterval(async () => {
                    await getList(false);
                }, myData.dssx * 1000)
            } else {
                if (myData.setIntervalFun) {
                    clearInterval(myData.setIntervalFun)
                }
            }
        }
        await initPage(props.dxjcxx)
        watch(() => props.dxjcxx, function (newJcxx) {
            initPage(newJcxx);
        })
        watch(() => props.searchParams, function (newVal: any) {
            myData.formData = newVal
            getList()
        })
        /**
         * 弹窗页面引用
         * @type {Ref<UnwrapRef<{}>>}
         */
        const xUpdate = ref();

        const xModalBeforeClose = ref(false);
        /**
         * 弹窗引用
         * @type {Ref<UnwrapRef<{}>>}
         */
        const xModal = ref();

        const audio = ref();
        const audioPlay = ref(false);
        /**
         * 弹窗页面回调
         * @param isFlush 是否刷新页面
         */
        const fromClose = (isFlush: any, tdxjcxx: any, isCustomClose?: any) => {
            myData.tcckShow = false
            if (isCustomClose != undefined) {
                xModalBeforeClose.value = isCustomClose
            }
            if (isFlush !== false) {
                myData.selectReqData.page.totalRequired = true
                //判断是否是树形结构并且拥有父节点
                if (!isEmpty(getByPath(myData.dxjcxx, "cllxkz.select.tree.parentField")) && tdxjcxx.formData[myData.treeConfig.parentField]) {
                    let data = findRow(tdxjcxx.formData[myData.treeConfig.parentField], myData.tableData);
                    data && xGrid.value.reloadTreeExpand(data)
                }
                else {
                    getList()
                }
            }
        }
        const changeAudioPlay = (value: boolean) => {
            audioPlay.value = value
        }
        const menuClick = ({ row, column }: any) => {
            xGrid.value.clearEdit()
            if (myData.tableData) {
                myData.tableData.map((item: any) => {
                    item[column.field] = row[column.field]
                })
            }
        }
        const changeExportModal = (value: boolean) => {
            myData.exportModalVisible = value
        }
        const checkboxChange = ({ records }: any) => {
            if (props.mode === 'zjms')
                context.emit('update:modelValue', records)
        }
        const beforeHideMethod = async () => {
            if (xModalBeforeClose.value) {
                let type = 'confirm'
                await ElMessageBox.confirm(
                    '您确定要关闭吗？',
                    '警告',
                    {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning',
                    }
                )
                    .then(() => {
                    })
                    .catch(() => {
                        myData.tcckShow = true
                        type = 'canel'
                    })
                if (type === 'canel') {
                    return new Error()
                }
            }
        }

        const myHelpClick = async (id?: any) => {
            axios.post({
                sys: {
                    authCode: "QTQX_WDGL",
                    cllx: "dxjcxx"
                },
            }).then(async res => {
                myData.tcckProps.title = "帮助文档"
                myData.tcckShow = true
                await nextTick()
                await nextTick()
                xModal.value.maximize()
                xUpdate.value.initPage(res.data, 'ckwd', null, { ckwd: id });
            })
        }

        return {
            lbmsChange,
            myHelpClick,
            changeExportModal,
            myData,
            qxpz,
            getTotal,
            xFrom,
            xGrid,
            exportModalRef,
            search,
            pageChange,
            sortChange,
            hqqxlb,
            plcl,
            tHasAuth,
            //弹窗
            xUpdate,
            xModal,
            audio,
            beforeHideMethod,
            fromClose,
            audioPlay,
            ctyd,
            changeAudioPlay,
            objKeySort,
            menuClick,
            checkboxChange,
            echartsRef
        }
    },
    render() {
        this.myData.slots.top = () => {
            return <div v-show={this.myData.tableHideClass === 'hidden'} ref="echartsRef" style="width:100%;height:550px"></div>
        }
        this.myData.slots.toolbar_left = () => {
            return (
                <div>
                    <span class={classes.pageMainHeaderTitle}>{this.myData.dxjcxx.cllxkz.select.lbTitle ? this.myData.dxjcxx.cllxkz.select.lbTitle : "数据展示"}</span>
                    {this.tHasAuth('czqbsj') ? (<vxe-checkbox class={classes.vxeCheckbox} v-model={this.myData.czqbsj} content="操作全部"></vxe-checkbox>) : null}
                    <el-button-group size="small">
                        {this.myData.editCofnig.enabled && this.mode === 'ybms' ? (<el-button type="primary"
                                                                                              onClick={() => this.plcl('plbc', { content: '保存', buttonOptions: { clfs: 'plbc', htqqts: true, sfxyxzjl: false } })}
                                                                                              size={this.myData.options.elementSize}>保存</el-button>) : null}
                        {Object.keys(this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, false)).map((item) => {
                            return (
                                <el-button {...this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, false)[item]} onClick={() => this.plcl(item, this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, false)[item])} size={this.myData.options.elementSize}>
                                    {this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, false)[item].content}
                                </el-button>
                            )
                        })}
                        {Object.keys(this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, true)).length > 0 ? (
                            <el-dropdown
                                v-slots={{
                                    dropdown: () => {
                                        return (
                                            <el-dropdown-menu>
                                                {Object.keys(this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, true)).map((item) => {
                                                    return (
                                                        <el-dropdown-item {...this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, true)[item]} onClick={() => this.plcl(item, this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, true)[item])}>
                                                            {this.hqqxlb(this.qxpz.plclLeft, this.qxpz.plclLeftZdans, true)[item].content}
                                                        </el-dropdown-item>)
                                                })}
                                            </el-dropdown-menu>
                                        )
                                    }
                                }}
                            >
                                <el-button type="primary" size={this.myData.options.elementSize} >
                                    更多操作<el-icon><ArrowDown /></el-icon>
                                </el-button>
                            </el-dropdown>
                        ) : null}
                    </el-button-group>
                </div>
            )
        }
        this.myData.slots.toolbar_right = () => {
            return (
                <div style="display: flex;align-items:center">
                    <el-button-group>
                        {Object.keys(this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, false)).map((item: any, index: number) => {
                            return (
                                <el-button {...this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, false)[item]} onClick={() => this.plcl(item, this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, false)[item])} size={this.myData.options.elementSize}>
                                    {this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, false)[item].content}
                                </el-button>
                            );
                        })}
                        {Object.keys(this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, true)).length > 0 ? (<el-dropdown>
                            <el-button size={this.myData.options.elementSize}
                                       v-slots={{
                                           dropdown: () => {
                                               return (
                                                   <el-dropdown-menu>
                                                       {Object.keys(this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, true)).map((item: any, index: number) => {
                                                           return (
                                                               <el-dropdown-item onClick={() => this.plcl(item, this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, true)[item])} {...this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, true)[item]}>
                                                                   {this.hqqxlb(this.qxpz.plclRight, this.qxpz.plclRightZdans, true)[item].content}
                                                               </el-dropdown-item>
                                                           );
                                                       })}
                                                   </el-dropdown-menu>
                                               )
                                           }
                                       }}>
                                更多操作<el-icon><ArrowDown /></el-icon>
                            </el-button>
                        </el-dropdown>) : null}
                    </el-button-group>
                    {this.myData.lbmsList && this.myData.lbmsList.length > 1 ?
                        <vxe-select onChange={this.lbmsChange} style="margin-left:4px;width:70px" modelValue={this.myData.lbmsValue} options={this.myData.lbmsList} optionProps={{ value: 'dm', label: 'mc' }} />
                        : null}
                </div>)
        }
        this.myData.slots.lbcz = ({ row, column }: any) => {
            return (
                <>
                    {Object.keys(this.hqqxlb(column.params.btns, column.params.zdans, false,row)).map((item: any, index: number) => {
                        return (
                            <vxe-button {...this.hqqxlb(column.params.btns, column.params.zdans, false,row)[item]}
                                        onClick={() => this.plcl(item, this.hqqxlb(column.params.btns, column.params.zdans, false,row)[item], row)}
                            />
                        );
                    })}
                    {Object.keys(this.hqqxlb(column.params.btns, column.params.zdans, true,row)).length > 0 ? (<vxe-button
                        type='text' status="primary" transfer content="更多操作"
                        v-slots={{
                            dropdowns: () => {
                                return (
                                    <>
                                        {Object.keys(this.hqqxlb(column.params.btns, column.params.zdans, true,row)).map((item: any, index: number) => {
                                            return (
                                                <vxe-button {...this.hqqxlb(column.params.btns, column.params.zdans, true,row)[item]} onClick={() => this.plcl(item, this.hqqxlb(column.params.btns, column.params.zdans, true)[item], row)} />
                                            );
                                        })}
                                    </>
                                )
                            }
                        }}>
                    </vxe-button>) : null}
                </>
            )
        }
        this.myData.slots.MyImage = ({ row, column }: any) => {
            // 图片预览列表
            let srclist:any;
            function imgurl(val: String, ctdy: string) {
                let url;
                let arr = val.split(",");
                if (arr[0].length === 32 && arr[0].indexOf(".")==-1) {
                    url = axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id="
                        + arr[0] + (ctdy ? ("&sys.ctdy=" + ctdy) : "");
                    srclist = [];
                    for (let i in arr){
                        //处理多张图片的场景
                        srclist.push(axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id="
                            + arr[i] + (ctdy ? ("&sys.ctdy=" + ctdy) : ""))
                    }
                } else if(arr[0].startsWith('http')||arr[0].indexOf(".")!=-1){
                    // http图片
                    // 处理多张图片的情况
                    url = arr[0]
                    srclist = arr
                } else {
                    //base64图片
                    url = "data:image/png;base64," + arr[0];
                }
                return url;
            }
            return row[column.field] ? <el-image
                style="height:100%;width:100%;"
                src={imgurl(row[column.field], this.ctyd)}
                preview-src-list={
                    srclist ? srclist:[imgurl(row[column.field], this.ctyd)]
                }
                fit="cover"
                v-slots={{
                    error: () => {
                        return (<el-icon style="font-size:22px;display:block;"><PictureFilled /></el-icon>)
                    }
                }}
            ></el-image> : <span>暂无图片</span>

        }
        this.myData.slots.pager = () => {
            return this.pagerShow ? <vxe-pager
                style="border-top: 1px solid #e8eaec"
                align="right"
                v-model:current-page={this.myData.pagerConfig.currentPage}
                v-model:page-size={this.myData.pagerConfig.pageSize}
                v-model:page-sizes={this.myData.pagerConfig.pageSizes}
                layouts={this.myData.fytjxs === "0" || this.myData.fytjxs === "1" ? ["PrevJump", "PrevPage", "JumpNumber", "NextPage", "NextJump", "FullJump", "PageCount", "Total", "Sizes"] : this.myData.fytjxs === "2" ? ["PrevJump", "PrevPage", "Number", "NextPage", "NextJump", "FullJump", "PageCount", "Sizes"] : ["PrevJump", "PrevPage", "Jump", "NextPage", "Sizes"]}
                onPage-change={this.pageChange}
                total={this.myData.pagerConfig.total}
                v-slots={{
                    right: () => {
                        if (this.myData.fytjxs === "2") {
                            return <span>数据量统计中请稍后……</span>
                        }
                        if (this.myData.fytjxs === "3") {
                            return <vxe-button onClick={this.getTotal}>统计</vxe-button>
                        }
                        else {
                            return null
                        }
                    }
                }}>
            </vxe-pager> : null
        }
        return (
            <div class="page-warp" style="minWidth:1000px">
                {this.myData.bjsy && !isEmpty(this.myData.bjsy) ? <audio ref="audio" hidden controls src={axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + this.myData.bjsy} ></audio> : null}
                {this.myData.dxjcxx.cllxkz.select.titleShow !== false ? (<div class="page-title"><el-icon><HomeFilled /></el-icon> {this.myData.dxjcxx.sys.cdlj||this.myData.dxjcxx.sjdx.dxmc}
                    {!isEmpty(this.myData.myHelp) ? <el-tooltip content="帮助" placement="bottom">
                        <el-link icon={QuestionFilled} onClick={() => this.myHelpClick(this.myData.myHelp)} underline={false} style="padding: 0 10px 4px" />
                    </el-tooltip> : null}
                    {this.myData.bjsy && !isEmpty(this.myData.bjsy) ? (this.audioPlay ? <el-tooltip class="item" effect="dark" content="点击关闭声音" placement="top">
                        <el-button type="primary" onClick={() => this.changeAudioPlay(false)} icon={Bell} size="small" circle style="float:right;"></el-button>
                    </el-tooltip> : <el-tooltip class="item" effect="dark" content="点击开启声音" placement="top">
                        <el-button type="danger" onClick={() => this.changeAudioPlay(true)} icon={MuteNotification} size="small" circle style="float:right;"></el-button></el-tooltip>) : null}
                </div>) : null}
                <div v-show={this.myData.dxjcxx.sjdx.cxtj === '1'} class="page-header">
                    <vxe-form ref="xFrom" data={this.myData.formData} items={this.myData.formItems} rules={this.myData.formRule} onSubmit={this.search} v-slots={this.myData.slots}>
                    </vxe-form>
                </div>
                <div class="page-main">
                    <div class="custom-table">
                        <vxe-grid
                            class={classes.grid} {...this.myData.gridOptions}
                            ref="xGrid" toolbar-config={this.myData.tableToolbar} columns={this.myData.tableColumn}
                            data={this.myData.tableData} export-config={this.myData.exportConfig} tree-config={this.myData.treeConfig}
                            seq-config={this.myData.seqConfig} row-id={this.myData.dxjcxx.sjdx.zjzd}
                            edit-config={this.myData.editCofnig} edit-rules={this.myData.tableRule}
                            menu-config={this.myData.menuConfig}
                            pager-config={this.myData.pagerConfig} checkbox-config={this.myData.checkboxConfig}
                            onCheckbox-change={this.checkboxChange}
                            onCheckbox-all={this.checkboxChange}
                            onPage-change={this.pageChange}
                            onSort-change={this.sortChange}
                            onMenu-click={this.menuClick}
                            row-class-name={this.myData.tableHideClass}
                            header-row-class-name={this.myData.tableHideClass}
                            v-slots={this.myData.slots}
                        >
                        </vxe-grid>
                    </div>
                </div>
                <vxe-modal ref="xModal" destroy-on-close v-model={this.myData.tcckShow} {...this.myData.tcckProps}
                           before-hide-method={this.beforeHideMethod} show-zoom resize>
                    <MyForm ref="xUpdate" onClose={this.fromClose} slots={this.myData.slots}></MyForm>
                </vxe-modal>
                <MyExportModal ref="exportModalRef" visible={this.myData.exportModalVisible} onUpdate:modelValue={this.changeExportModal}></MyExportModal>
            </div >
        )
    }
})
