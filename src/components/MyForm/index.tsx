import { defineComponent, nextTick, reactive, ref } from 'vue'
import { ElMessage, ElMessageBox } from "element-plus";
import { options } from "@/plugins/vxe-table";
import axios from "@/axios";
import MyForm from ".";
import {assignDeep, clone, copyByPathMap, dayjsMethod, getByPath, isEmpty, setByPath, uuid} from "@/utils/common";
import classes from './index.module.scss';
import { isCardID, isEmail } from "./validate"
import html2canvas from "html2canvas";

export default defineComponent({
    name: "MyForm",
    components: {},
    props: {
        /**
         * 父页面传入对象基础信息时，本页面将不再重复请求
         */
        dxjcxx: {
            type: Object
        },
        mode: {
            type: String,
            //默认查询页面（ybms），支持：ybms(一般模式)、zjms(组件模式)
            default: "ybms"
        },
        MyFormVNode: {
            type: Object
        },
        slots: {
            type: Object
        }
    },
    //关闭弹窗
    emits: ["close", "update:modelValue"],
    /**
     * from组件主要进行页面布局，然后需要展示的都可以以组件形式集成进去
     * @param props
     * @param context
     * @returns {Promise<{xForm: Ref<UnwrapRef<{}>>, tcck: tcck, close: () => void, myData: UnwrapNestedRefs<{formItems: *[], options, formData: {}, formRule: {}, dxjcxx: {}, timestamp: number}>}>}
     */
    setup: async (props, context) => {
        /**
         * 我的组件数据
         * @type {UnwrapNestedRefs<{formItems: *[], options, formData: {}, formRule: {}, dxjcxx: {}, timestamp: number}>}
         */

        let myData: any = reactive({
            /**
             * 全局统一配置
             */
            options: options,
            slots: props.slots,
            group: {},
            /**
             * 当前时间作为id
             */
            timestamp: new Date().getTime(),
            /**
             * 对象基础信息
             */
            dxjcxx: {
                sjdx: {
                    zjzd: "",
                    dxmc: "",
                },
                sys: {
                    authCode: ""
                }
            },
            /**
             * 表单数据：也表单绑定，用看到的值
             */
            formData: {
            },
            /**
             * 初始化表单数据
             */
            initFormData: {
            },
            /**
             * 表单数据原始值：外部传入的值
             */
            obj: {

            },
            /**
             * 表单字段信息
             */
            formItems: [] as any[],
            /**
             * 表单字段验证规则
             */
            formRule: {},
            /**
             * 弹出窗口是否展示
             */
            tcckShow: false,
            tcckProps: {
                /**
                 * 弹出窗口标题
                 */
                title: "",
                width: undefined,
                height: undefined,
            },
        })
        /**
         * 表单引用对象
         * @type {Ref<UnwrapRef<{}>>}
         */
        const xForm = ref();

        const xModalBeforeClose = ref(false);

        /**
         * 根据对象基础信息进行页面初始化
         * @param dxjcxx 对象基础信息
         * @param cllx 处理类型
         * @param boptions 权限信息
         * @param obj 当前操作的行
         * @param ids 列表选择的id数组
         * @returns {Promise<void>}
         */
        const initPage = async (dxjcxx: any, cllx?: any, boptions?: any, obj?: any, ids?: any) => {
            if (!cllx) {
                //外部没传处理类型时，采用基础新中的处理类型
                cllx = dxjcxx.sys.cllx
            }
            myData.formData = {};
            myData.initFormData = {};
            //重置表单项
            myData.formItems = []
            myData.formRule = {}
            if (!obj) {
                obj = dxjcxx.yobj
            }
            let isObj = obj && Object.keys(obj).length > 0;
            if (!dxjcxx.fields) {
                //加载对象基础信息
                let params = JSON.parse(JSON.stringify(dxjcxx))
                params.sys.cllx = "dxjcxx"
                await axios.post(params, { ctdy: false }).then(async (rep) => {
                    assignDeep(rep.data, dxjcxx);
                    //设置处理类型
                    rep.data.sys.cllx = cllx;
                    dxjcxx = rep.data;
                });
            }
            myData.dxjcxx = dxjcxx;
            if (isObj) {
                myData.obj = obj
                //深拷贝数据，避免修改对外部的影响
                myData.formData = clone(obj);
                myData.initFormData = clone(obj);
            }
            const fields = dxjcxx.fields;
            for (const key in fields) {
                /**
                 * 配置字段信息
                 */
                const f = fields[key];
                let lxkz;
                if (getByPath(f, "kzxxObj.cllxkz." + cllx)) {
                    lxkz = f.kzxxObj.cllxkz[cllx];
                    //默认值
                    if (!isObj && lxkz.default) {
                        let defaultVal = lxkz.default;
                        if (defaultVal === "sys_guid()") {
                            defaultVal = uuid();
                        } else if (f.kjlx === 'ElDatePicker') {
                            const times = JSON.parse(f.xzmrz);
                            defaultVal = dayjsMethod(times)
                        }
                        myData.formData[f.zddm] = defaultVal;
                    }
                }
            }
            /**
             * 表单项
             */
            let fi: any;
            let ssfz = null;
            for (const key in fields) {
                /**
                 * 配置字段信息
                 */
                const f = fields[key];
                let lxkz;
                if(getByPath(f, "kzxxObj.cllxkz." + cllx)){
                    lxkz = f.kzxxObj.cllxkz[cllx];
                }
                //根据处理类型判断是否展示该字段
                if (lxkz && f.kzxxObj.cllxkz[cllx].show) {
                    //添加校验规则
                    myData.formRule[f.zddm] = [
                        {
                            validator({ itemValue, rule, rules, data, property }: any) {
                                //自定义校验规则，跟后台规则一致的前端实现
                                console.log(itemValue);
                            }
                        }
                    ]
                    if (getByPath(f.kzxxObj, "yzgz." + cllx + ".notBlank")) {
                        //单独处理非空校验，便于在页面给用户红色星号提示必填字段
                        myData.formRule[f.zddm].push({ required: true, message: f.zdmc + '必填', trigger: 'change' });
                    }
                    //长度限制
                    if (getByPath(f.kzxxObj, "yzgz." + cllx + ".length")) {
                        if (getByPath(f.kzxxObj, "yzgz." + cllx + ".length.min")) {
                            myData.formRule[f.zddm].push({ message: f.zdmc + '不能小于' + getByPath(f.kzxxObj, "yzgz." + cllx + ".length.min"), min: getByPath(f.kzxxObj, "yzgz." + cllx + ".length.min"), trigger: 'change' });
                        }
                        if (getByPath(f.kzxxObj, "yzgz." + cllx + ".length.max")) {
                            myData.formRule[f.zddm].push({ message: f.zdmc + '不能大于' + getByPath(f.kzxxObj, "yzgz." + cllx + ".length.max"), max: getByPath(f.kzxxObj, "yzgz." + cllx + ".length.max"), trigger: 'change' });
                        }
                        //单独处理非空校验，便于在页面给用户红色星号提示必填字段
                    }
                    //正则校验
                    if (getByPath(f.kzxxObj, "yzgz." + cllx + ".zzbds")) {
                        myData.formRule[f.zddm].push({ message: getByPath(f.kzxxObj, "yzgz." + cllx + ".zzbds.ts"), pattern: getByPath(f.kzxxObj, "yzgz." + cllx + ".zzbds.value"), trigger: 'change' });
                    }
                    //数字校验
                    if (getByPath(f.kzxxObj, "yzgz." + cllx + ".number")) {
                        myData.formRule[f.zddm].push({ message: f.zdmc + '要求是数字', type: 'number', trigger: 'change' });
                    }
                    //身份证号
                    if (getByPath(f.kzxxObj, "yzgz." + cllx + ".sfzh")) {
                        myData.formRule[f.zddm].push({
                            validator({ itemValue }: any) {
                                if (!isEmpty(itemValue) && !isCardID(itemValue)) {
                                    return new Error('身份证号不正确')
                                }
                            }
                        })
                    }
                    //email
                    if (getByPath(f.kzxxObj, "yzgz." + cllx + ".email")) {
                        myData.formRule[f.zddm].push({
                            validator({ itemValue }: any) {
                                if (!isEmpty(itemValue) && !isEmail(itemValue)) {
                                    return new Error('邮箱格式不正确')
                                }
                            }
                        })
                    }

                    fi = {
                        field: f.zddm,
                        title: f.zdmc,
                        span: 8,
                        itemRender: {
                            name: f.kjlx,
                            props: {
                                placeholder: f.zdts,
                                disabled: lxkz.disabled,
                                readonly: lxkz.readonly
                            }
                        }
                    }
                    fi.itemRender.props.zdhd = function (option: any) {
                        plcl(null, option);
                    }
                    if (ssfz && f.kjlx !== 'MyGroup') {
                        myData.group[ssfz].push(fi.field)
                    }
                    let zdkd = lxkz.zdkd;
                    if (zdkd > 10000) {
                        fi.span = Math.round(zdkd % 10000 / 100);
                    }
                    //控件属性统一设置部分
                    fi.itemRender.props.slots = myData.slots
                    fi.itemRender.props.myData = myData
                    if (!fi.itemRender.props.key) {
                        fi.itemRender.props.key = cllx + f.id + myData.formData[myData.dxjcxx.sjdx.zjzd];
                    }
                    if (fi.itemRender.props.disabled || fi.itemRender.props.readonly) {
                        fi.itemRender.props.clearable = false
                    }
                    switch (f.kjlx) {
                        case 'ElCascader':
                            fi.itemRender.name = 'MyCascader';
                            fi.itemRender.props.size = options.input.size;
                            fi.itemRender.props.multiple = f.zddx === '1';
                            if (fi.itemRender.props.clearable !== false) {
                                fi.itemRender.props.clearable = options.input.clearable
                            }
                            fi.itemRender.props.zdlb = f.zdzdlb;
                            break
                        case '$switch':
                        case '$select':
                            //还要考虑字典树
                            if (f.zddx === '1') {
                                //大字典，采用下拉分页搜索框、
                                fi.itemRender.name = 'MyDownListMultiple';
                            } else if (f.zdfy === '1') {
                                //大字典，采用下拉分页搜索框、
                                fi.itemRender.name = 'MyDownList';
                            } else {
                                //普通下拉框
                                fi.itemRender.name = 'MySelect';
                            }
                            fi.itemRender.props.zdlb = f.zdzdlb;
                            break
                        case '$radio':
                            //还要考虑字典树
                            fi.itemRender.name = 'MyRadio';
                            fi.itemRender.props.zdlb = f.zdzdlb;
                            break
                        case '$checkbox':
                            //还要考虑字典树
                            fi.itemRender.name = 'MyCheckbox';
                            fi.itemRender.props.zdlb = f.zdzdlb;
                            break
                        case 'jsoneditor':
                            fi.itemRender.props.codeType = "json";
                            fi.itemRender.name = 'MyMarkDown';
                            if (fi.itemRender.props.clearable === false) {
                                fi.itemRender.props.mode = "preview";
                            }
                            break
                        case 'MyMarkDown':
                            if (fi.itemRender.props.clearable === false) {
                                fi.itemRender.props.mode = "preview";
                            }
                            break
                        case 'MyImage':
                            fi.itemRender.props.cllx = cllx;
                            break
                        case 'MyFile':
                            fi.itemRender.props.zdlb = f.sjdx;
                            fi.span = 24;
                            break
                        case 'MyDownListMultiple':
                            fi.itemRender.props.zdlb = f.zdzdlb;
                            fi.span = 24
                            break
                        case 'ElDatePicker':
                            //时间选择器
                            fi.itemRender.props.type = "datetime";
                            fi.itemRender.props.valueFormat = "YYYYMMDDHHmmss";
                            fi.itemRender.props.size = options.input.size;
                            if (fi.itemRender.props.clearable !== false) {
                                fi.itemRender.props.clearable = options.input.clearable
                            }
                            break
                        case '$textarea':
                            break
                        case '$buttons':
                            //按钮组
                            let children = [];
                            //获取配置的按钮组
                            let $buttons = getByPath(f.kzxxObj, "kjkz.btns");
                            for (let i in $buttons) {
                                //处理类型设置到名称，便于后续按钮事件中使用
                                $buttons[i].name = i;

                                if (isEmpty($buttons[i].px)) {
                                    //默认增加px字段
                                    $buttons[i].px = 99
                                }
                                //按钮点击回调方法
                                $buttons[i].click = function (option: any) {
                                    plcl(option.props.name, option.props);
                                }
                                children.push({ props: $buttons[i] })
                            }
                            //排序
                            children.sort((a, b) => {
                                return a.props.px - b.props.px
                            })

                            fi.itemRender.children = children;
                            break
                        case 'MySelectGrid':
                            //查询表格
                            //获取配置的对象信息
                            let authCode = getByPath(f.kzxxObj, "kjkz.itemRender.props.dxjcxx.sys.authCode");
                            let dxdm = getByPath(f.kzxxObj, "kjkz.itemRender.props.dxjcxx.sjdx.dxdm");
                            if (authCode || dxdm) {
                                //配置了对象信息则展示配置的对象的查询列表
                                fi.itemRender.props.dxjcxx = {
                                    sjdx: {
                                        dxdm: dxdm
                                    },
                                    sys: {
                                        authCode: authCode,
                                        cllx: "select"
                                    }
                                }
                            } else {
                                //没有配置则展示当前页面对象的查询列表
                                fi.itemRender.props.dxjcxx = dxjcxx
                            }
                            fi.itemRender.props.key = new Date().getTime();
                            //设置初始编辑状态
                            fi.itemRender.props.csbjzt = lxkz.csbjzt;
                            if (myData.dxjcxx.sjdx.zjzd) {
                                //将主键设置为表格的值
                                if (!myData.formData[f.zddm]) {
                                    myData.formData[f.zddm] = [];
                                }
                            }
                            break
                        case "MyMenu":
                            fi.title = "";
                            fi.itemRender.name = 'MyMenu';
                            break
                        case "password":
                            //密码框
                            if (fi.itemRender.props.clearable !== false) {
                                //编辑状态
                                fi.itemRender.name = '$input'
                                fi.itemRender.props.type = "password"
                                //编辑状态清除原始值，避免没修过传到后台
                                delete myData.formData[f.zddm]
                            } else {
                                //不可编辑状态，显示查看密码的按钮
                                fi.itemRender.name = '$buttons'
                                let obj = {} as any
                                obj.value = myData.formData[f.zddm];
                                obj.field = f;
                                fi.itemRender.children = [
                                    {
                                        props: {
                                            "type": "button",
                                            "content": "查看密码",
                                            "status": "primary",
                                            name: "ckmm",
                                            buttonOptions: {
                                                "clfs": "tcck",
                                                sfgbtc: false,
                                                tckz: {
                                                    width: 400,
                                                    height: 200
                                                },
                                                "dxjcxx": {
                                                    "sjdx": {
                                                        "dxdm": "SYS_SJGL_MMCK"
                                                    },
                                                    "sys": {
                                                        "cllx": "insert"
                                                    },
                                                    "obj": obj
                                                },
                                            },
                                            click: function (option: any) {
                                                plcl(option.props.name, option.props);
                                            }
                                        }
                                    }
                                ]
                            }
                            break
                        case "MyGroup":
                            fi.itemRender.name = 'MyGroup';
                            fi.title = "";
                            fi.itemRender.props.title = f.zdmc;
                            fi.span = 24
                            fi.itemRender.props.ssfz = f.zddm;
                            ssfz = f.zddm;
                            myData.group[ssfz] = []
                            break
                        case 'MyRealTimeMsg':
                            fi.itemRender.props.mdddl = myData.dxjcxx.sjdx.dxdm;
                            fi.itemRender.props.mddxl = myData.obj[myData.dxjcxx.sjdx.zjzd];
                            fi.span = 24
                            break
                        case 'MyIconPicker':
                            fi.itemRender.name = 'MyIconPicker';
                            break
                        case 'slot':
                            fi.slots = { "default": fi.field }
                            break
                        case 'MyTransfer':
                            fi.itemRender.name = 'MyTransfer'
                            fi.itemRender.props.id = myData.formData.id
                            break
                        case 'MyEditor':
                            fi.itemRender.name = 'MyEditor';
                            break
                        case 'MyMixEditor':
                            fi.itemRender.name = 'MyMixEditor';
                            if (fi.itemRender.props.clearable === false) {
                                fi.itemRender.props.mode = "preview";
                            }
                            break
                        case 'MyDocument':
                            fi.title = "";
                            fi.span = 24;
                            break
                        case 'ElInputNumber':
                            break
                        default:
                            //默认普通输入框
                            fi.itemRender.name = '$input'
                    }
                    //合并字段的控件扩展，覆盖默认值，通用
                    assignDeep(fi, f.kzxxObj.kjkz);
                    //按处理类型设置
                    myData.formItems.push(assignDeep(fi, lxkz.kjkz));
                    //属性复制
                    copyByPathMap(fi, myData, f.kzxxObj.sxkz)
                }
            }
            //此处再进行一次myData与对象中的该处理类型扩展合并
            myData = assignDeep(myData, getByPath(dxjcxx, "cllxkz." + cllx + ".pagekz"))
        }
        //初始进入页面
        if (props.dxjcxx) {
            await initPage(props.dxjcxx);
        }
        /**
         * 作为弹窗窗口被调用
         * @param dxjcxx 对象基础信息
         * @param cllx 处理类型
         * @param options 权限信息
         * @param obj 当前操作的行
         * @param ids 列表选择的id数组
         */
        const tcck = (dxjcxx: any, cllx: any, options: any, obj: any, ids: any) => {
            initPage(dxjcxx, cllx, options, obj, ids);
        }
        /**
         * 通用后台请求
         * @param cllx 处理类型
         * @param buttonOptions 按钮参数
         * @param fromData 表单数据
         */
        const htqq = (htqqcs: any, buttonOptions: any) => {
            axios.post(htqqcs).then((req: any) => {
                ElMessage.success(req.msg);
                if (buttonOptions.jghbdbd === true) {
                    //请求结果合并到表单
                    myData.formData = assignDeep(myData.formData, req.data);
                }
                if (buttonOptions.sfgbtc === true) {
                    context.emit('close', buttonOptions.sfsxym,myData, false)
                }
            }).catch((req) => {
                console.log("处理异常", req)
                if (req.code === 1001) {
                    axios.post({
                        sjdx: {
                            "dxdm": "SYS_LOG_SJSCCW"
                        },
                        sys: {
                            "cllx": "dxjcxx"
                        }
                    }).then(async res0 => {
                        myData.tcckProps.title = "数据上传错误列表"
                        myData.tcckShow = true
                        await nextTick()
                        await nextTick()
                        res0.data.sys.cllx = 'select'
                        res0.data.fields.qqid.cxmrz = req.sys.qqid
                        xUpdate.value.initPage(res0.data);
                    })
                }
            });
        }
        /**
         * 表单中的按钮事件
         * @param cllx 处理类型
         * @param btnProps 按钮参数
         */
        const plcl = async (cllx: any, btnProps: any) => {
            const buttonOptions = btnProps.buttonOptions;
            myData.clhdcs = btnProps;
            //处理方式
            const clfs = buttonOptions.clfs || 'htqq';
            let content = btnProps.content;
            if (!(buttonOptions.sfjybd === false)) {
                //表单校验
                let jyjg = true
                await xForm.value.validate().then(function (errMap: any) {
                    if (errMap !== undefined && Object.keys(errMap).length > 0) {
                        jyjg = false
                    }
                })
                if (!jyjg) {
                    return
                }
            }
            let params: any;
            switch (clfs) {
                //后台请求
                case "htqq":
                    //后台请求参数
                    let htqqcs = {
                        sjdx: myData.dxjcxx.sjdx,
                        sys: {
                            authCode: myData.dxjcxx.sys.authCode,
                            cllx: cllx,
                            ylParams:myData.dxjcxx.sys.ylParams
                        },
                        yobj: myData.formData
                    }

                    //如果是更新比较更新的obj参数，只传修改过的值
                    if (cllx === 'update' && props.mode === 'ybms') {
                        //记录修改的obj
                        let updateObj = { }
                        updateObj[myData.dxjcxx.sjdx.zjzd]= myData.formData[myData.dxjcxx.sjdx.zjzd]
                        Object.keys(myData.formData).map(item => {
                            if (myData.formData[item] === myData.initFormData[item]) {
                                return
                            }
                            updateObj[item] = myData.formData[item]
                        })
                        htqqcs.yobj = updateObj
                    }
                    copyByPathMap(htqqcs, myData, buttonOptions.jcxxkz);
                    htqqcs = assignDeep(htqqcs, buttonOptions.params);
                    if (buttonOptions.htqqts) {
                        ElMessageBox.confirm("你确定" + content + "吗?", "提示", {
                            confirmButtonText: "确定",
                            cancelButtonText: "取消",
                            type: "warning"
                        }).then(() => {
                            if (props.mode === 'zjms') {
                                context.emit('update:modelValue', htqqcs.yobj)
                            } else {
                                htqq(htqqcs, buttonOptions)
                            }
                        }).catch(function () {
                            console.info("用户取消操作：" + content)
                        })
                    } else {
                        if (props.mode === 'zjms') {
                            context.emit('update:modelValue', htqqcs.yobj)
                        } else {
                            htqq(htqqcs, buttonOptions)
                        }
                    }
                    break
                //弹出窗口
                case "tcck":
                    //窗口高宽初始化
                    myData.tcckProps.width = "80%"
                    myData.tcckProps.height = "80%"
                    //窗口标题
                    myData.tcckProps.title = content + "【" + myData.dxjcxx.sjdx.dxmc + "】"
                    //窗口显示
                    myData.tcckShow = true
                    assignDeep(myData.tcckProps, buttonOptions.tckz)
                    if (buttonOptions.tcqp) {
                        //最大化
                        xModal.value.maximize()
                    } else {
                        //还原
                        xModal.value.revert()
                    }
                    let tdxjcxx = myData.dxjcxx as any;
                    if (buttonOptions.dxjcxx) {
                        //加载对象基础信息，该按钮设置了处理其他数据对象
                        let jcxxqqcs = {
                            sjdx: buttonOptions.dxjcxx.sjdx,
                            sys: {
                                authCode: buttonOptions.dxjcxx.sys.authCode,
                                cllx: "dxjcxx"
                            }
                        }
                        copyByPathMap(jcxxqqcs, myData, buttonOptions.jcxxqqcskz)
                        await axios.post(jcxxqqcs, { ctdy: false }).then((rep) => {
                            tdxjcxx = rep.data;
                        });
                        if (buttonOptions.dxjcxx.obj) {
                            //该按钮设置了数据
                            tdxjcxx.obj = buttonOptions.dxjcxx.obj;
                        }
                        tdxjcxx.sys.cllx = getByPath(buttonOptions, "dxjcxx.sys.cllx");
                        if (!tdxjcxx.sys.cllx) {
                            //按钮没有设置处理类型时，采用按钮的处理类型
                            tdxjcxx.sys.cllx = cllx
                        }
                    } else {
                        //默认采用按钮的处理类型
                        tdxjcxx.sys.cllx = cllx
                    }
                    if (!tdxjcxx.obj) {
                        tdxjcxx.obj = myData.formData;
                    }
                    copyByPathMap(tdxjcxx, myData, buttonOptions.jcxxkz)
                    await nextTick()
                    await nextTick()
                    xUpdate.value.tcck(tdxjcxx, tdxjcxx.sys.cllx, buttonOptions, tdxjcxx.obj);
                    break
                //关闭弹窗
                case "gbtc":
                    context.emit('close', buttonOptions.sfsxym,myData, buttonOptions.tcgbts)
                    break
                //文件下载
                case "download":
                    params = {
                        sjdx: myData.dxjcxx.sjdx,
                        yobj: myData.formData,
                        sys: {
                            authCode: myData.dxjcxx.sys.authCode,
                            cllx: cllx
                        }
                    };
                    copyByPathMap(params, myData, buttonOptions.jcxxkz)
                    if (buttonOptions.dcwjm) {
                        //后台定制了文件名称
                        params.sys.dcwjm = buttonOptions.dcwjm
                    }
                    axios.download(assignDeep(params, buttonOptions.params));
                    break
                //页面参数替换
                case "ymcsth":
                    params = buttonOptions.params
                    if (!params) {
                        params = {}
                    }
                    copyByPathMap(params, myData, buttonOptions.jcxxkz)
                    myData = assignDeep(myData, params)
                    if (content) {
                        ElMessage.info("[" + content + "]设置成功")
                    }
                    break
                //设值回调
                case "szhd":
                    xForm.value.getItems().map((item: any, index: number) => {
                        if (myData.group[buttonOptions.ssfz].includes(item.field)) {
                            if (index === xForm.value.getItems().length - 1 && item.itemRender.name === '$buttons') {
                                return
                            }
                            item.visible = buttonOptions.szzt
                        }
                    })
                    break;
                case "jtxz":
                    //截图下载
                    html2canvas(xForm.value.$el).then((canvas) => {
                        const imageDataUrl = canvas.toDataURL(); // 获取图像的Base64编码字符串

                        // 如果想要下载图像文件，则可以创建一个a标签并设置其href属性为imageDataUrl，再模拟点击该a标签触发下载操作
                        const link = document.createElement('a');
                        link.setAttribute('download', buttonOptions.jtmc||myData.dxjcxx.sjdx.dxmc+'.png');
                        link.href = imageDataUrl;
                        link.click();
                    });
                    break;
                default:
                    ElMessage.error("暂不支持该处理方式");
            }
        }
        /**
         * 弹窗页面引用
         * @type {Ref<UnwrapRef<{}>>}
         */
        const xUpdate = ref();
        /**
         * 弹窗引用
         * @type {Ref<UnwrapRef<{}>>}
         */
        const xModal = ref();
        /**
         * 弹窗页面回调
         * @param isFlush 是否刷新页面
         */
        const fromClose = (isFlush: any,tdxjcxx: any, isCustomClose?: boolean) => {
            myData.tcckShow = false
            if (isCustomClose != undefined) {
                xModalBeforeClose.value = isCustomClose
            }
            console.log("弹窗回调：" + isFlush)
        }

        const beforeHideMethod = async () => {
            if (xModalBeforeClose.value) {
                let type = 'confirm'
                await ElMessageBox.confirm(
                    '您确定要关闭吗？',
                    '警告',
                    {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning',
                    }
                )
                    .then(() => {
                    })
                    .catch(() => {
                        myData.tcckShow = true
                        type = 'canel'
                    })
                if (type === 'canel') {
                    return new Error()
                }
            }
        }

        return {
            myData,
            xForm,
            tcck,
            initPage,
            //弹窗
            xUpdate,
            xModal,
            beforeHideMethod,
            fromClose,
            // vxeFormNode
        }
    },
    render() {
        return (
            <div class={classes.myForm}>
                <vxe-form ref="xForm" key={this.myData.timestamp} data={this.myData.formData} rules={this.myData.formRule}
                          items={this.myData.formItems} v-slots={this.myData.slots}>
                </vxe-form>
                <vxe-modal ref="xModal" destroy-on-close v-model={this.myData.tcckShow} {...this.myData.tcckProps}
                           before-hide-method={this.beforeHideMethod} show-zoom resize>
                    <MyForm ref="xUpdate" onClose={this.fromClose} slots={this.myData.slots} ></MyForm>
                </vxe-modal>
            </div>
        )
    }
})
