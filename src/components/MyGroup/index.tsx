/*
 * @Date: 2022-09-12 16:50:02
 * @LastEditTime: 2023-02-13 10:35:20
 * @Description: 分组标题组件
 */
import { ArrowDown, ArrowUp } from '@element-plus/icons-vue';
import { computed, defineComponent, onMounted, reactive, watch } from 'vue'
import classes from './index.module.scss';

export default defineComponent({
    name: 'MyGroup',
    props: {
        icon: {
            type: String,
            default: "el-icon-edit"
        },
        title: {
            type: String
        },
        /**
         * 字段回调方法
         */
        zdhd: {
            type: Function
        },
        /**
         * 所属分组
         */
        ssfz: {
            type: String
        },
        /**
         * 收展状态
         */
        szzt: {
            type: Boolean,
            //默认展开
            default: true
        }
    },
    setup: async (props, context) => {
        const myData: any = reactive({
            szzt: props.szzt
        } as any);
        myData.szhd = () => {
            myData.szzt = !myData.szzt;
            if (props.zdhd) {
                props.zdhd({ buttonOptions: { sfjybd: false, clfs: "szhd", ssfz: props.ssfz, szzt: myData.szzt } });
            }
        }
        if (!props.szzt && props.zdhd) {
            props.zdhd({ buttonOptions: { sfjybd: false, clfs: "szhd", ssfz: props.ssfz, szzt: myData.szzt } });
        }
        return {
            myData
        }
    },
    render() {
        return (
            <div class={classes.title}>
                {this.icon ? <e-icon icon-name={this.icon} style="font-size: 20px;" /> : null}
                <div class={classes.titleText}>{this.title}</div>
                {this.myData.szzt ? <el-link icon={ArrowUp} onClick={this.myData.szhd}>收起</el-link> : <el-link icon={ArrowDown} onClick={this.myData.szhd}>展开</el-link>}
            </div>
        )
    }
})
