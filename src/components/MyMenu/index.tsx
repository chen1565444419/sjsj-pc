/*
 * @Date: 2022-09-06 15:01:26
 * @LastEditTime: 2022-10-27 20:42:24
 * @Description: 横向菜单组件
 */
import { defineComponent, reactive, ref } from 'vue'
import MenuItem from '@/components/MenuItem'
import SJDX from '@/components/Sjdx'
import axios from "@/axios";
import { isEmpty, assignDeep } from "@/utils/common"
import MyMenuItem from '@/components/MyMenuItem'
import { ElMessage } from 'element-plus';

export default defineComponent({
    name: 'MyMenu',
    inheritAttrs: true,
    props: {
        /**
         * @description: 授权码
         * @return {*}
         */
        authCode: {
            type: String
        },
        /**
         * @description: 展示模式 fullScreen为全屏
         * @return {*}
         */
        showMode: {
            type: String
        },
        /**
         * @description: 菜单扩展接收参数
         * @return {*}
         */
        params: {
            type: Object
        }
    },
    components: { MenuItem, MyMenuItem },
    setup(props) {
        const myData = reactive({
            /**
             * 菜单列表
             */
            menuList: [],
            //iframe地址
            iframeUrl: undefined as any,
            //菜单初始化代码
            firstDm: '',
            //默认滑动窗口高度
            scrollHeight: 300,
            //当前展示菜单数据
            menuItemData: {} as any,
        });
        /**
         * 配置显示模式为fullScreen时控制iframe高度
         */
        if (props.showMode === 'fullScreen') {
            // @ts-ignore
            myData.scrollHeight = document.getElementById("main-container").offsetHeight - 100
        }
        /**
         * 加载菜单
         * @param dm 菜单根节点
         * @param fqx 菜单父节点
         */
        const getMenuList = function (dm: any, fqx?: any) {
            axios.post({
                sys: {
                    authCode: "QTQX_QXXX",
                    cllx: "cds"
                },
                yobj: {
                    treeModel: 'cds',
                    treeRoot: dm,
                    fqx: fqx ? fqx : ''
                }
            }, { tloading: false }).then(response => {
                if (response.status) {
                    setMenuList(fqx, response.data[0].children)
                }
            }).catch((e) => {
                console.log("获取菜单失败", e)
            })
        }
        /**
         * 设置菜单数据
         * @param fqx 父权限
         * @param list 菜单列表
         */
        const setMenuList = (fqx: any, list: any) => {
            if (!fqx) {
                getMenuListByAuthCode(list, props.authCode)
            } else {
                myData.menuList.forEach((item: any) => {
                    if (item.dm === fqx) {
                        item.children = list
                    }
                })
            }
        }

        /**
         * @description: 获取iframe拼接后的uirl地址
         * @param {any} params
         * @param {any} url
         * @return {*}
         */
        const getQueryString = (params: any, url: any) => {
            if (!isEmpty(params)) {
                let pj = '?'
                if (url.indexOf('?') > -1) {
                    pj = '&'
                }
                var str = '';
                for (var Key in params) {
                    str += Key + '=' + params[Key] + '&';
                }
                return url + pj + str.substr(0, str.length - 1);
            } else {
                return url
            }
        }

        /**
         * @description: 获取菜单
         * @param {any} list
         * @param {any} authCode
         * @return {*}
         */
        const getMenuItems = (list: any, authCode: any): any => {
            let item = null;
            for (let index = 0; index < list.length; index += 1) {
                if (list[index].dm === authCode) {
                    item = list[index];
                    break;
                }
                if (list[index].children instanceof Array && list[index].children.length > 0) {
                    const sub = getMenuItems(list[index].children, authCode);
                    if (sub)
                        return sub;
                }
            }
            return item;
        }

        /**
         * @description: 获取第一项菜单代码
         * @param {any} data
         * @return {*}
         */
        const getFirstMenuItem = (data: any) => {
            if (data[0].children) {
                getFirstMenuItem(data[0].children)
            }
            else {
                myData.firstDm = data[0].dm
                myData.menuItemData = data[0]
            }
        }

        /**
         * @description: 根据AuthCode获取菜单列
         * @param {any} list
         * @param {any} authCode
         * @return {*}
         */
        const getMenuListByAuthCode = (list: any, authCode: any) => {
            for (let i = 0; i < list.length; i++) {
                if (list[i].dm === authCode) {
                    if (list[i].children) {
                        myData.menuList = list[i].children
                    }
                    else {
                        ElMessage.error("配置的目录下未找到菜单项");
                    }
                }
                if (list.children) {
                    getMenuListByAuthCode(list.children, authCode)
                }
            }
            getFirstMenuItem(myData.menuList)
        }

        const isCollapse = ref(false)
        const handleOpen = (key: string, keyPath: string[]) => {
            console.log(key, keyPath)
        }
        const handleClose = (key: string, keyPath: string[]) => {
            console.log(key, keyPath)
        }

        /**
         * @description: 菜单点击事件
         * @param {any} key
         * @param {any} keyPath
         * @return {*}
         */
        const handleSelect = (key: any, keyPath: any) => {
            let item = getMenuItems(myData.menuList, key)
            myData.menuItemData = item
            if (item.dzlx === '02' || item.dzlx === '05') {
                iframeLoad
            }
        }
        const handleItemClick = (src: any) => {
            console.log(src)
        }

        // iframe自适应会用到
        const calcPageHeight = (doc: any) => {
            let cHeight = Math.max(doc.body.clientHeight, doc.documentElement.clientHeight)
            let sHeight = Math.max(doc.body.scrollHeight, doc.documentElement.scrollHeight)
            let height = Math.max(cHeight, sHeight)
            return height
        }

        /**
         * @description: iframe加载方法
         * @return {*}
         */
        const iframeLoad = () => {
            if (props.showMode !== 'fullScreen') {
                let iframe = document.getElementById("iframe")
                //@ts-ignore
                let iDoc = iframe.contentDocument || iframe.document || iframe.contentWindow || iframe.contentDocument.parentWindow
                let height = calcPageHeight(iDoc);
                myData.scrollHeight = height
            }
        }

        // @ts-ignore

        getMenuList(window.WEB_CONFIG.cdRoot)
        const scrollbar = ref();

        // getMenuList(window.WEB_CONFIG.cdRoot, props.authCode)
        return {
            props,
            myData,
            isCollapse,
            handleOpen,
            handleClose,
            handleSelect,
            handleItemClick,
            iframeLoad,
            getQueryString
        }
    },
    render() {
        return (
            <div style="margin: 5px;padding:5px;border:1px solid #e5e7ed;border-radius:4px;display:flex;flex-direction:column">
                <el-scrollbar style="height:50px;width:100%">
                    <el-menu ellipsis={false} default-active={this.myData.firstDm} mode={"horizontal"} onSelect={this.handleSelect} active-text-color="#5d9dfe">
                        {this.myData.menuList.map((item: any) => {
                            return <MyMenuItem item={item} onHandleItemClick={this.handleItemClick}></MyMenuItem>
                        })}
                    </el-menu>
                </el-scrollbar>
                {this.myData.menuItemData.dzlx === '01' || this.myData.menuItemData.dzlx === '03' || this.myData.menuItemData.dzlx === '04' ? <SJDX params={assignDeep({ 'sys.authCode': this.myData.menuItemData.dm }, this.props.params)}></SJDX> : null}
                {
                    this.myData.menuItemData.dzlx === '02' || this.myData.menuItemData.dzlx === '05'
                        ? <iframe onLoad={this.iframeLoad} id="iframe" style={"width:100%;height:" + this.myData.scrollHeight + "px;"} src={this.getQueryString(this.props.params, this.myData.menuItemData.dz)} frameborder="0"></iframe>
                        : null
                }
            </div >
        )
    }

})
