import axios from "@/axios";
import { VXETable } from "@majinju/vxe-table";

// default-config.ts
export default {
    base_url: '/public/resource/tinymce',
    suffix: ".min",
    language: "zh-Hans",
    height: "400",
    menubar: true,
    branding: false,
    promotion: false,
    formats: {
        h1: { block: 'h1', styles: { 'font-size': '2em' } },
        h2: { block: 'h2', styles: { 'font-size': '1.5em' } },
        h3: { block: 'h3', styles: { 'font-size': '1.3em' } },
        h4: { block: 'h4', styles: { 'font-size': '1em' } },
        h5: { block: 'h5', styles: { 'font-size': '0.8em' } },
        h6: { block: 'h6', styles: { 'font-size': '0.7em' } },
    },
    images_upload_handler: (blobInfo: any, progress: any) => new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', axios.getBaseURL() + "/default");

        xhr.upload.onprogress = (e) => {
            progress(e.loaded / e.total * 100);
        };

        xhr.onload = () => {
            if (xhr.status === 403) {
                reject({ message: 'HTTP Error: ' + xhr.status, remove: true });
                return;
            }

            if (xhr.status < 200 || xhr.status >= 300) {
                reject('HTTP Error: ' + xhr.status);
                return;
            }

            const json = JSON.parse(xhr.responseText);

            if (!json || typeof json.data[0].id != 'string') {
                reject('Invalid JSON: ' + xhr.responseText);
                return;
            }

            resolve(axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + json.data[0].id);
        };

        xhr.onerror = () => {
            reject('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
        };

        const formData = new FormData();
        formData.append('files', blobInfo.blob(), blobInfo.filename());
        formData.append("yobj.sjzt", "default");
        formData.append("sys.cllx", "upload");
        formData.append("sys.authCode", 'QTQX');

        xhr.send(formData);
    }),
    file_picker_callback: async (callback: any, value: any, meta: any) => {
        // Provide file and text for the link dialog
        if (meta.filetype == 'file') {
            const { file, files } = await VXETable.readFile()
            const formData = new FormData();

            formData.append("files", file);

            formData.append("yobj.sjzt", "default");
            formData.append("sys.cllx", "upload");
            formData.append("sys.authCode", 'QTQX');
            axios.upload(formData).then((res: any) => {
                if (res.status) {
                    callback(axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + res.data[0].id, { text: res.data[0].wjm });
                    console.log("上传成功")
                }
            })


        }
    },

    plugins: "quickbars lists table link image fullscreen preview searchreplace code help codesample",
    toolbar:
        'undo redo | blocks | bold italic backcolor | alignleft aligncenter alignright alignjustify |  codesample image link | bullist numlist outdent indent | removeformat | code  preview help fullscreen',
    fontsize_formats:
        "8px 10px 12px 14pt 16px 18px 20px 22px 24px 26px 28px 30px 36px",
}
