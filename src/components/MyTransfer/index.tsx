/*
 * @Date: 2022-09-12 16:50:02
 * @LastEditTime: 2023-02-13 10:36:16
 * @Description: 穿梭框组件
 */
import axios from '@/axios';
import { assignDeep, getByPath } from '@/utils/common';
import { useUserStore } from '@/store/modules/user';
import { defineComponent, reactive, ref, watch } from 'vue'
import { CaretLeft, CaretRight } from '@element-plus/icons-vue';

export default defineComponent({
    name: 'MyTransfer',
    props: {
        cxsc: {
            type: Object,
            default: {}
        },
        plcz: {
            type: Object,
            default: {}
        },
        /**
         * 字段映射
         */
        zdys: {
            type: Object,
            default: {}
        },
        /**
         * 表格配置集
         */
        gridOptions: {
            type: Object,
            default: {}
        },
        id: {
            type: String,
            default: ""
        },
        /**
         * 搜索参数，与默认参数合并
         */
        searchParams: {
            type: Object,
            default: { yobj: {} }
        },
        disabled: {
            type: Boolean
        },
        readonly: {
            type: Boolean
        },
    },
    setup(props, ctx) {
        const srGrid = ref()
        const scGrid = ref()

        const myData: any = reactive({
            /**
             * 绑定输入框值
             */
            srValue: "",
            /**
             * 数据加载中
             */
            srLoading: false,
            scLoading: false,
            /**
             * 列定义
             */
            gridOptions: [],
            /**
             * 表格数据
             */
            srTableData: [],
            scTableData: [],
            /**
             * 分页配置
             */
            srTablePage: {
                pageSizes: [5, 10, 20, 50],
                total: 0,
                currentPage: 1,
                pageSize: 5,
                layouts: ['Sizes', 'PrevJump', 'PrevPage', 'NextPage', 'NextJump', 'FullJump', 'Total'],
            },
            scTablePage: {
                pageSizes: [5, 10, 20, 50],
                total: 0,
                currentPage: 1,
                pageSize: 5,
                layouts: ['Sizes', 'PrevJump', 'PrevPage', 'NextPage', 'NextJump', 'FullJump', 'Total'],
            },
            /**
             * 搜索参数-默认
             */
            searchParamsDefault: props.cxsc,
            srSearchParams: {
                sys: {
                    searchKey: ""
                },
                page: {
                    pageNumber: 0,
                    totalRequired: true,
                    pageSize: 5,
                }
            },
            scSearchParams: {
                sys: {
                    searchKey: ""
                },
                page: {
                    pageNumber: 0,
                    totalRequired: true,
                    pageSize: 5,
                }
            },
            visible: false,
            disabled: props.disabled,
            readonly: props.readonly,
            isdisabled: props.readonly || props.disabled,
        });
        myData.srSearchParams = assignDeep({}, myData.searchParamsDefault, props.searchParams);
        myData.srSearchParams.yobj.keyNot = props.id
        myData.scSearchParams = assignDeep({}, myData.searchParamsDefault, props.searchParams);
        myData.scSearchParams.yobj.keyIn = props.id


        /**
         * 翻页操作
         * @param currentPage 当前页
         * @param pageSize 页大小
         */
        const pageChangeEvent = ({ currentPage, pageSize }: any, mode: string, isSearch?: boolean) => {
            myData[mode + "TablePage"].currentPage = currentPage
            myData[mode + "TablePage"].pageSize = pageSize
            myData[mode + "SearchParams"].page.pageSize = pageSize
            if (!isSearch) {
                myData[mode + "SearchParams"].page.totalRequired = true
                searchList(mode);
            }
        }

        const searchList = (mode: string, reload: boolean = false) => {
            if (reload) {
                pageChangeEvent({ currentPage: 1, pageSize: myData[mode + "TablePage"].pageSize }, mode, true)
            }
            console.log("搜索参数", myData[mode + "SearchParams"].sys.searchKey)

            myData[mode + "Loading"] = true
            myData[mode + "SearchParams"].page.pageNumber = myData[mode + "TablePage"].currentPage
            axios.post(myData[mode + "SearchParams"], { tloading: false }).then(req => {
                myData[mode + "TableData"] = req.data.list
                myData[mode + "Loading"] = false
                if (myData[mode + "SearchParams"].page.totalRequired) {
                    myData[mode + "TablePage"].total = req.data.totalRow;
                }
            })
        }

        const change = async (mode: string) => {
            if (mode === 'sr') {
                if (srGrid.value.getCheckboxRecords().length > 0) {
                    let params = props.plcz
                    params.sys.editTableData = []
                    srGrid.value.getCheckboxRecords().forEach((item: any) => {
                        let row = {}
                        row[props.zdys["my_glj"]] = item.myGlj;
                        row[props.zdys["my_key"]] = props.id;
                        params.sys.editTableData.push(row);

                    })
                    await axios.post(params)
                    searchList("sr")
                    searchList("sc")
                }
            }
            else {
                if (scGrid.value.getCheckboxRecords().length > 0) {
                    let params = props.plcz
                    params.sys.editTableData = []
                    scGrid.value.getCheckboxRecords().forEach((item: any) => {
                        params.sys.editTableData.push({
                            id: item.myId,
                            yxx: 0
                        })

                    })
                    await axios.post(params)
                    searchList("sr")
                    searchList("sc")
                }
            }

        }
        //初始化
        searchList("sr")
        searchList("sc")


        myData.gridOptions = { ...props.gridOptions }
        //添加复选框
        myData.gridOptions.columns = [{ type: 'checkbox', title: '', width: 50 }, ...props.gridOptions.columns]

        return { pageChangeEvent, searchList, change, myData, srGrid, scGrid }
    },
    render() {
        return (
            <div style="display:flex;box-shadow:0 2px 12px 0 rgb(0 0 0 / 10%);">
                <div style="flex: 1;">
                    <div style="display:flex;align-items: center;padding: 10px;">
                        <el-input
                            size="small"
                            placeholder="请输入内容"
                            v-model={this.myData.srSearchParams.sys.searchKey}
                            clearable>
                        </el-input>
                        <el-button size="small" style="margin-left:10px;" onClick={() => this.searchList("sr", true)}>搜索</el-button>
                    </div>
                    <vxe-grid
                        ref="srGrid"
                        auto-resize
                        height="auto"
                        max-height="252px"
                        loading={this.myData.srLoading}
                        pager-config={this.myData.srTablePage}
                        data={this.myData.srTableData}
                        columns={this.myData.tableColumn}
                        onPage-change={(change: any) => this.pageChangeEvent(change, 'sr')}
                        {...this.myData.gridOptions}>
                    </vxe-grid>
                </div>

                <div style="padding:48px 8px 0;margin:auto 0">
                    <div><el-button size="small" icon={CaretRight} type="primary" onClick={() => this.change('sr')}></el-button></div>
                    <div><el-button size="small" icon={CaretLeft} type="primary" onClick={() => this.change('sc')} style="margin-top:4px"></el-button></div>
                </div>

                <div style="flex: 1;">
                    <div style="display:flex;align-items: center;padding: 10px;">
                        <el-input
                            size="small"
                            placeholder="请输入内容"
                            v-model={this.myData.scSearchParams.sys.searchKey}
                            clearable>
                        </el-input>
                        <el-button size="small" style="margin-left:10px;" onClick={() => this.searchList("sc", true)}>搜索</el-button>
                    </div>
                    <vxe-grid
                        ref="scGrid"
                        style="flex:1"
                        auto-resize
                        height="auto"
                        max-height="252px"
                        loading={this.myData.scLoading}
                        pager-config={this.myData.scTablePage}
                        data={this.myData.scTableData}
                        columns={this.myData.tableColumn}
                        onPage-change={(change: any) => this.pageChangeEvent(change, 'sc')}
                        {...this.myData.gridOptions}>
                    </vxe-grid>
                </div>
            </div>
        )
    }
})
