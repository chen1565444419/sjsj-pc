import { defineComponent, ref } from 'vue'
import Editor from '../MyEditor';
import MyMarkDown from '../MyMarkDown';

export default defineComponent({
    name: 'MixEditor',
    props: {
        /**
         * 输入值
         */
        modelValue: {
            type: String
            // default: ''
        },
        mode: {
            type: String
        },
        /**
         * 代码类型，传了就作为代码编辑器，不传就是普通markdown编辑器
         */
        codeType: {
            type: String
        }
    },
    emits: ["update:modelValue"],
    setup(props, context) {
        const type = ref("Markdown")
        if (props.modelValue && props.modelValue.substr(0, 1) === '<' && props.modelValue.substr(-1) === '>') {
            type.value = "HTML"
        }
        const update = (val: any) => {
            context.emit('update:modelValue', val)
        }
        const handleClick = (tab: any, event: Event) => {

        }
        return { type, handleClick, update }
    },
    render() {
        return (
            <>
                <el-tabs v-model={this.type} onTab-click={this.handleClick}>
                    <el-tab-pane label="富文本编辑器" name="HTML">
                        {this.mode === 'preview' ? <iframe frameborder="false" style="height:500px;width:100%;" srcdoc={this.modelValue}></iframe> : <Editor onUpdate:modelValue={this.update} value={this.modelValue}></Editor>}
                    </el-tab-pane>
                    <el-tab-pane label="Markdown编辑器" name="Markdown">
                        <MyMarkDown onUpdate:modelValue={this.update} mode={this.mode} modelValue={this.modelValue} codeType={this.codeType}></MyMarkDown>
                    </el-tab-pane>
                </el-tabs>
            </>
        )
    }
})
