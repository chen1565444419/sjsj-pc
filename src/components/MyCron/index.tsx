import { defineComponent, reactive, ref, watch } from 'vue'
import { vue3Cron } from 'my-cron-vue3'
import 'my-cron-vue3/lib/vue3Cron.css'
import classes from "index.module.scss"; // 引入样式

export default defineComponent({
    name: 'MyCron',
    components:{vue3Cron},
    inheritAttrs: true,
    props: {
        /**
         * 输入值
         */
        modelValue: {
            type: String
        },
        disabled: {
            type: String
        },
        readonly: {
            type: String
        }
    },
    emits: ["update:modelValue"],
    setup: async (props, context) => {
        const myData = reactive({
            options: {},
            valve: props.modelValue,
            isdisabled: props.readonly || props.disabled
        });

        const xInput = ref();
        watch(() => props.zdlb, async (newZdlb) => {
        })
        watch(() => props.disabled, (newValue) => {
            myData.isdisabled = props.readonly || newValue
        })
        watch(() => props.readonly, (newValue) => {
            myData.isdisabled = props.disabled || newValue
        })
        watch(() => props.modelValue, (newValue) => {
            myData.valve = newValue
        })
        const updateVal = (newValue: any) => {
            context.emit("update:modelValue", newValue)
            myData.valve = newValue
        }
        const ruleForm = ref<any>({
            // 任务表达式
            taskCron: '',
        })

        const cronPopover = ref(false)

        const changeCron = (val: any) => {
            if (typeof (val) !== 'string') return false
            ruleForm.value.taskCron = val
        }

        const togglePopover = (bol: any) => {
            cronPopover.value = bol
        }
        /**
         * 返回值
         */
        return {
            myData,
            xInput,
            updateVal,
            togglePopover,
            changeCron,
            cronPopover
        }
    },
    render() {
        return (
            <template>
                <el-input class="wih-450p" v-model={this.props.modelValue} placeholder="请点击配置或直接编辑"/>
                <el-button type="primary" style="margin-left: 10px" click={this.togglePopover(true)}>
                    配置
                </el-button>
                <el-button type="danger" style="margin-left: 10px" click={this.props.modelValue= ''}>
                    清除
                </el-button>
                <div class={classes.cron}>
                    <el-drawer v-model={this.cronPopover} with-header={false}>
                        <vue3Cron change={this.changeCron} close={this.togglePopover(false)} max-height="400px" i18n="cn">
                        </vue3Cron>
                    </el-drawer>
                </div>
            </template>
        )
    }
})
