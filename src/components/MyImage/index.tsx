import { defineComponent, reactive, watch } from 'vue'
import axios from "@/axios";
import { ElMessage } from "element-plus";
import classes from './index.module.scss';
import { Plus } from '@element-plus/icons-vue';
export default defineComponent({
    name: "MyImage",
    inheritAttrs: false,
    props: {
        /**
         * 输入值
         */
        modelValue: {
            type: String
        },
        /**
         * @description: 处理类型，用于判断图片是否是编辑还是详情查看
         * @return {*}
         */
        cllx: {
            type: String,
        },
        disabled: {
            type: String,
        },
        readonly: {
            type: String
        },
        /**
         * 请求参数
         */
        qqcs:{
            type: Object
        }
    },
    //与父组件进行双向绑定，绑定值为图片的id
    emits: ["update:modelValue"],
    setup(props, context) {
        const myData: any = reactive({
            value: '',
            imageUrl: '',
            isdisabled: props.readonly || props.disabled,
            qqcs:{ "sys.authCode": "QTQX", "sys.cllx": "upload", "yobj.sjzt": 'default' }
        })
        if(props.qqcs){
            for(let key in props.qqcs){
                myData.qqcs[key]=props.qqcs[key];
            }
        }

        // @ts-ignore
        const ctdy = window.WEB_CONFIG.ctdy;
        if (props.modelValue) {
            let url;
            let arr = props.modelValue.split(",");
            if (arr[0].length === 32 && arr[0].indexOf(".")==-1) {
                url = axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id="
                    + arr[0] + (ctdy ? ("&sys.ctdy=" + ctdy) : "");
            } else if(arr[0].startsWith('http')||arr[0].indexOf(".")!=-1){
                // http图片
                // 处理多张图片的情况
                url = arr[0]
            } else {
                //base64图片
                url = "data:image/png;base64," + arr[0];
            }
            myData.imageUrl = url;
        }
        /**
         * 自定义上传方式
         * @param params
         */
        const uploadSectionFile = (params: any) => {
            const file = params.file,
                fileType = file.type,
                isImage = fileType.indexOf("image") !== -1,
                isLt2M = file.size / 1024 / 1024 < 2;
            //对上传文件过滤，只允许上传图片
            if (!isImage) {
                ElMessage.error("只能上传图片格式png、jpg、gif!")
                return;
            }
            if (!isLt2M) {
                ElMessage.error("只能上传图片大小小于2M")
                return;
            }
            // 根据后台需求数据格式.可后续根据需要进行修改
            const form = new FormData();
            form.append("files", file);
            if(myData.qqcs){
                for(let key in myData.qqcs){
                    form.append(key, myData.qqcs[key]);
                }
            }
            axios.post(form).then(res => {
                if (res.status) {
                    myData.value = res.data[0].id
                    let url = axios.getBaseURL() + "/default?sjdx.dxdm=SYS_QX_QTQX&sys.cllx=download&yobj.id=" + res.data[0].id;
                    if (ctdy) {
                        url += "&sys.ctdy=" + ctdy;
                    }
                    myData.imageUrl = url;
                    context.emit("update:modelValue", res.data[0].zjfwdz)
                } else {
                    ElMessage.error("数据上传失败")
                }
            })
        }
        watch(() => props.disabled, (newValue) => {
            myData.isdisabled = props.readonly || newValue
        })
        watch(() => props.readonly, (newValue) => {
            myData.isdisabled = props.disabled || newValue
        })
        return {
            props,
            myData,
            uploadSectionFile
        }
    },
    render() {
        return (
            <el-upload
                class={classes.avatarUploader}
                v-model={this.myData.value}
                disabled={this.myData.isdisabled}
                action
                show-file-list={false}
                http-request={this.uploadSectionFile}>
                {this.myData.imageUrl ? <el-image
                    style="width: 100px; height: 100px"
                    src={this.myData.imageUrl}
                    preview-src-list={this.props.cllx === 'dxjcxx' ? [this.myData.imageUrl] : []}
                    fit="cover"></el-image> : <el-icon class="avatar-uploader-icon"><Plus /></el-icon>}
            </el-upload>
        )
    }
})
