import { store } from '@/store';
import watermark from '@/utils/watermark';
import axios from "@/axios";
import { ElMessage } from "element-plus";
import { defineStore } from 'pinia';
import { useRouterStore } from '@/store/modules/router';
import router from '@/router';

interface UserState {
    userInfo: any | null;
    token: string | undefined;
    sys: any | null;
    currentSystemName: String | undefined;
    currentSystemCode: String | undefined;
}

export const useUserStore = defineStore({
    id: 'app-user',
    state: (): UserState => ({
        //用户信息
        userInfo: null,
        //token
        token: undefined,
        //系统信息
        sys: null,
        //当前系统名称
        currentSystemName: undefined,
        //当前系统代码
        currentSystemCode: undefined,
    }),
    getters: {
        getUser(): any {
            return this.userInfo || JSON.parse(localStorage.getItem("userInfo") || '{}') || {};
        },
        getSys(): any {
            return this.sys || JSON.parse(localStorage.getItem("sys") || '{}') || {};
        },
        getCurrentSystemName(): string {
            // @ts-ignore
            return this.currentSystemName || JSON.parse(localStorage.getItem("currentSystem") || '{}').name || window.WEB_CONFIG.xtmc || ''
        },
        getCurrentSystemCode(): string {
            // @ts-ignore
            return this.currentSystemCode || JSON.parse(localStorage.getItem("currentSystem") || '{}').code || window.WEB_CONFIG.cdRoot || ''
        },
        getToken(): string {
            // @ts-ignore
            return this.token || localStorage.getItem("token") || ''
        }
    },
    actions: {

        /**
         * 设置用户信息
         * @param userInfo
         */
        setUser(userInfo: any | null) {
            this.userInfo = userInfo;
            if (userInfo) {
                localStorage.setItem("userInfo", JSON.stringify(userInfo))
                watermark.set(userInfo.yhxm + "," + userInfo.yhdm);
            } else {
                localStorage.removeItem("userInfo")
                watermark.set("临时用户");
            }
        },

        /**
         * 设置系统信息
         * @param sys
         */
        setSys(sys: any) {
            this.sys = sys;
            if (sys) {
                localStorage.setItem("sys", JSON.stringify(sys))
            }
            else {
                localStorage.removeItem("sys")
            }
        },

        /**
         * 设置token
         * @param token
         */
        setToken(token: string | undefined) {
            this.token = token
            if (token)
                localStorage.setItem("token", token)
            else {
                localStorage.removeItem("token")
            }
        },

        /**
         * 设置当前系统名称
         * @param name
         */
        setCurrentSystemName(name: string | undefined) {
            this.currentSystemName = name
            if (name) {
                localStorage.setItem("currentSystemName", name)
            }
            else {
                localStorage.removeItem("currentSystemName")
            }
        },

        /**
         * 设置当前系统代码
         * @param code
         */
        setCurrentSystemCode(code: string | undefined) {
            this.currentSystemCode = code
            if (code)
                localStorage.setItem("currentSystemCode", code)
            else {
                localStorage.removeItem("currentSystemCode")
            }

        },

        /**
         * 登录
         * @param params
         */
        async login(params: any) {
            axios.post(params, { ctdy: false })
                .then(async (response: any) => {
                    if (response.status) {
                        this.saveUserInfo(response.data)
                        ElMessage.success("登陆成功");
                        await useRouterStore().setRouter()
                        router.push("/")
                    } else {
                        ElMessage.error(response.msg);
                    }
                });

        },

        saveUserInfo(data: any) {
            this.setSys(data.sys)
            this.setUser(data.user)
            this.setToken(data.sys.token)
        },

        /**
         * 登出
         */
        logout() {
            axios.post({
                sjdx: {
                    dxdm: 'SYS_QX_YHXX_MRDL'
                },
                sys: {
                    cllx: 'yhtc'
                }
            }, { ctdy: false }).then((req: any) => {
                if (req.status) {
                    ElMessage.success(req.msg)
                    router.push('/login')
                    //将当前系统代码返回回去
                    this.setToken(undefined)
                    this.setSys(null)
                    this.setCurrentSystemName(undefined)
                    this.setCurrentSystemCode(undefined)
                    this.setUser(null)
                } else {
                    ElMessage.warning(req.msg)
                }
            })

        }
    },
})

// Need to be used outside the setup
export function useUserStoreWithOut() {
    return useUserStore(store);
}
