/*
 * @Date: 2023-02-10 15:34:57
 * @LastEditTime: 2023-02-16 20:19:18
 * @Description: 
 */

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import '@/styles/element.scss';


import zhCn from 'element-plus/dist/locale/zh-cn.mjs'

// import locale from 'element-plus/lib/locale/lang/zh-cn.js'

export default (app: any) => {
  app.use(ElementPlus, { locale: zhCn, size: 'small', zIndex: 3000 })
}
