import VXETable from '@majinju/vxe-table';
import XEUtils from "xe-utils";
import MyDownList from "../components/MyDownList";
import MySelect from "../components/MySelect";
import MySelectGrid from "../components/MySelectGrid";
// import MySelectGrid1 from "@/views/sjdx1/MySelectGrid1.vue";
import { dateFormat, zdMcByDm } from "@/utils/common"
import MyMarkDown from "../components/MyMarkDown";
import MyRadio from "../components/MyRadio";
import MyCheckbox from "../components/MyCheckbox";
import MyImage from "../components/MyImage";
import MyFile from "../components/MyFile";
import MyDownListMultiple from "../components/MyDownListMultiple";
import MyGroup from '@/components/MyGroup';
import MyCascader from "../components/MyCascader";
import MyMenu from "../components/MyMenu";
import MyIconPicker from "../components/MyIconPicker";
import MyRealTimeMsg from "@/components/MyRealTimeMsg";
import MyForm from '@/components/MyForm';
import MyTransfer from "@/components/MyTransfer";
import MyEditor from "@/components/MyEditor";
import MyMixEditor from "@/components/MyMixEditor";
import MyDocument from "@/components/MyDocument";
import MyNumPicker from '@/components/MyNumPicker';

/**
 * 基于 vxe-table 表格的适配插件
 */
export const VxeExtend = {
  install(vxetablecore: any) {
    const { renderer } = vxetablecore
    renderer.mixin({
      MySelect: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MySelect v-model={row[column.property]} {...props} />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MySelect v-model={data[property]} {...props} />
          ]
        },
      },
      MyRadio: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MySelect v-model={row[column.property]} {...props} />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyRadio v-model={data[property]} {...props} />
          ]
        },
      },
      MyCheckbox: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MySelect v-model={row[column.property]} {...props} />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyCheckbox v-model={data[property]} {...props} />
          ]
        },
      },
      MySwitch: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <vxe-switch v-model={row[column.property]} {...props} />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <vxe-switch v-model={data[property]} {...props} />
          ]
        },
      },
      MyDownList: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MyDownList v-model={row[column.property]} {...props}
            />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyDownList v-model={data[property]} {...props} />
          ]
        },
      },
      MyMarkDown: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyMarkDown v-model={data[property]} {...props} />
          ]
        },
      },
      MyJsonEditor: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <v-md-editor v-model={data[property]} {...props} />
          ]
        },
      },
      MySelectGrid: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MySelectGrid v-model={data[property]} {...props} />
          ]
        },
      },
      MyForm: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyForm v-model={data[property]} {...props} />
          ]
        },
      },
      MyMenu: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyMenu v-model={data[property]} {...props} />
          ]
        },
      },
      MyIconPicker: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyIconPicker v-model={data[property]} {...props} />
          ]
        },
      },
      MyGroup: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyGroup v-model={data[property]} {...props} />
          ]
        },
      },
      MyRealTimeMsg: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyRealTimeMsg v-model={data[property]} {...props} />
          ]
        },
      },
      MyImage: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MyImage v-model={row[column.property]} {...props}
            />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyImage v-model={data[property]} {...props} />
          ]
        },
      },
      MyFile: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MyFile v-model={row[column.property]} {...props}
            />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyFile v-model={data[property]} {...props} />
          ]
        },
      },
      MyDownListMultiple: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MyDownListMultiple v-model={row[column.property]} {...props}
            />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyDownListMultiple v-model={data[property]} {...props} />
          ]
        },
      },
      MyCascader: {
        autofocus: 'input.my-input__inner',
        // 可编辑激活模板
        renderEdit(renderOpts: any, params: any) {
          let { row, column } = params
          const { props } = renderOpts
          let val = row[column.property];
          if (val) {
            zdMcByDm(column.editRender.props.zdlb, val).then(function (mc: any) {
              row[column.property + "Mc"] = mc;
            })
          }
          return [
            <MyCascader v-model={row[column.property]} {...props}
            />
          ]
        },
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyCascader v-model={data[property]} {...props} />
          ]
        },
      },
      MyTransfer: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyTransfer v-model={data[property]} {...props} />
          ]
        },
      },
      MyEditor: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyEditor v-model={data[property]} {...props} />
          ]
        },
      },
      MyMixEditor: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyMixEditor v-model={data[property]} {...props} />
          ]
        },
      },
      MyDocument: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyDocument v-model={data[property]} {...props} />
          ]
        },
      },
      MyNumPicker: {
        autofocus: 'input.my-input__inner',
        renderItemContent(renderOpts: any, params: any) {
          const { data, property } = params
          const { props } = renderOpts
          return [
            <MyNumPicker v-model={data[property]} {...props} />
          ]
        },
      }
    })
  }
}

/**
 * 时间日期格式转换---日期格式
 */
VXETable.formats.add('formatDate', ({ cellValue, column }) => {
  return dateFormat(cellValue, column.params.format || 'YYYY-MM-DD HH:mm:ss')
})
/**
 * 格式金额，默认2位数
 */
VXETable.formats.add('myAmount', ({ cellValue }, digits = 2) => {
  return XEUtils.commafy(XEUtils.toNumber(cellValue), { digits })
})

/**
 * 下拉框字典转换
 */
VXETable.formats.add('formatterZd', ({ cellValue, row, column }) => {
  return row[column.property + 'Mc'];
})


export default VxeExtend
