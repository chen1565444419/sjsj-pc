/*
 * @Date: 2023-02-10 16:11:54
 * @LastEditTime: 2023-02-16 20:01:27
 * @Description:
 */
import { createApp } from 'vue';
import App from './App';
//状态
// import store from './store';
import { setupStore } from '@/store';
//请求工具
import axios from "./axios";
//路由
import router from './router';
//饿了么组件
import installElementPlus from './plugins/element';
//vxe表格
import installVxe from './plugins/vxe-table';
//国际化
// import i18n from './i18n';//暂时屏蔽
//自定义公共样式
import '@/styles/common.scss';
import '@/assets/icon/iconfont.css'
import './index.css'


import eIconPicker from 'e-icon-picker';
import "e-icon-picker/icon/default-icon/symbol.js"; //基本彩色图标库
import 'e-icon-picker/index.css'; // 基本样式，包含基本图标
import 'font-awesome/css/font-awesome.min.css';

//element-plus图标官网现在已经通过组件式使用了
//不再是字体形式，所以使用时需要全局注册组件
//如果已经全局使用了element-plus组件，则element-plus会默认全部注册图标，不需要再去安装图标库注册
import * as ElementPlusIconsVue from '@element-plus/icons-vue'; //element-plus 图标库
import eIconList from 'e-icon-picker/icon/default-icon/eIconList.js'
import elementPlusIcon from "e-icon-picker/icon/ele/element-plus.js";//element-plus的图标
import fontAwesome470 from "e-icon-picker/icon/fontawesome/font-awesome.v4.7.0.js";//fontAwesome470的图标



//设置服务端基础路径
// @ts-ignore
axios.setBaseURL(window.WEB_CONFIG.urlRoot);

const app = createApp(App, {});

// 全局注册所有element-plus图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

installElementPlus(app);
installVxe(app);
setupStore(app);

//markdown
import installMarkdown from "@/plugins/markdown";
installMarkdown(app);

// 百度地图相关
import BaiduMap from 'vue-baidu-map-3x'
app.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'tw2cKfyUPYXfe895595SBer19VzstzLN',
})

app.use(router).use(eIconPicker, {
  addIconList: [...elementPlusIcon, ...fontAwesome470, ...eIconList],//全局添加图标
  removeIconList: [],//全局删除图标
  zIndex: 3100//选择器弹层的最低层,全局配置
})
  .mount('#app');
