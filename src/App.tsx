/*
 * @Date: 2022-05-31 14:27:16
 * @LastEditTime: 2022-10-10 16:05:39
 * @Description: vue入口
 */
import { defineComponent } from "vue";
import { RouterView } from 'vue-router';

export default defineComponent({
    name: "App",
    setup() {
        const renderDom = () => {
            return (
                <div class="body">
                    <RouterView />
                </div>
            )
        }
        return renderDom
    }
})
