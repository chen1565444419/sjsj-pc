import MyDownList from "@/components/MyDownList";
import { isEmpty } from "@/utils/common"
import { ElMessage } from "element-plus";
export default {
    ptgl_sjdx_dxzt: (data: any) => {
        const dxztChange = (row: any) => {
            if (row) {
                data.data.dxztlx = row.lx
                if (data.$form.getItemByField("jtdx")) {
                    data.$form.getItemByField("jtdx").itemRender.props.searchParams = {
                        yobj: { owner: row.dxgs, sjzt: row.dm, ztlx: row.lx },
                    }
                    if (row.lx === 'elastic_search') {
                        data.$form.getItemByField("jtdx").itemRender.props.searchParams = {
                            sjdx: { dxdm: 'SYS_SJGL_SJDX' },
                            sys: { cllx: 'searchTableByEs', authCode: '' },
                            yobj: { owner: row.dxgs, sjzt: row.dm, ztlx: row.lx },
                        }
                    }
                } else if (data.$form.getItemByField("pldrxz")) {
                    data.$form.getItemByField("pldrxz").itemRender.props.searchParams = {
                        owner: row.dxgs, sjzt: row.dm, ztlx: row.lx
                    }
                    data.$form.getItemByField("pldrxz").itemRender.props.getList =
                        function (getListMr: Function, myData: any, xGrid: any) {
                            getListMr(true)
                        }
                }
                data.data.jtdxShow = true
            } else {
                data.data.dxztlx = ""
                data.data.jtdxShow = false
                if (data.$form.getItemByField("pldrxz")) {
                    data.$form.getItemByField("pldrxz").itemRender.props.searchParams = {
                        owner: row.dxgs, sjzt: null, ztlx: row.lx
                    }
                    data.$form.getItemByField("pldrxz").itemRender.props.getList =
                        function (getListMr: Function, myData: any, xGrid: any) {
                            myData.tableData = []
                            xGrid.value.reloadData(myData.tableData)
                            ElMessage.warning("请先选择数据载体");
                        }
                }
            }
        }
        if (data.$table) {
            return (
                <span>{data.row.dxztMc}</span>
            )
        }
        if (data.$form) {
            const updateVal = (value: any) => {
                data.data.dxzt = value
                data.data.jtdx = ''
            }
            return (
                <MyDownList {...data.item.itemRender.props} modelValue={data.data.dxzt} onUpdate:modelValue={updateVal} onResult={dxztChange} ></MyDownList>
            )
        }
    },
    ptgl_sjdx_dxgs: (data: any) => {
        if (data.$table) {
            return (
                <span>{data.row.dxgs}</span>
            )
        }
        if (data.$form) {
            const updateVal = (value: any) => {
                data.data.dxgs = value
                data.data.jtdx = ''
                if (data.$form.getItemByField("jtdx")) {
                    data.$form.getItemByField("jtdx").itemRender.props.searchParams.yobj.owner = value
                } else if (data.$form.getItemByField("pldrxz")) {
                    data.$form.getItemByField("pldrxz").itemRender.props.searchParams.owner = value
                }
            }
            return (
                <vxe-input {...data.item.itemRender.props} onUpdate:modelValue={updateVal} modelValue={data.data.dxgs}></vxe-input>
            )
        }
    },
    ptgl_sjdx_jtdx: (data: any) => {
        if (data.$table) {
            return (
                <span>{data.row.jtdx}</span>
            )
        }
        if (data.$form) {
            const updateVal = (value: any) => {
                data.data.jtdx = value
            }
            if (["ftp", "bdwj", "kafka", "qtzt", "url"].includes(data.data.dxztlx)) {
                return <vxe-input {...data.item.itemRender.props} onUpdate:modelValue={updateVal} modelValue={data.data.jtdx}></vxe-input>
            }
            return !isEmpty(data.data.dxzt) && data.data.jtdxShow ? <MyDownList {...data.item.itemRender.props} zdlb="SYS_COMMON_TABLES" onUpdate:modelValue={updateVal} modelValue={data.data.jtdx} ></MyDownList> : <vxe-input placeholder="请选择数据载体" disabled></vxe-input>
        }
    }
}
