/*
 * @Date: 2022-10-22 16:06:21
 * @LastEditTime: 2022-10-22 16:23:06
 * @Description: 
 */
import MyDownList from "@/components/MyDownList";
export default {
    fqx: (data: any) => {
        if (data.$table) {
            return (
                <span>{data.row.dm}</span>
            )
        }
        if (data.$form) {
            const updateVal = (value: any) => {
                data.data.fqx = value
            }
            const change = (value: any) => {
                if (value && value.qxdm) {
                    data.data.dm = value.qxdm + "_"
                }
            }
            return (
                <MyDownList {...data.item.itemRender.props} modelValue={data.data.fqx} onUpdate:modelValue={updateVal} onChange={change}></MyDownList>
            )
        }
    }
}
